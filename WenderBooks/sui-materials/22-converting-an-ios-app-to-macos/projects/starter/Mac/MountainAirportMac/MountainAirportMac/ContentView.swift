//
//  ContentView.swift
//  MountainAirportMac
//
//  Created by Don Clore on 6/12/22.
//

import SwiftUI

struct ContentView: View {
    @StateObject var flightInfo = FlightData()

    @SceneStorage("displayState") var displayState: DisplayState = .none

    @SceneStorage("lastViewedFlightID") var lastViewedFlightID: Int?
    @SceneStorage("selectedFlightID") var selectedFlightID: Int?

    var selectedFlight: FlightInformation? {
        guard let id = selectedFlightID else {
            return nil
        }
        return flightInfo.getFlightById(id)
    }

    var lastViewedFlight: FlightInformation? {
        guard let id = lastViewedFlightID else {
            return nil
        }
        return flightInfo.getFlightById(id)
    }

    var body: some View {
        NavigationView {
            WelcomeView(flightInfo: flightInfo)

            switch displayState {
            case .none:
                EmptyView()
            case .flightBoard:
                HStack {
                    FlightStatusBoard(flights: flightInfo.getDaysFlights(Date()))
                    FlightDetails(flight: selectedFlight)
                }
            case .searchFlights:
                SearchFlights(flightData: flightInfo.flights)
            case .awards:
                AwardsView()
            case .timeline:
                FlightTimelineView(flights: flightInfo.flights.filter { Calendar.current.isDate($0.localTime, inSameDayAs: Date()) })
            case .lastFlight:
                FlightDetails(flight: lastViewedFlight)
            }
        }
        .navigationTitle("Mountain Airport")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
