//
//  MountainAirportMacApp.swift
//  MountainAirportMac
//
//  Created by Don Clore on 6/12/22.
//

import SwiftUI

@main
struct MountainAirportMacApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
        .commands {
            SidebarCommands()
        }
    }
}

enum DisplayState: Int {
    case none
    case flightBoard
    case searchFlights
    case awards
    case timeline
    case lastFlight
}
