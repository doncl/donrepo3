//
//  MacMarkDownApp.swift
//  MacMarkDown
//
//  Created by Don Clore on 6/12/22.
//

import SwiftUI

@main
struct MacMarkDownApp: App {
    var body: some Scene {
        DocumentGroup(newDocument: MacMarkDownDocument()) { file in
            ContentView(document: file.$document)
        }
        .commands {
            MenuCommands()
        }

        Settings {
            SettingsView()
        }
    }
}
