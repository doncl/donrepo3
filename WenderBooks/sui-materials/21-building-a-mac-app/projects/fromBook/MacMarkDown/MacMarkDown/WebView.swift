//
//  WebView.swift
//  MacMarkDown
//
//  Created by Don Clore on 6/12/22.
//

import SwiftUI
import WebKit

struct WebView: NSViewRepresentable {
    @AppStorage("styleSheet") var styleSheet: StyleSheet = .raywenderlich

    typealias NSViewType = WKWebView

    var html: String

    var formattedHtml: String {
        return """
        <html>
        <head>
           <link href="\(styleSheet).css" rel="stylesheet">
        </head>
        <body>
           \(html)
        </body>
        </html>
        """
    }

    init(html: String) {
        self.html = html
    }

    func makeNSView(context _: Context) -> WKWebView {
        WKWebView()
    }

    func updateNSView(_ nsView: WKWebView, context _: Context) {
        nsView.loadHTMLString(formattedHtml, baseURL: Bundle.main.resourceURL)
    }
}
