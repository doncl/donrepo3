//
//  MenuCommands.swift
//  MacMarkDown
//
//  Created by Don Clore on 6/12/22.
//

import SwiftUI

struct MenuCommands: Commands {
    @AppStorage("styleSheet") var styleSheet: StyleSheet = .raywenderlich

    var body: some Commands {
        CommandGroup(before: .help) {
            Button("Markdown Cheatsheet") {
                showCheatSheet()
            }
            .keyboardShortcut("/", modifiers: .command)

            Divider()
        }

        CommandMenu("Stylesheet") {
            ForEach(StyleSheet.allCases, id: \.self) { style in
                Button(style.rawValue) {
                    styleSheet = style
                }
                .keyboardShortcut(style.shortcutKey, modifiers: .command)
            }
        }
    }

    private func showCheatSheet() {
        let cheatSheetAddress = "https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet"

        guard let url = URL(string: cheatSheetAddress) else {
            fatalError("Invalid cheatsheet URL")
        }

        NSWorkspace.shared.open(url)
    }
}
