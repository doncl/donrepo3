//
//  SidebarView.swift
//  OnThisDay
//
//  Created by Don Clore on 6/26/22.
//

import SwiftUI

struct SidebarView: View {
    @Binding var selection: EventType?

    var body: some View {
        List(selection: $selection) {
            Section("TODAY") {
                ForEach(EventType.allCases, id: \.self) { type in
                    Text(type.rawValue)
                }
            }
        }
        .listStyle(.sidebar)
    }
}

struct SidebarView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            SidebarView(selection: .constant(nil))
                .frame(width: 200)
                .preferredColorScheme(.light)

            SidebarView(selection: .constant(nil))
                .frame(width: 200)
                .preferredColorScheme(.dark)
        }
    }
}
