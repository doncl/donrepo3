//
//  ContentView.swift
//  OnThisDay
//
//  Created by Don Clore on 6/26/22.
//

import SwiftUI

struct ContentView: View {
    @State private var eventType: EventType? = .events
    @EnvironmentObject var appState: AppState

    var windowTitle: String {
        if let eventType = eventType {
            return "On This Day - \(eventType.rawValue)"
        }
        return "On This Day"
    }

    var events: [Event] {
        appState.dataFor(eventType: eventType)
    }

    var body: some View {
        NavigationView {
            SidebarView(selection: $eventType)
            GridView(gridData: events)
        }
        .frame(
            minWidth: 700,
            idealWidth: 1000,
            maxWidth: .infinity,
            minHeight: 400,
            idealHeight: 800,
            maxHeight: .infinity
        )

        .navigationTitle(windowTitle)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
