//
//  OnThisDayApp.swift
//  OnThisDay
//
//  Created by Don Clore on 6/26/22.
//

import SwiftUI

@main
struct OnThisDayApp: App {
    @StateObject var appState = AppState()

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(appState)
        }
    }
}
