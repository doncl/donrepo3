//
//  ContentView.swift
//  LottieInSwiftUI
//
//  Created by Giordano Scalzo on 06/07/2021.
//

import Lottie
import SwiftUI

struct LottieAnimationWrapper: UIViewRepresentable {
    private let animationView = LottieAnimationView()
    var animationName = ""

    @Binding var play: Bool

    func makeUIView(context _: Context) -> UIView {
        let view = UIView()

        animationView.animation = LottieAnimation.named(animationName)
        animationView.contentMode = .scaleAspectFit
        animationView.scalesLargeContentImage = true
        animationView.translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(animationView)
        NSLayoutConstraint.activate([
            animationView.heightAnchor.constraint(equalTo: view.heightAnchor),
            animationView.widthAnchor.constraint(equalTo: view.widthAnchor),
        ])
        return view
    }

    func updateUIView(_: UIView, context _: Context) {
        guard play else { return }

        animationView.play { _ in
            play = false
        }
    }
}

struct ContentView: View {
    @State
    private var play = false

    var body: some View {
        ZStack {
            Color.yellow
                .edgesIgnoringSafeArea(.all)
            VStack {
//                LottieAnimationWrapper(animationName: "filling-heart",
//                                play: $play)
//                    .padding(.horizontal, 30)
//              LottieAnimationWrapper(animationName: "IdunnoWhatThisIs",
//                              play: $play)
                LottieAnimationWrapper(animationName: "Lottie Lego",
                                       play: $play)
                    .padding(.horizontal, 30)

                    .padding(.horizontal, 30)

                Button {
                    play = true
                } label: {
                    Text("Fill me!")
                        .fontWeight(.heavy)
                        .padding(15)
                        .background(.white)
                        .cornerRadius(10)
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
