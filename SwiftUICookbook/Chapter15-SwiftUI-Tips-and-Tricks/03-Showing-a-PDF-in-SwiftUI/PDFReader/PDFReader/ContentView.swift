//
//  ContentView.swift
//  PDFReader
//
//  Created by Giordano Scalzo on 15/09/2021.
//

import PDFKit
import SwiftUI

struct PDFKitView: UIViewRepresentable {
    let url: URL

    func makeUIView(context _: UIViewRepresentableContext<PDFKitView>)
        -> PDFView
    {
        let pdfView = PDFView()
        pdfView.document = PDFDocument(url: url)
        return pdfView
    }

    func updateUIView(_: PDFView,
                      context _: UIViewRepresentableContext<PDFKitView>) {}
}

struct ContentView: View {
    let documentURL = Bundle.main.url(forResource: "PDFBook",
                                      withExtension: "pdf")!
    var body: some View {
        VStack(alignment: .leading) {
            Text("The Waking Lights")
                .font(.largeTitle)
            Text("By Urna Semper")
                .font(.title)
            PDFKitView(url: documentURL)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
