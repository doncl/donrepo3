//
//  SnapshotTestingSwiftUITests.swift
//  SnapshotTestingSwiftUITests
//
//  Created by Giordano Scalzo on 16/09/2021.
//

import SnapshotTesting
@testable import SnapshotTestingSwiftUI
import SwiftUI
import XCTest

public extension Snapshotting where Value: View, Format == UIImage {
    static func image(
        on config: ViewImageConfig) -> Snapshotting
    {
        Snapshotting<UIViewController, UIImage>.image(on: config).pullback(UIHostingController.init(rootView:))
    }
}

class SnapshotTestingSwiftUITests: XCTestCase {
    override class func setUp() {
        diffTool = "ksdiff"
    }

    func testContentView() throws {
        assertSnapshot(matching: ContentView(),
                       as: .image(on: .iPhoneXsMax))
    }
}
