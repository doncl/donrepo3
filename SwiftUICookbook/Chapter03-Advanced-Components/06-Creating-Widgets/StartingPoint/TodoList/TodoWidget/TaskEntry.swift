//
//  TaskEntry.swift
//  TodoList
//
//  Created by Don Clore on 9/18/22.
//

import SwiftUI
import WidgetKit

struct TaskEntry: TimelineEntry {
    let date: Date
    let task: Task
}
