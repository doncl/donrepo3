//
//  Provider.swift
//  TodoList
//
//  Created by Don Clore on 9/18/22.
//

import SwiftUI
import WidgetKit

struct Provider: TimelineProvider {
    func placeholder(in _: Context) -> TaskEntry {
        TaskEntry(date: Date(), task: tasks[0])
    }

    func getSnapshot(in _: Context, completion: @escaping (TaskEntry) -> Void) {
        let entry = TaskEntry(date: Date(), task: tasks[0])
        completion(entry)
    }

    func getTimeline(in _: Context, completion: @escaping (Timeline<TaskEntry>) -> Void) {
        var entries: [TaskEntry] = []

        let currentDate = Date()
        let filteredTasks = tasks.sorted(by: { $0.priority > $1.priority })

        for index in 0 ..< filteredTasks.count {
            let task = filteredTasks[index]

            let entryDate = Calendar.current.date(byAdding: .second, value: index * 10, to: currentDate)!

            let entry = TaskEntry(date: entryDate, task: task)

            entries.append(entry)
        }

        let timeline = Timeline(entries: entries, policy: .atEnd)

        completion(timeline)
    }
}

struct TodoWidgetEntryView: View {
    var entry: Provider.Entry

    var body: some View {
        ZStack {
            Color("WidgetBackground")
                .ignoresSafeArea()

            Text(entry.task.description)
        }
    }
}
