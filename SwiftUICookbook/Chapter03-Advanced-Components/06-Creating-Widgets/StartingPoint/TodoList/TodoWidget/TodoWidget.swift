//
//  TodoWidget.swift
//  TodoWidget
//
//  Created by Don Clore on 9/18/22.
//

import SwiftUI
import WidgetKit

// struct Provider: TimelineProvider {
//    func placeholder(in context: Context) -> SimpleEntry {
//        SimpleEntry(date: Date())
//    }
//
//    func getSnapshot(in context: Context, completion: @escaping (SimpleEntry) -> ()) {
//        let entry = SimpleEntry(date: Date())
//        completion(entry)
//    }
//
//    func getTimeline(in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
//        var entries: [SimpleEntry] = []
//
//        // Generate a timeline consisting of five entries an hour apart, starting from the current date.
//        let currentDate = Date()
//        for hourOffset in 0 ..< 5 {
//            let entryDate = Calendar.current.date(byAdding: .hour, value: hourOffset, to: currentDate)!
//            let entry = SimpleEntry(date: entryDate)
//            entries.append(entry)
//        }
//
//        let timeline = Timeline(entries: entries, policy: .atEnd)
//        completion(timeline)
//    }
// }

// struct TodoWidgetEntryView : View {
//    var entry: Provider.Entry
//
//    var body: some View {
//        Text(entry.date, style: .time)
//    }
// }

@main
struct TodoWidget: Widget {
    let kind: String = "TodoWidget"

    var body: some WidgetConfiguration {
        StaticConfiguration(kind: kind, provider: Provider()) { entry in
            TodoWidgetEntryView(entry: entry)
        }
        .configurationDisplayName("Task List Widget")
        .description("Shows next pressing item on a todo list")
        .supportedFamilies([.systemMedium, .systemLarge, .systemSmall, .systemExtraLarge])
    }
}

struct TodoWidget_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            TodoWidgetEntryView(entry: TaskEntry(date: Date(), task: tasks[0]))
                .previewContext(WidgetPreviewContext(family: .systemSmall))

            TodoWidgetEntryView(entry: TaskEntry(date: Date(), task: tasks[0]))
                .previewContext(WidgetPreviewContext(family: .systemMedium))
        }
    }
}
