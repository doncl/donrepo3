//
//  ViewPreview.swift
//  PreviewUIKitViews
//
//  Created by Edgar Nzokwe on 8/23/21.
//

import SwiftUI
import UIKit

struct ViewPreview: UIViewRepresentable {
    let viewBuilder: () -> UIView

    init(_ viewBuilder: @escaping () -> UIView) {
        self.viewBuilder = viewBuilder
    }

    func makeUIView(context _: Context) -> some UIView {
        viewBuilder()
    }

    func updateUIView(_: UIViewType, context _: Context) {
        // Nothing here
    }
}
