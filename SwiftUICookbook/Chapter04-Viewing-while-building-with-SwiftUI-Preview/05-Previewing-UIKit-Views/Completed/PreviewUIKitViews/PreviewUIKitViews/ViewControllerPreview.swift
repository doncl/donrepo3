//
//  ViewControllerPreview.swift
//  PreviewUIKitViews
//
//  Created by Edgar Nzokwe on 8/23/21.
//

import SwiftUI
import UIKit

struct ViewControllerPreview: UIViewControllerRepresentable {
    let viewControllerBuilder: () -> UIViewController

    init(_ viewControllerBuilder: @escaping () -> UIViewController) {
        self.viewControllerBuilder = viewControllerBuilder
    }

    func makeUIViewController(context _: Context) -> some UIViewController {
        return viewControllerBuilder()
    }

    func updateUIViewController(_: UIViewControllerType, context _: Context) {
        // Stays empty because we're not dealing with state changes
    }
}
