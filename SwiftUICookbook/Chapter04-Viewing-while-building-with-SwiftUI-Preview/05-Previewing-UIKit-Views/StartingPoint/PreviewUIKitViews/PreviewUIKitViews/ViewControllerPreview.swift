//
//  ViewControllerPreview.swift
//  PreviewUIKitViews
//
//  Created by Don Clore on 9/19/22.
//

import SwiftUI
import UIKit

struct ViewControllerPreview: UIViewControllerRepresentable {
    let viewControllerBuilder: () -> UIViewController

    init(_ viewControllerBuilder: @escaping () -> UIViewController) {
        self.viewControllerBuilder = viewControllerBuilder
    }

    func makeUIViewController(context _: Context) -> UIViewController {
        return viewControllerBuilder()
    }

    func updateUIViewController(_: UIViewController, context _: Context) {
        // Stays empty because we're not dealing with state changes.
    }
}
