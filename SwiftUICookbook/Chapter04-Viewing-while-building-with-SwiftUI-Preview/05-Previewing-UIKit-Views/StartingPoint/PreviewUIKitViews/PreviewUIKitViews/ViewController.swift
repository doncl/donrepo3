//
//  ViewController.swift
//  PreviewUIKitViews
//
//  Created by Edgar Nzokwe on 8/22/21.
//

import SwiftUI
import UIKit

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
        label.center = CGPoint(x: 160, y: 285)
        label.textAlignment = .right
        label.text = "Previewing UIKit views"
        label.font = UIFont.systemFont(ofSize: 64)
        label.textColor = .white
        label.backgroundColor = .purple
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        view.addSubview(label)
    }
}

struct ViewController_Preview: PreviewProvider {
    static var previews: some View {
        ViewControllerPreview {
            ViewController()
        }
    }
}
