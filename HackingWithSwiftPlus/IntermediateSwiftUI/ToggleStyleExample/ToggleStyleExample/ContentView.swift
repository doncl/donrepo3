//
//  ContentView.swift
//  ToggleStyleExample
//
//  Created by Don Clore on 7/8/22.
//

import SwiftUI

struct CheckToggleStyle: ToggleStyle {
    func makeBody(configuration: Configuration) -> some View {
        HStack {
            configuration.label

            Button {
                configuration.isOn.toggle()
            } label: {
                Image(systemName: configuration.isOn ? "checkmark.circle.fill" : "circle")
                    .foregroundColor(configuration.isOn ? .accentColor : .secondary)
                    .accessibility(label: Text(configuration.isOn ? "Checked" : "Unchecked"))
                    .imageScale(configuration.isOn ? .large : .medium)
            }
        }
    }
}

struct ContentView: View {
    @State private var showAdvanced = false

    var body: some View {
        Toggle("Show Advanced Options", isOn: $showAdvanced)
            .toggleStyle(CheckToggleStyle())
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
