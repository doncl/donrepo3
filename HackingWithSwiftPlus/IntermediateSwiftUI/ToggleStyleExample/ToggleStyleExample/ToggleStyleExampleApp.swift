//
//  ToggleStyleExampleApp.swift
//  ToggleStyleExample
//
//  Created by Don Clore on 7/8/22.
//

import SwiftUI

@main
struct ToggleStyleExampleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
