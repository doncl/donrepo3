//
//  DynamicPropertyPropertyWrapperApp.swift
//  DynamicPropertyPropertyWrapper
//
//  Created by Don Clore on 7/7/22.
//

import SwiftUI

@main
struct DynamicPropertyPropertyWrapperApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
