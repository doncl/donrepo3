//
//  ProgressViewStyleExampleApp.swift
//  ProgressViewStyleExample
//
//  Created by Don Clore on 7/8/22.
//

import SwiftUI

@main
struct ProgressViewStyleExampleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
