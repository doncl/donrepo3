//
//  LabelStyleExampleApp.swift
//  LabelStyleExample
//
//  Created by Don Clore on 7/8/22.
//

import SwiftUI

@main
struct LabelStyleExampleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
