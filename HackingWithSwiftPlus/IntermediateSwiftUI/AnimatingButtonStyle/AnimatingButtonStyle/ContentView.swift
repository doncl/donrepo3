//
//  ContentView.swift
//  AnimatingButtonStyle
//
//  Created by Don Clore on 7/8/22.
//

import SwiftUI

protocol AnimatingButtonStyle: ButtonStyle {
    init(animation: Double)
}

struct PulsingButtonStyle: AnimatingButtonStyle {
    let animation: Double

    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding()
            .background(Color.blue)
            .clipShape(Circle())
            .foregroundColor(Color.white)
            .padding(4)
            .overlay(
                Circle()
                    .stroke(Color.blue, lineWidth: 2)
                    .scaleEffect(CGFloat(1 + animation))
                    .opacity(1 - animation)
            )
    }
}

struct SpinningArcsButtonStyle: AnimatingButtonStyle {
    let animation: Double

    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding()
            .background(Color.blue)
            .clipShape(Circle())
            .foregroundColor(.white)
            .padding(4)
            .overlay(
                Circle()
                    .trim(from: 0, to: 0.5)
                    .stroke(Color.blue, lineWidth: 4)
                    .rotationEffect(.init(degrees: -animation * 360))
            )
            .padding(6)
            .overlay(
                Circle()
                    .trim(from: 0, to: 0.5)
                    .stroke(Color.blue, lineWidth: 4)
                    .rotationEffect(.init(degrees: animation * 360))
            )
    }
}

struct AnimatedButton<ButtonStyle: AnimatingButtonStyle, Content: View>: View {
    let buttonStyle: ButtonStyle.Type
    var animationSpeed: Double = 5.0
    let action: () -> Void
    let label: () -> Content

    @State private var animation = 0.0

    var body: some View {
        Button(action: action, label: label)
            .buttonStyle(buttonStyle.init(animation: animation))
            .onAppear {
                withAnimation(Animation.easeOut(duration: animationSpeed).repeatForever(autoreverses: false)) {
                    animation = 1
                }
            }
    }
}

struct ContentView: View {
    @State private var animation = 0.0

    var body: some View {
        AnimatedButton(buttonStyle: SpinningArcsButtonStyle.self, animationSpeed: 5) {
            print("Pressed")
        } label: {
            Image(systemName: "star")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
