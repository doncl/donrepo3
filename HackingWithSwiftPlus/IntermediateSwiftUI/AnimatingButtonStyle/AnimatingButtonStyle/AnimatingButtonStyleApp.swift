//
//  AnimatingButtonStyleApp.swift
//  AnimatingButtonStyle
//
//  Created by Don Clore on 7/8/22.
//

import SwiftUI

@main
struct AnimatingButtonStyleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
