//
//  ContentView.swift
//  PrimitiveButtonStyleExample
//
//  Created by Don Clore on 7/8/22.
//

import Combine
import SwiftUI

#if DEBUG
    typealias CustomButtonType = DebugButtonStyle
#else
    typealias CustomButtonType = DefaultButtonStyle
#endif

struct CancellableButtonStyle: PrimitiveButtonStyle {
    private struct CancellableButton: View {
        @State private var timerSubscription: Cancellable?
        @State private var timer = Timer.publish(every: 1, on: .main, in: .common)
        @State private var countDown = 0

        let configuration: Configuration
        let timeOut: Int

        var body: some View {
            Button {
                if timerSubscription == nil {
                    timer = Timer.publish(every: 1, on: .main, in: .common)
                    timerSubscription = timer.connect()
                    countDown = timeOut
                } else {
                    cancelTimer()
                }
            } label: {
                if timerSubscription == nil {
                    configuration.label
                } else {
                    Text("Cancel? \(countDown)")
                }
            }
            .onReceive(timer) { _ in
                if countDown > 1 {
                    countDown -= 1
                } else {
                    configuration.trigger()
                    cancelTimer()
                }
            }
        }

        func cancelTimer() {
            timerSubscription?.cancel()
            timerSubscription = nil
        }
    }

    var timeOut = 3

    func makeBody(configuration: Configuration) -> some View {
        CancellableButton(configuration: configuration, timeOut: timeOut)
    }
}

struct DefaultButtonStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding()
            .background(Color.blue)
            .foregroundColor(.white)
            .clipShape(Capsule())
            .opacity(configuration.isPressed ? 0.7 : 1)
    }
}

struct DebugButtonStyle: PrimitiveButtonStyle {
    let location: String

    func makeBody(configuration: Configuration) -> some View {
        Button {
            print("Button pressed on line \(location)")
            configuration.trigger()
        } label: {
            configuration.label
        }
        .buttonStyle(DefaultButtonStyle())
    }

    init(file: String = #file, line: Int = #line) {
        location = "\(line) in \(file)"
    }
}

struct ContentView: View {
    var body: some View {
        Button {
            print("Pressed")
        } label: {
            Text("Press Me")
        }
        .buttonStyle(CancellableButtonStyle())
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
