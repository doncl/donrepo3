//
//  PrimitiveButtonStyleExampleApp.swift
//  PrimitiveButtonStyleExample
//
//  Created by Don Clore on 7/8/22.
//

import SwiftUI

@main
struct PrimitiveButtonStyleExampleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
