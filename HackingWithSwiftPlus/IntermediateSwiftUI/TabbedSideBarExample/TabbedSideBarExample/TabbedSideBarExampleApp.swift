//
//  TabbedSideBarExampleApp.swift
//  TabbedSideBarExample
//
//  Created by Don Clore on 7/8/22.
//

import SwiftUI

@main
struct TabbedSideBarExampleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
