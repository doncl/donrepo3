//
//  ContentView.swift
//  TabbedSideBarExample
//
//  Created by Don Clore on 7/8/22.
//

import SwiftUI

struct TitledView {
    let title: String
    let icon: Image
    let view: AnyView

    init<T: View>(title: String, systemImage: String, view: T) {
        self.title = title
        icon = Image(systemName: systemImage)
        self.view = AnyView(view)
    }
}

struct TabbedSidebar: View {
    @Environment(\.horizontalSizeClass) var sizeClass

    @State private var selection: String? = ""

    private let views: [TitledView]

    var body: some View {
        if sizeClass == .compact {
            TabView(selection: $selection) {
                ForEach(views, id: \.title) { item in
                    item.view
                        .tabItem {
                            Text(item.title)
                            item.icon
                        }
                        .tag(item.title)
                }
            }
        } else {
            NavigationView {
                List(selection: $selection) {
                    ForEach(views, id: \.title) { item in
                        NavigationLink(destination: item.view, tag: item.title, selection: $selection) {
                            Label {
                                Text(item.title)
                            } icon: {
                                item.icon
                            }
                        }
                    }
                }
                .listStyle(SidebarListStyle())
            }
        }
    }

    init(content: [TitledView]) {
        views = content
        _selection = State(wrappedValue: content[0].title)
    }
}

struct ContentView: View {
    var body: some View {
        TabbedSidebar(content: [
            TitledView(title: "Home", systemImage: "house", view: Text("Home")),
            TitledView(title: "Buy", systemImage: "cart", view: Text("Buy")),
            TitledView(title: "Account", systemImage: "person.circle", view: Text("Account")),
        ])
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
