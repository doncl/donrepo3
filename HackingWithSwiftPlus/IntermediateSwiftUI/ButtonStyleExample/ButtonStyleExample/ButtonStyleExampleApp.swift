//
//  ButtonStyleExampleApp.swift
//  ButtonStyleExample
//
//  Created by Don Clore on 7/8/22.
//

import SwiftUI

@main
struct ButtonStyleExampleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
