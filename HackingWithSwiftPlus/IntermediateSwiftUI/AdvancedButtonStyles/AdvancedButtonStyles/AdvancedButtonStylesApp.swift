//
//  AdvancedButtonStylesApp.swift
//  AdvancedButtonStyles
//
//  Created by Don Clore on 7/8/22.
//

import SwiftUI

@main
struct AdvancedButtonStylesApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
