//
//  ContentView.swift
//  HWSPTips
//
//  Created by Don Clore on 9/15/22.
//

import SwiftUI
import UIKit

struct ContentView: View {
    var body: some View {
        NavigationView {
            ListingView()
                .navigationTitle("Collections")
        }
        .onAppear {
            UITableView.appearance().backgroundColor = UIColor(named: "Background")!
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
