//
//  HWSPTipsApp.swift
//  HWSPTips
//
//  Created by Don Clore on 9/15/22.
//

import SwiftUI

@main
struct HWSPTipsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
