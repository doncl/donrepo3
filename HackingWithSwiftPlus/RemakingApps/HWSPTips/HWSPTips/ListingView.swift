//
//  ListingView.swift
//  HWSPTips
//
//  Created by Don Clore on 9/15/22.
//

import SwiftUI

struct ListingView: View {
    var body: some View {
        List {
            Section(
                header: NavigationLink(destination: DetailView()) {
                    ZStack(alignment: .leading) {
                        Image("iOS14")
                            .renderingMode(.original)
                            .resizable()
                            .scaledToFill()
                            .frame(height: 250)
                            .clipShape(RoundedRectangle(cornerRadius: 10))

                        VStack(alignment: .leading) {
                            Text("What's New")
                                .font(.title)
                                .bold()
                                .foregroundColor(.white)

                            Text("2 tips")
                                .foregroundColor(Color.white.opacity(0.8))
                        }
                        .textCase(.none)
                        .offset(x: 15, y: 10)
                    }
                }
            ) {
                ForEach(0 ..< 30) { _ in
                    NavigationLink(destination: Text("Detail View")) {
                        ListingRow()
                    }
                    .listRowBackground(Color("Background"))
                }
            }
        }
        .listStyle(GroupedListStyle())
    }
}

struct ListingView_Previews: PreviewProvider {
    static var previews: some View {
        ListingView()
    }
}
