//
//  HWSPWeatherApp.swift
//  HWSPWeather
//
//  Created by Don Clore on 7/31/22.
//

import SwiftUI

@main
struct HWSPWeatherApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
