//
//  LightningBolt.swift
//  HWSPWeather
//
//  Created by Don Clore on 9/12/22.
//

import SwiftUI

class LightningBolt {
    var points: [CGPoint] = []
    var width: Double
    var angle: Double

    init(start: CGPoint, width: Double, angle: Double) {
        points.append(start)
        self.width = width
        self.angle = angle
    }
}
