//
//  Cloud.swift
//  HWSPWeather
//
//  Created by Don Clore on 7/31/22.
//

import SwiftUI

class Cloud {
    enum Thickness: CaseIterable {
        case none
        case thin
        case light
        case regular
        case thick
        case ultra
    }

    var position: CGPoint
    let imageNumber: Int
    let speed = Double.random(in: 4 ... 12)
    let scale: Double

    init(imageNumber: Int, scale: Double) {
        self.imageNumber = imageNumber
        self.scale = scale

        let startX = Double.random(in: -400 ... 400)
        let startY = Double.random(in: -50 ... 200)
        position = CGPoint(x: startX, y: startY)
    }
}
