//
//  Feature.swift
//  HWSPWelcome
//
//  Created by Don Clore on 9/15/22.
//

import SwiftUI

struct Feature: Decodable, Identifiable {
    var id = UUID()
    let title: String
    let description: String
    let image: String
}
