//
//  HWSPWelcomeApp.swift
//  HWSPWelcome
//
//  Created by Don Clore on 9/15/22.
//

import SwiftUI

@main
struct HWSPWelcomeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
                .accentColor(Color(red: 0.9, green: 0, blue: 0, opacity: 1))
        }
    }
}
