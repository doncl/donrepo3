//
//  LockScreenButton.swift
//  HWSPLockScreen
//
//  Created by Don Clore on 9/15/22.
//

import SwiftUI

struct LockScreenButton: View {
    @State private var pressed = false
    @State private var activated = false

    let image: String

    var body: some View {
        Image(systemName: image)
            .font(.title3)
            .frame(width: 50, height: 50)
            .background(activated ? Color.white : Color.black.opacity(pressed ? 0.8 : 0.4))
            .foregroundColor(activated ? .black : .white)
            .clipShape(Circle())
            .scaleEffect(pressed ? 1.5 : 1)
            .animation(.spring(response: 0.5, dampingFraction: 0.7))
            .onLongPressGesture(minimumDuration: 0.4) { bool in
                pressed = bool
            } perform: {
                let generator = UIImpactFeedbackGenerator()
                generator.impactOccurred()
                activated.toggle()
                pressed = false
            }
    }
}

struct LockScreenButton_Previews: PreviewProvider {
    static var previews: some View {
        LockScreenButton(image: "camera.fill")
    }
}
