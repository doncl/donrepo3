//
//  HWSPLockScreenApp.swift
//  HWSPLockScreen
//
//  Created by Don Clore on 9/15/22.
//

import SwiftUI

@main
struct HWSPLockScreenApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
