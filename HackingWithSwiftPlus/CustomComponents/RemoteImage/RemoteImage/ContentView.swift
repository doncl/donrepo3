//
//  ContentView.swift
//  RemoteImage
//
//  Created by Don Clore on 7/7/22.
//

import SwiftUI

struct RemoteImage: View {
    enum LoadState {
        case loading
        case success
        case failure
    }

    @StateObject private var loader: Loader
    var loading: Image
    var failure: Image

    var body: some View {
        selectImage()
            .resizable()
    }

    init(url: String, loading: Image = Image(systemName: "photo"), failure: Image = Image(systemName: "multiply.circle")) {
        _loader = StateObject(wrappedValue: Loader(url: url))
        self.loading = loading
        self.failure = failure
    }

    private func selectImage() -> Image {
        switch loader.state {
        case .loading:
            return loading

        case .failure:
            return failure

        default:
            if let image = UIImage(data: loader.data) {
                return Image(uiImage: image)
            } else {
                return failure
            }
        }
    }
}

struct ContentView: View {
    var body: some View {
        RemoteImage(url: "https://www.hackingwithswift.com/img/app-store@2x.png")
            .aspectRatio(contentMode: .fit)
            .frame(width: 200)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

extension RemoteImage {
    class Loader: ObservableObject {
        var data = Data()
        var state = LoadState.loading

        init(url: String) {
            guard let parsedURL = URL(string: url) else {
                fatalError("Invalid URL: \(url)")
            }

            URLSession.shared.dataTask(with: parsedURL) { data, _, _ in
                DispatchQueue.main.async {
                    if let data = data, data.count > 0 {
                        self.data = data
                        self.state = .success
                    } else {
                        self.state = .failure
                    }
                }
                DispatchQueue.main.async {
                    self.objectWillChange.send()
                }
            }
            .resume()
        }
    }
}
