//
//  RemoteImageApp.swift
//  RemoteImage
//
//  Created by Don Clore on 7/7/22.
//

import SwiftUI

@main
struct RemoteImageApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
