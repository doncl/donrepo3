//
//  FlipViewApp.swift
//  FlipView
//
//  Created by Don Clore on 7/6/22.
//

import SwiftUI

@main
struct FlipViewApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
