//
//  ContentView.swift
//  FlipView
//
//  Created by Don Clore on 7/6/22.
//

import SwiftUI

struct FlipView<Front: View, Back: View>: View {
    var isFlipped: Bool

    var front: () -> Front
    var back: () -> Back

    var body: some View {
        ZStack {
            front()
                .rotation3DEffect(Angle.degrees(isFlipped == true ? 180 : 0), axis: (x: 0, y: 1, z: 0))
                .opacity(isFlipped == true ? 0 : 1)
                .accessibility(hidden: isFlipped == true)

            back()
                .rotation3DEffect(Angle.degrees(isFlipped == true ? 0 : -180), axis: (x: 0, y: 1, z: 0))
                .opacity(isFlipped == true ? 1 : -1)
                .accessibility(hidden: isFlipped == false)
        }
    }

    init(isFlipped: Bool = false, @ViewBuilder front: @escaping () -> Front, @ViewBuilder back: @escaping () -> Back) {
        self.isFlipped = isFlipped
        self.front = front
        self.back = back
    }
}

struct ContentView: View {
    @State private var cardFlipped: Bool = false

    var body: some View {
        FlipView(isFlipped: cardFlipped) {
            Text("Font Side")
                .font(.largeTitle)
                .padding()
                .background(Color.yellow)
                .clipShape(RoundedRectangle(cornerRadius: 25))
        } back: {
            Text("Back Side")
                .font(.largeTitle)
                .foregroundColor(Color.white)
                .padding()
                .background(Color.red)
                .clipShape(RoundedRectangle(cornerRadius: 25))
        }
        .animation(Animation.spring(response: 0.35, dampingFraction: 0.7), value: cardFlipped)
        .onTapGesture {
            cardFlipped.toggle()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
