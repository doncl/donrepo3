//
//  ContentView.swift
//  StretchingHeader
//
//  Created by Don Clore on 7/7/22.
//

import SwiftUI

struct StretchingHeader<Content: View>: View {
    let content: () -> Content

    var body: some View {
        GeometryReader { geo in
            content()
                .frame(width: geo.size.width, height: height(for: geo))
                .offset(y: offset(for: geo))
        }
    }

    func offset(for proxy: GeometryProxy) -> CGFloat {
        let y = proxy.frame(in: .global).minY
        return min(0, -y)
    }

    func height(for proxy: GeometryProxy) -> CGFloat {
        let y = proxy.frame(in: .global).minY
        return proxy.size.height + max(0, y)
    }
}

struct ContentView: View {
    var body: some View {
        ScrollView {
            VStack {
                StretchingHeader {
                    Image("waterfall")
                        .resizable()
                        .scaledToFill()
                }
                .frame(height: 200)

                Text("Photo by David Klaasen")
                    .font(.title)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
