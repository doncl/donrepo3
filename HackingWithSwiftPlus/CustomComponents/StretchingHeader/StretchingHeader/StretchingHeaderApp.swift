//
//  StretchingHeaderApp.swift
//  StretchingHeader
//
//  Created by Don Clore on 7/7/22.
//

import SwiftUI

@main
struct StretchingHeaderApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
