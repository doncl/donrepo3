//
//  ContentView.swift
//  FilteringListExample
//
//  Created by Paul Hudson on 06/06/2020.
//  Copyright © 2020 Paul Hudson. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    let users = Bundle.main.decode([User].self, from: "users.json")
    @State private var filteredItems: [User] = []
    @State private var filterString: String = ""

    var body: some View {
        NavigationView {
            FilteringList(users, filterKeys: \.name, \.address) { user in
                VStack(alignment: .leading) {
                    Text(user.name)
                        .font(.headline)
                    Text(user.address)
                        .foregroundColor(.secondary)
                }
            }
        }
        .navigationBarTitle("Address Book")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

extension Binding {
    func onChange(_ handler: @escaping () -> Void) -> Binding<Value> {
        Binding(
            get: { self.wrappedValue },
            set: { newValue in
                self.wrappedValue = newValue
                handler()
            }
        )
    }
}
