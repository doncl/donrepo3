//
//  FilteringList.swift
//  FilteringListExample
//
//  Created by Don Clore on 7/5/22.
//  Copyright © 2022 Paul Hudson. All rights reserved.
//

import SwiftUI

struct FilteringList<T: Identifiable, Content: View>: View {
    @State private var filteredItems: [T] = []
    @State private var filterString: String = ""

    let listItems: [T]
    let filterKeyPaths: [KeyPath<T, String>]
    let content: (T) -> Content

    var body: some View {
        VStack {
            TextField("Type fo filter", text: $filterString.onChange(applyFilter))
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding(.horizontal)

            List(filteredItems, rowContent: content)
                .onAppear {
                    applyFilter()
                }
        }
    }

    init(_ data: [T], filterKeys: KeyPath<T, String>..., @ViewBuilder rowContent: @escaping (T) -> Content) {
        listItems = data
        filterKeyPaths = filterKeys
        content = rowContent
    }

    private func applyFilter() {
        let cleanedFilter: String = filterString.trimmingCharacters(in: .whitespacesAndNewlines)

        if cleanedFilter.isEmpty {
            filteredItems = listItems
        } else {
            filteredItems = listItems.filter { element in
                filterKeyPaths.contains {
                    element[keyPath: $0]
                        .localizedCaseInsensitiveContains(cleanedFilter)
                }
            }
        }
    }
}
