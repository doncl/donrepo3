//
//  ContentView.swift
//  LongPressButton
//
//  Created by Don Clore on 7/6/22.
//

import SwiftUI

struct LongPressButton: View {
    @GestureState private var pressed = false

    let image: Image
    var backgroundColor: Color = .clear
    var foregroundColor: Color = .primary
    var strokeColor: Color = .red
    var strokeWidth: CGFloat = 6
    var action: () -> Void

    var body: some View {
        image
            .font(.title)
            .foregroundColor(foregroundColor)
            .accessibility(addTraits: .isButton)
            .accessibility(removeTraits: .isImage)
            .padding()
            .background(
                Circle()
                    .fill(backgroundColor)
            )
            .overlay(
                Circle()
                    .rotation(Angle(degrees: -90))
                    .trim(from: 0, to: pressed ? 1 : 0)
                    .stroke(strokeColor, style: StrokeStyle(lineWidth: strokeWidth, lineCap: .round))
            )
            .gesture(
                LongPressGesture(minimumDuration: 0.5)
                    .updating($pressed) { new, existing, _ in
                        existing = new
                    }
                    .onEnded { _ in
                        action()
                    }
            )
            .animation(.linear)
    }
}

struct ContentView: View {
    var body: some View {
        LongPressButton(image: Image(systemName: "bolt.fill")) {
            print("Activated!")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
