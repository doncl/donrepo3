//
//  LongPressButtonApp.swift
//  LongPressButton
//
//  Created by Don Clore on 7/6/22.
//

import SwiftUI

@main
struct LongPressButtonApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
