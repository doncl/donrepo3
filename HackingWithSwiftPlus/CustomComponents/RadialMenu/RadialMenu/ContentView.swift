//
//  ContentView.swift
//  RadialMenu
//
//  Created by Don Clore on 7/7/22.
//

import SwiftUI

struct RadialButton {
    var label: String
    var image: Image
    var action: () -> Void
}

struct RadialMenu: View {
    @State private var isExpanded: Bool = false
    @State private var isShowingSheet: Bool = false

    var title: String
    let closedImage: Image
    let openImage: Image
    let buttons: [RadialButton]
    var direction = Angle(degrees: 315)
    var range = Angle(degrees: 90)
    var distance = 100.0
    var animation = Animation.default

    var body: some View {
        ZStack {
            Button {
                if UIAccessibility.isVoiceOverRunning {
                    isShowingSheet.toggle()
                } else {
                    isExpanded.toggle()
                }
            } label: {
                isExpanded ? openImage : closedImage
            }
            .accessibility(label: Text(title))

            ForEach(0 ..< buttons.count, id: \.self) { i in
                Button {
                    buttons[i].action()
                    isExpanded.toggle()
                } label: {
                    buttons[i].image
                        .resizable()
                        .frame(width: 20, height: 20)
                }
                .frame(width: 20, height: 20)
                .fixedSize()
                .accessibility(hidden: isExpanded == false)
                .accessibility(label: Text(buttons[i].label))
                .offset(offset(for: i))
            }
            .opacity(isExpanded ? 1 : 0)
            .animation(animation, value: isExpanded)
        }
        .actionSheet(isPresented: $isShowingSheet) {
            ActionSheet(title: Text(title), message: nil, buttons:
                buttons.map { btn in
                    ActionSheet.Button.default(Text(btn.label), action: btn.action)
                } + [.cancel()])
        }
    }

    func offset(for index: Int) -> CGSize {
        guard isExpanded else {
            return CGSize.zero
        }

        let buttonAngle = range.radians / Double(buttons.count - 1)

        let ourAngle = buttonAngle * Double(index)
        let finalAngle = direction - (range / 2) + Angle(radians: ourAngle)

        let finalX = cos(finalAngle.radians - .pi / 2) * distance
        let finalY = sin(finalAngle.radians - .pi / 2) * distance

        return CGSize(width: finalX, height: finalY)
    }
}

struct ContentView: View {
    var buttons: [RadialButton] {
        [
            RadialButton(label: "Photo", image: Image(systemName: "photo"), action: photoTapped),
            RadialButton(label: "Video", image: Image(systemName: "video"), action: videoTapped),
            RadialButton(label: "Document", image: Image(systemName: "doc"), action: documentTapped),
            RadialButton(label: "Photo", image: Image(systemName: "photo"), action: photoTapped),
            RadialButton(label: "Video", image: Image(systemName: "video"), action: videoTapped),
            RadialButton(label: "Document", image: Image(systemName: "doc"), action: documentTapped),

            RadialButton(label: "Photo", image: Image(systemName: "photo"), action: photoTapped),
            RadialButton(label: "Video", image: Image(systemName: "video"), action: videoTapped),
            RadialButton(label: "Document", image: Image(systemName: "doc"), action: documentTapped),
            RadialButton(label: "Photo", image: Image(systemName: "photo"), action: photoTapped),
            RadialButton(label: "Video", image: Image(systemName: "video"), action: videoTapped),
            RadialButton(label: "Document", image: Image(systemName: "doc"), action: documentTapped),
        ]
    }

    var body: some View {
        GeometryReader { geo in
            ZStack(alignment: .center) {
                Color.black
                    .edgesIgnoringSafeArea(.all)

                RadialMenu(title: "Attach...",
                           closedImage: Image(systemName: "ellipsis.circle"),
                           openImage: Image(systemName: "multiply.circle.fill"),
                           buttons: buttons,
                           range: Angle(degrees: 360),
                           distance: 0.35 * min(geo.size.width, geo.size.height),
                           animation: Animation.interactiveSpring(response: 0.4, dampingFraction: 0.6))
                    .buttonStyle(CustomButtonStyle())
            }
        }
    }

    func photoTapped() {
        print("Photo tapped")
    }

    func videoTapped() {
        print("Video tapped")
    }

    func documentTapped() {
        print("Document tapped")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct CustomButtonStyle: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding()
            .font(.title)
            .background(Color.blue.opacity(configuration.isPressed ? 0.5 : 1))
            .clipShape(Circle())
            .foregroundColor(.white)
    }
}
