//
//  RadialMenuApp.swift
//  RadialMenu
//
//  Created by Don Clore on 7/7/22.
//

import SwiftUI

@main
struct RadialMenuApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
