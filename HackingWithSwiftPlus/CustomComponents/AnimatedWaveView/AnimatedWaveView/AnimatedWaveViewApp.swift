//
//  AnimatedWaveViewApp.swift
//  AnimatedWaveView
//
//  Created by Don Clore on 7/6/22.
//

import SwiftUI

@main
struct AnimatedWaveViewApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
