//
//  ContentView.swift
//  ShapeViewExample
//
//  Created by Paul Hudson on 31/05/2020.
//  Copyright © 2020 Paul Hudson. All rights reserved.
// Syhy

import SwiftUI

struct ShapeView: Shape {
    let bezier: UIBezierPath

    func path(in rect: CGRect) -> Path {
        let path = Path(bezier.cgPath)
        let multiplier = min(rect.width, rect.height)
        let transform = CGAffineTransform(scaleX: multiplier, y: multiplier)
        return path.applying(transform)
    }
}

struct ContentView: View {
    static let gradientColor: Color = .init(red: 0.392, green: 0.188, blue: 1.000)
    static let gradientColor2: Color = .init(red: 1.000, green: 0.420, blue: 0.290)
    static let gradient: LinearGradient = .init(colors: [gradientColor, gradientColor2], startPoint: UnitPoint(x: 0.5, y: 0), endPoint: UnitPoint(x: 0.5, y: 0.6))

    @State private var endAmount: CGFloat = 0

    var body: some View {
        ZStack {
            ShapeView(bezier: UIBezierPath.viddlLogo)
                .trim(from: 0, to: endAmount)
                .stroke(Color.blue, lineWidth: 4)
                .frame(width: 300, height: 300)
                .onAppear {
                    withAnimation(Animation.easeInOut(duration: 3).repeatForever(autoreverses: true)) {
                        self.endAmount = 1
                    }
                }

            ShapeView(bezier: UIBezierPath.viddlLogo)
                .trim(from: 0, to: endAmount)
                .fill(ContentView.gradient)
                .frame(width: 300, height: 300)
                .onAppear {
                    withAnimation(Animation.easeInOut(duration: 3).repeatForever(autoreverses: true)) {
                        self.endAmount = 1
                    }
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

extension UIBezierPath {
    static var unwrapLogo: UIBezierPath {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0.534, y: 0.5816))
        path.addCurve(to: CGPoint(x: 0.1877, y: 0.088), controlPoint1: CGPoint(x: 0.534, y: 0.5816), controlPoint2: CGPoint(x: 0.2529, y: 0.4205))
        path.addCurve(to: CGPoint(x: 0.9728, y: 0.8259), controlPoint1: CGPoint(x: 0.4922, y: 0.4949), controlPoint2: CGPoint(x: 1.0968, y: 0.4148))
        path.addCurve(to: CGPoint(x: 0.0397, y: 0.5431), controlPoint1: CGPoint(x: 0.7118, y: 0.5248), controlPoint2: CGPoint(x: 0.3329, y: 0.7442))
        path.addCurve(to: CGPoint(x: 0.6211, y: 0.0279), controlPoint1: CGPoint(x: 0.508, y: 1.1956), controlPoint2: CGPoint(x: 1.3042, y: 0.5345))
        path.addCurve(to: CGPoint(x: 0.6904, y: 0.3615), controlPoint1: CGPoint(x: 0.7282, y: 0.2481), controlPoint2: CGPoint(x: 0.6904, y: 0.3615))
        return path
    }

    static var viddlLogo: UIBezierPath {
        let pathPath = UIBezierPath()
        pathPath.move(to: CGPoint(x: 0.85, y: 0.23))
        pathPath.addCurve(to: CGPoint(x: 0.28, y: 0.19), controlPoint1: CGPoint(x: 0.7, y: 0.08), controlPoint2: CGPoint(x: 0.45, y: 0.06))
        pathPath.addCurve(to: CGPoint(x: 0.2, y: 0.74), controlPoint1: CGPoint(x: 0.09, y: 0.32), controlPoint2: CGPoint(x: 0.06, y: 0.57))
        pathPath.addCurve(to: CGPoint(x: 0.8, y: 0.81), controlPoint1: CGPoint(x: 0.35, y: 0.91), controlPoint2: CGPoint(x: 0.61, y: 0.94))
        pathPath.addCurve(to: CGPoint(x: 0.88, y: 0.82), controlPoint1: CGPoint(x: 0.82, y: 0.79), controlPoint2: CGPoint(x: 0.86, y: 0.8))
        pathPath.addCurve(to: CGPoint(x: 0.87, y: 0.9), controlPoint1: CGPoint(x: 0.9, y: 0.84), controlPoint2: CGPoint(x: 0.89, y: 0.88))
        pathPath.addCurve(to: CGPoint(x: 0.11, y: 0.81), controlPoint1: CGPoint(x: 0.63, y: 1.06), controlPoint2: CGPoint(x: 0.3, y: 1.02))
        pathPath.addCurve(to: CGPoint(x: 0.21, y: 0.1), controlPoint1: CGPoint(x: -0.07, y: 0.59), controlPoint2: CGPoint(x: -0.03, y: 0.27))
        pathPath.addCurve(to: CGPoint(x: 0.97, y: 0.19), controlPoint1: CGPoint(x: 0.44, y: -0.06), controlPoint2: CGPoint(x: 0.78, y: -0.02))
        pathPath.addCurve(to: CGPoint(x: 0.97, y: 0.26), controlPoint1: CGPoint(x: 0.98, y: 0.21), controlPoint2: CGPoint(x: 0.98, y: 0.24))
        pathPath.addLine(to: CGPoint(x: 0.64, y: 0.71))
        pathPath.addCurve(to: CGPoint(x: 0.6, y: 0.73), controlPoint1: CGPoint(x: 0.63, y: 0.72), controlPoint2: CGPoint(x: 0.61, y: 0.73))
        pathPath.addCurve(to: CGPoint(x: 0.55, y: 0.71), controlPoint1: CGPoint(x: 0.58, y: 0.73), controlPoint2: CGPoint(x: 0.56, y: 0.72))
        pathPath.addLine(to: CGPoint(x: 0.34, y: 0.36))
        pathPath.addCurve(to: CGPoint(x: 0.36, y: 0.29), controlPoint1: CGPoint(x: 0.32, y: 0.34), controlPoint2: CGPoint(x: 0.33, y: 0.3))
        pathPath.addCurve(to: CGPoint(x: 0.43, y: 0.31), controlPoint1: CGPoint(x: 0.38, y: 0.27), controlPoint2: CGPoint(x: 0.42, y: 0.28))
        pathPath.addLine(to: CGPoint(x: 0.6, y: 0.58))
        pathPath.addLine(to: CGPoint(x: 0.85, y: 0.23))
        pathPath.close()
        pathPath.usesEvenOddFillRule = true

        return pathPath
    }
}
