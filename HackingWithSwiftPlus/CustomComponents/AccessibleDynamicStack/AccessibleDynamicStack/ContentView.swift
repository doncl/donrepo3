//
//  ContentView.swift
//  AccessibleDynamicStack
//
//  Created by Don Clore on 7/7/22.
//

import SwiftUI

struct AccessibleStack<Content: View>: View {
    @Environment(\.sizeCategory) var size

    var spacing: CGFloat?
    var horizontalAlignment: HorizontalAlignment
    var verticalAlignment: VerticalAlignment
    var verticalStartSize: ContentSizeCategory

    let content: () -> Content

    var body: some View {
        if size >= verticalStartSize {
            VStack(alignment: horizontalAlignment, spacing: spacing, content: content)
        } else {
            HStack(alignment: verticalAlignment, spacing: spacing, content: content)
        }
    }

    init(horizontalAlignment: HorizontalAlignment = .center, verticalAlignment: VerticalAlignment = .center, spacing: CGFloat? = nil, verticalStartSize: ContentSizeCategory = .accessibilityMedium, @ViewBuilder content: @escaping () -> Content) {
        self.horizontalAlignment = horizontalAlignment
        self.verticalAlignment = verticalAlignment
        self.spacing = spacing
        self.verticalStartSize = verticalStartSize
        self.content = content
    }
}

struct ContentView: View {
    var body: some View {
        AccessibleStack(spacing: 10) {
            Text("This is label1")
            Text("This is label2")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView()
                .environment(\.sizeCategory, .extraLarge)
        }
    }
}
