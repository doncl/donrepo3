//
//  AccessibleDynamicStackApp.swift
//  AccessibleDynamicStack
//
//  Created by Don Clore on 7/7/22.
//

import SwiftUI

@main
struct AccessibleDynamicStackApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
