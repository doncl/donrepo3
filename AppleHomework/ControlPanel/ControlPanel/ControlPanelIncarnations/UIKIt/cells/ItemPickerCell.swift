//
//  ItemPickerCell.swift
//  ControlPanel
//
//  Created by Don Clore on 8/24/21.
//

import UIKit

class ItemPickerCell: UITableViewCell {
    enum Constants {
        static let leadingPad: CGFloat = 16.0
        static let flagWidth: CGFloat = 16.0
        static let flagHeight: CGFloat = 24.0
        static let vertPad: CGFloat = 8.0
    }

    static let id: String = "ItemPickerCellID"

    var text: String = "" {
        didSet {
            itemLabel.text = text
            itemLabel.sizeToFit()
        }
    }

    let itemLabel = UILabel()
    let flagImage = UIImageView(image: UIImage(named: "Flag")!)

    func select(_ selected: Bool) {
        flagImage.isHidden = !selected
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        selectionStyle = .none

        [flagImage, itemLabel].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview($0)
        }

        itemLabel.font = UIFont.preferredFont(forTextStyle: .title1)

        flagImage.contentMode = .scaleAspectFit

        NSLayoutConstraint.activate([
            flagImage.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Constants.leadingPad),
            flagImage.widthAnchor.constraint(equalToConstant: Constants.flagWidth),
            flagImage.heightAnchor.constraint(equalToConstant: Constants.flagHeight),
            flagImage.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),

            itemLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Constants.vertPad),
            itemLabel.leadingAnchor.constraint(equalTo: flagImage.trailingAnchor, constant: Constants.leadingPad),
            itemLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),

            // self-sizing cell.
            contentView.bottomAnchor.constraint(equalTo: itemLabel.bottomAnchor, constant: Constants.vertPad),
        ])

        flagImage.isHidden = true
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
