//
//  ListWithPropertyPanel.swift
//  ControlPanel
//
//  Created by Don Clore on 8/23/21.
//

import SwiftUI

struct ListWithPropertyPanel: View {
    @State var selectedItemIndex: Int = 0

    var body: some View {
        HStack(alignment: .top) {
            ItemPicker(currentItemIndex: $selectedItemIndex)
                .frame(maxWidth: .infinity, maxHeight: .infinity)

            Spacer()
            PropertyPanel(item: API.shared.allItems()[selectedItemIndex])
        }
        .padding()
    }
}

struct ListWithPropertyPanel_Previews: PreviewProvider {
    static var previews: some View {
        ListWithPropertyPanel(selectedItemIndex: 0)
            .preferredColorScheme(.light)
            .previewLayout(.fixed(width: 896, height: 414))

        ListWithPropertyPanel(selectedItemIndex: 0)
            .preferredColorScheme(.dark)
            .previewLayout(.fixed(width: 896, height: 414))
    }
}
