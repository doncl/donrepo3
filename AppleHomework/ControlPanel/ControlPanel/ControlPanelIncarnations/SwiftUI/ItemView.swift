//
//  ItemView.swift
//  ControlPanel
//
//  Created by Don Clore on 8/23/21.
//

import SwiftUI

struct ItemView: View {
    var item: Item

    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Text("Name: ")
                    .font(Font.system(.title))

                Text(item.name)
                    .font(Font.system(.title2))
            }

            HStack {
                Text("ID: ")
                    .font(Font.system(.title))
                Text(item.id)
                    .font(Font.system(.callout))
                    .allowsTightening(true)
            }
        }
    }
}

struct ItemView_Previews: PreviewProvider {
    static var item = Item(name: "Fake Item", id: "b39a059d603d4f2d9665100362abd5c7")
    static var previews: some View {
        Group {
            ItemView(item: item)
                .previewDevice(PreviewDevice(rawValue: "iPhone 12 mini"))

            ItemView(item: item)
                .previewDevice(PreviewDevice(rawValue: "iPhone SE (2nd generation)"))
        }
    }
}
