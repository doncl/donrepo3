//
//  SwiftUIVersionOfAssignment.swift
//  ControlPanel
//
//  Created by Don Clore on 8/24/21.
//

import SwiftUI

/// This is just one level of indirection to demonstraint that ListWithPropertyPanel is a reusable component.
struct SwiftUIVersionOfAssignment: View {
    var body: some View {
        ListWithPropertyPanel()
    }
}

struct SwiftUIVersionOfAssignment_Previews: PreviewProvider {
    static var previews: some View {
        SwiftUIVersionOfAssignment()
            .previewLayout(.fixed(width: 896, height: 414))
    }
}
