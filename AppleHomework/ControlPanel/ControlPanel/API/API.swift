//
//  API.swift
//  ControlPanel
//
//  Created by Don Clore on 8/23/21.
//

import Combine
import Foundation

class Item: ObservableObject, Identifiable {
    @Published var name: String {
        didSet {
            API.shared.itemDirtied(id: id)
        }
    }

    let id: String

    init(name: String, id: String) {
        self.name = name
        self.id = id
    }

    func update(name: String) {
        self.name = name
    }
}

class API: ApiProtocol {
    enum Constants {
        // Every 30 seconds?  Who knows?  This is a requirements discussion with Product Mgmt.  This value kinda depends on a lot of
        // things.
        //  • Is this a shared network database that others are partying on?
        //  • Is it crucial that, in the event of a crash, we don't lose pending writes?  My strategy loses in that case.
        //  • Are there multiple writers?  Is it a last-writer-wins strategy?
        //  • Would you want to poll the database from time to time to see if changes have been made by others?
        //    There's a can o' worms down that path.
        static let updateDBInterval: TimeInterval = 30.0
    }

    // The idea here is that, in theory, we could inject different versons, for whatever reasons, testing, etc.
    static var shared: ApiProtocol = API()
    let uploadInterval: TimeInterval = 5.0

    private var items: [Item] = []

    private var dict: [String: Item] = [:]

    private var dirtyItemIds = Set<String>()
    private weak var timer: Timer?

    // MARK: Existing given methods

    // I didn't use these, as I opted to cache these things in RAM, and update them periodically to the fake DB.
    //
    // My main reason for not doing the surgical updates with delayed callback is that I wanted to use Combine and SwiftUI,
    // and I didn't really want to create a ViewModel layer for this small project.  The SwiftUI Textfield is going to update
    // its bound string content on every keystroke, and this is going to result in updates to the in-memory model.
    // So my theory, for better or worse, was to just update the in-memory cache, retain the ids of the dirty items, and
    // periodically flush to our imaginary db.
    // I suppose this entire strategy could be reduced to the idea of batching updates from the SwiftUI textfield.
    func getName(id: String, callback: @escaping (_: String) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            callback(self.dict[id]!.name)
        }
    }

    func setName(id: String, name: String, callback: @escaping () -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.dict[id]!.name = name
            callback()
        }
    }

    // MARK: Methods I added

    func sendUpdateBatch() {
        let batch = Array(dirtyItemIds.compactMap { dict[$0] })

        guard batch.count > 0 else {
            print("\(#function) no changes made, returning")
            return
        }
        fakeDBUpdateMethod(items: batch) { [weak self] result in
            guard let self = self else {
                return
            }
            switch result {
            case let .success(updatedWinners):
                for updatedItem in updatedWinners {
                    guard let existingItem = self.dict[updatedItem.key] else {
                        continue
                    }
                    existingItem.update(name: existingItem.name)
                }
                self.dirtyItemIds.removeAll()
					// At this juncture, self.dict has updated name values for everything that was updated by the db,
				    // and since Item is a reference type, self.items points to the very same values as self.dict does.
				    // So everything is updated.

            case let .failure(error):
                print("\(#function): SNAP!  - failure, error = \(error)")
                // The notes said, "For simplicity, don't worry about the error cases".
            }
        }
    }

    private func fakeDBUpdateMethod(items: [Item], completion: @escaping ((Result<[String: Item], Error>) -> Void)) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            // Just pretend we called the database, sent our update request, and got back the updated results of
            // whatever the reconciliation strategy of the database is, ACID or not.

            var newFakeDict: [String: Item] = [:]
            for item in items {
                newFakeDict[item.id] = item
            }

            print("\(#function): YAY! Successfully updated \(items.count) items with fresh data from the DB")

            completion(.success(newFakeDict))
        }
    }

    private init() {
        items.append(Item(name: "Item 1", id: "b39a059d603d4f2d9665100362abd5c7"))
        items.append(Item(name: "Item 2", id: "b2b0cc1f2d954a87a58acd92a48dcca4"))
        items.append(Item(name: "Item 3", id: "5e810fce7c8c44bfba5eaba051ca007f"))
        items.append(Item(name: "Item 4", id: "09ff75aa44ff458a939d511689d895a0"))
        items.append(Item(name: "Item 5", id: "bd30f197bd74455981e6dec6391cea59"))

        for item in items {
            dict[item.id] = item
        }

        timer = Timer.scheduledTimer(withTimeInterval: Constants.updateDBInterval, repeats: true) { [weak self] escapingTimer in
            guard let self = self else { return }
            self.handleTimer(escapingTimer)
        }
    }

    @objc func handleTimer(_: Timer) {
        sendUpdateBatch()
    }

    func itemDirtied(id: String) {
        dirtyItemIds.insert(id)
    }

    func allItems() -> [Item] {
        return items
    }
}
