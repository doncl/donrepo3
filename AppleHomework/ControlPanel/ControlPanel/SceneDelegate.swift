//
//  SceneDelegate.swift
//  ControlPanel
//
//  Created by Don Clore on 8/23/21.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo _: UISceneSession, options _: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else {
            return
        }
        let win = UIWindow(windowScene: windowScene)
        window = win
        win.rootViewController = RootViewController()
        win.makeKeyAndVisible()
    }
}
