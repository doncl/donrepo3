//
//  Edge.swift
//  Dijkstra
//
//  Created by Don Clore on 12/3/22.
//

import Foundation

public struct Edge<T> {
    public let source: Vertex<T>
    public let destination: Vertex<T>
    public let weight: Double?
}

extension Edge: Hashable where T: Hashable {}
extension Edge: Equatable where T: Equatable {}
