//
//  Dijkstra.swift
//  Dijkstra
//
//  Created by Don Clore on 12/3/22.
//

import Foundation

public enum Visit<T: Hashable> {
    case start
    case edge(Edge<T>)
}

public class Dijkstra<T: Hashable> {
    public typealias Graph = AdjacencyList<T>

    let graph: Graph

    public init(graph: Graph) {
        self.graph = graph
    }

    public func shortestPath(to destination: Vertex<T>, paths: [Vertex<T>: Visit<T>]) -> [Edge<T>] {
        return route(to: destination, with: paths)
    }

    public func shortestPath(from start: Vertex<T>) -> [Vertex<T>: Visit<T>] {
        var paths: [Vertex<T>: Visit<T>] = [start: .start]

        var priorityQueue = PriorityQueue<Vertex<T>>(sort: { (lhs: Vertex<T>, rhs: Vertex<T>) in
            let lDist: Double = self.distance(to: lhs, with: paths)
            let rDist: Double = self.distance(to: rhs, with: paths)
            return lDist < rDist
        })
        priorityQueue.enqueue(start)

        while !priorityQueue.isEmpty {
            let vertex = priorityQueue.dequeue()!
            print(priorityQueue.heap.elements)

            let edges = graph.edges(from: vertex)
            for edge in edges {
                guard let weight = edge.weight else {
                    continue
                }

                if paths[edge.destination] == nil || distance(to: vertex, with: paths) + weight < distance(to: edge.destination, with: paths) {
                    paths[edge.destination] = .edge(edge)
                    priorityQueue.enqueue(edge.destination)
                }
            }
        }

        return paths
    }

    private func route(to destination: Vertex<T>, with paths: [Vertex<T>: Visit<T>]) -> [Edge<T>] {
        var vertex = destination
        var path: [Edge<T>] = []

        while let visit = paths[vertex], case let .edge(edge) = visit {
            path = [edge] + path
            vertex = edge.source
        }
        return path
    }

    private func distance(to destination: Vertex<T>, with paths: [Vertex<T>: Visit<T>]) -> Double {
        let path = route(to: destination, with: paths)
        let distances = path.compactMap { $0.weight }
        return distances.reduce(0.0, +)
    }
}
