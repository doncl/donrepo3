//
//  main.swift
//  Dijkstra
//
//  Created by Don Clore on 12/3/22.
//

import Foundation

let graph = AdjacencyList<String>()

// let a = graph.createVertex(data: "A")
// let b = graph.createVertex(data: "B")
// let c = graph.createVertex(data: "C")
// let d = graph.createVertex(data: "D")
// let e = graph.createVertex(data: "E")
// let f = graph.createVertex(data: "F")
// let g = graph.createVertex(data: "G")
// let h = graph.createVertex(data: "H")
//
// graph.add(.directed, from: a, to: b, weight: 8)
// graph.add(.directed, from: a, to: f, weight: 9)
// graph.add(.directed, from: a, to: g, weight: 1)
// graph.add(.directed, from: b, to: f, weight: 3)
// graph.add(.directed, from: b, to: e, weight: 1)
// graph.add(.directed, from: f, to: a, weight: 2)
// graph.add(.directed, from: h, to: f, weight: 2)
// graph.add(.directed, from: h, to: g, weight: 5)
// graph.add(.directed, from: g, to: c, weight: 3)
// graph.add(.directed, from: c, to: e, weight: 1)
// graph.add(.directed, from: c, to: b, weight: 3)
// graph.add(.undirected, from: e, to: c, weight: 8)
// graph.add(.directed, from: e, to: b, weight: 1)
// graph.add(.directed, from: e, to: d, weight: 2)

let a = graph.createVertex(data: "A")
let b = graph.createVertex(data: "B")
let c = graph.createVertex(data: "C")
let d = graph.createVertex(data: "D")
let e = graph.createVertex(data: "E")
let w = graph.createVertex(data: "W")
let x = graph.createVertex(data: "X")
let y = graph.createVertex(data: "Y")
let z = graph.createVertex(data: "Z")

graph.addUndirectedEdge(between: a, and: b, weight: 4)
graph.addUndirectedEdge(between: a, and: c, weight: 3)
graph.addUndirectedEdge(between: a, and: d, weight: 2)
graph.addUndirectedEdge(between: a, and: z, weight: 30)

graph.addUndirectedEdge(between: b, and: e, weight: 5)

graph.addUndirectedEdge(between: c, and: e, weight: 5)
graph.addUndirectedEdge(between: c, and: y, weight: 6)

graph.addUndirectedEdge(between: d, and: y, weight: 3)

graph.addUndirectedEdge(between: e, and: w, weight: 2)
graph.addUndirectedEdge(between: e, and: x, weight: 1)

graph.addUndirectedEdge(between: w, and: z, weight: 9)
graph.addUndirectedEdge(between: x, and: y, weight: 5)

let dijkstra = Dijkstra(graph: graph)
let pathsFromA = dijkstra.shortestPath(from: a)
let path = dijkstra.shortestPath(to: z, paths: pathsFromA)

for edge in path {
    print("\(edge.source) --|\(edge.weight ?? 0.0)|--> \(edge.destination)")
}
