//
//  QueueStack.swift
//  BFS
//
//  Created by Don Clore on 12/3/22.
//

import Foundation

public struct QueueStack<T>: Queue {
    private var leftStack: [T] = []
    private var rightStack: [T] = []
    public init() {}

    public var isEmpty: Bool {
        leftStack.isEmpty && rightStack.isEmpty
    }

    public var peek: T? {
        !leftStack.isEmpty ? leftStack.last : rightStack.first
    }

    @discardableResult
    public mutating func enqueue(_ element: T) -> Bool {
        rightStack.append(element)
        return true
    }

    @discardableResult
    public mutating func dequeue() -> T? {
        if leftStack.isEmpty {
            leftStack = rightStack.reversed()
            rightStack.removeAll()
        }
        return leftStack.popLast()
    }
}

extension QueueStack: CustomStringConvertible {
    public var description: String {
        let printList = leftStack.reversed() + rightStack
        return String(describing: printList)
    }
}
