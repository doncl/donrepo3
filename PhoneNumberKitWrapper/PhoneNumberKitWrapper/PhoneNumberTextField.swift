//
//  PhoneNumberTextField.swift
//  PhoneNumberKitWrapper
//
//  Created by Don Clore on 5/23/23.
//

import SwiftUI
import PhoneNumberKit

struct PhoneNumberKitSwiftUI: UIViewRepresentable {
    @Binding var phoneNumber: String
    var textField: PhoneNumberTextField = PhoneNumberTextField(frame: .zero)
    
    func makeUIView(context: Context) -> PhoneNumberTextFieldWrapper {
        let textFieldWrapper = PhoneNumberTextFieldWrapper(phoneNumber: $phoneNumber, textField: textField)
        textFieldWrapper.textField.addTarget(context.coordinator, action: #selector(Coordinator.textFieldDidChange(_:)), for: .editingChanged)
        return textFieldWrapper
    }
    
    func updateUIView(_ uiView: PhoneNumberTextFieldWrapper, context: Context) {
        uiView.textField.text = phoneNumber
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(textField)
    }
    
    class Coordinator: NSObject, UITextFieldDelegate {
        let parent: PhoneNumberTextField
        
        init(_ parent: PhoneNumberTextField) {
            self.parent = parent
        }
        
        @objc func textFieldDidChange(_ textField: UITextField) {
            //parent.phoneNumber = textField.text ?? ""
        }
    }
}

class PhoneNumberTextFieldWrapper: UIView {
    let textField: PhoneNumberTextField
    var phoneNumber: Binding<String>
    
    init(phoneNumber: Binding<String>, textField: PhoneNumberTextField) {
        self.phoneNumber = phoneNumber
        self.textField = textField
        super.init(frame: .zero)
        
        addSubview(textField)
        textField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            textField.leadingAnchor.constraint(equalTo: leadingAnchor),
            textField.trailingAnchor.constraint(equalTo: trailingAnchor),
            textField.topAnchor.constraint(equalTo: topAnchor),
            textField.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var intrinsicContentSize: CGSize {
        CGSize(width: UIView.noIntrinsicMetric, height: textField.intrinsicContentSize.height)
    }
}
