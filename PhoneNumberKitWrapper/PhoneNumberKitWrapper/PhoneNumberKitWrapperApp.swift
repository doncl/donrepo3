//
//  PhoneNumberKitWrapperApp.swift
//  PhoneNumberKitWrapper
//
//  Created by Don Clore on 5/23/23.
//

import SwiftUI

@main
struct PhoneNumberKitWrapperApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
