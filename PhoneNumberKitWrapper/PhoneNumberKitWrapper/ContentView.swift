//
//  ContentView.swift
//  PhoneNumberKitWrapper
//
//  Created by Don Clore on 5/23/23.
//

import SwiftUI

struct ContentView: View {
    @State private var phoneNumber = ""
    var body: some View {
        VStack {
            PhoneNumberKitSwiftUI(phoneNumber: $phoneNumber)
        }
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
