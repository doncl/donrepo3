//
//  MovieFetcherBuddy.swift
//  TubiTest
//
//  Created by Don Clore on 7/30/22.
//

import Foundation

enum MovieFetcherBuddyError: Error {
    case unsafeDetailsEntity
}

struct MovieFetcherBuddy {
    enum Constants {
        static let movieListURL: URL = .init(string: "https://us-central1-modern-venture-600.cloudfunctions.net/api/movies")!
    }

    private var cache = LRUCache<String, MovieDetails>(size: 5)

    static var shared = MovieFetcherBuddy()

    private init() {}

    // Look, I don't love do-catch, try-catch, semantics, and I think this was a mistake the Swift community made with Swift 3.0.
    // We argued about this for years at Microsoft in the 90s, and concluded that C++ exceptions should be reserved for truly exceptional
    // errors, and not for general error handling.   Similarly, a decade later, Google concluded the same thing, and their C++ engineers are
    // not allowed to use structured exception handling this way.
    //
    // But the C++ community, and the Swift community, and basically all the engineers that have come along since have never heard any of these arguments,
    // and it looks easier and 'modern', to use this style of coding that is basically a non-local goto.
    //
    // But do I have the religion?   No, if I work at a shop that wants to use do-catch, I will do it exactly as the shop does it, without complaint.
    // I don't really think the modern way Swift do-catch works really makes it all that much harder to debug. I'm not terribly enamored of the argument that
    // it 'reduces boilerplate', or 'less ceremony', but what the heck?   I'll do it, no prob.   But if you give me the choice and latitude, as in this
    // homework assignment?   Probably not.   I prefer to have async methods return Result<T, Error>.
    //
    // To be fair, I actually prefer networking calls with callbacks (which seem to be roundly denounced by all engineers in the known universe except me, as
    // being 'callback hell'.  But I'm not here to fight City Hall.  async-await it is.

    func getMovieList() async -> Result<[MovieListItem], Error> {
        let result: Result<[UnsafeMovieListItem], Error> = await Network.shared.getSomething(url: Constants.movieListURL)

        switch result {
        case let .success(unsafeMovieItemList):
            let safeMovieList = unsafeMovieItemList.compactMap { $0.toSafeModel() }
            return .success(safeMovieList)

        case let .failure(error):
            return .failure(error)
        }
    }

    mutating func getMovieDetails(id: String) async -> Result<MovieDetails, Error> {
        if let details = cache.get(atKey: id) {
            return .success(details)
        }

        let url = Constants.movieListURL.appendingPathComponent(id)

        let result: Result<UnsafeMovieDetails, Error> = await Network.shared.getSomething(url: url)

        switch result {
        case let .success(unsafeMovieDetails):

            guard let movieDetails = unsafeMovieDetails.toSafeModel() else {
                return .failure(MovieFetcherBuddyError.unsafeDetailsEntity)
            }
            cache.set(value: movieDetails, forKey: id)
            return .success(movieDetails)

        case let .failure(error):
            return .failure(error)
        }
    }
}
