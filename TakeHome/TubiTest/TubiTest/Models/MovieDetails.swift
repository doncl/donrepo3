//
//  MovieDetails.swift
//  TubiTest
//
//  Created by Don Clore on 7/30/22.
//

import Foundation

struct UnsafeMovieDetails: Codable {
    var title: String?
    var image: String?
    var id: String? // a String?  really? Seems odd, but OK.  What are the uniqueness guarantees, if any?
    var index: Int? // What are the semantics of this guy?

    func toSafeModel() -> MovieDetails? {
        guard let title = title,
              let imageString = image,
              let imageURL = URL(string: imageString.replacingInsecureScheme()),
              let id = id,
              let index = index
        else {
            return nil
        }

        return MovieDetails(title: title, image: imageURL, id: id, index: index)
    }
}

struct MovieDetails {
    let title: String
    let image: URL
    let id: String
    let index: Int
}

extension MovieDetails: Equatable {}
