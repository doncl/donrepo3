//
//  MovieListItem.swift
//  TubiTest
//
//  Created by Don Clore on 7/30/22.
//

import Foundation

// Class or struct?   If we were doing SwiftUI/Combine, it'd be a class.  For UIKit....it depends on what type semantics
// we need.  This project isn't complex enough that one can clearly discern the requirement.

class UnsafeMovieListItem: Codable {
    var title: String?
    var image: String?
    var id: String?

    func toSafeModel() -> MovieListItem? {
        guard let title = title,
              let imageString = image,
              let imageURL = URL(string: imageString.replacingInsecureScheme()),
              let idString = id
        else {
            return nil
        }

        return MovieListItem(title: title, image: imageURL, id: idString)
    }
}

struct MovieListItem {
    let title: String
    let image: URL
    let id: String
}

extension MovieListItem: Equatable {}
