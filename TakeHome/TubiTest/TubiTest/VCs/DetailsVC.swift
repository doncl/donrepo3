//
//  DetailsVC.swift
//  TubiTest
//
//  Created by Don Clore on 7/30/22.
//

import UIKit

class DetailsVC: UIViewController {
    var movieDetails: MovieDetails? {
        didSet {
            guard let movieDetails = movieDetails else {
                return
            }

            self.navigationItem.title = movieDetails.title

            detailsView.movieDetails = movieDetails
        }
    }

    lazy var detailsView: MovieDetailsView = {
        let v = MovieDetailsView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        [detailsView].forEach {
            view.addSubview($0)
        }

        NSLayoutConstraint.activate([
            detailsView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            detailsView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
            detailsView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
            detailsView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
        ])
        navigationItem.hidesBackButton = false
        detailsView.setIndexLabel(visible: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let nav = navigationController else {
            return
        }
        nav.setNavigationBarHidden(false, animated: true)
    }
}
