//
//  MasterVC.swift
//  TubiTest
//
//  Created by Don Clore on 7/30/22.
//

import UIKit

// Yes, this can be done with callbacks, and many other more modren techniques, which are perfectly valid.
// still delegates are strongly-typed and bulletproof.
protocol MasterVCDelegate: AnyObject {
    func movieSelected(_ movie: MovieListItem)
    func isRegular() -> Bool
}

class MasterVC: UIViewController {
    let table: UITableView = .init()

    weak var delegate: MasterVCDelegate?

    private var movies: [MovieListItem] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        navigationItem.title = "Movie List"

        [table].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview($0)
        }

        NSLayoutConstraint.activate([
            table.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            table.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            table.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            table.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
        ])

        table.register(MovieListCell.self, forCellReuseIdentifier: MovieListCell.id)
        table.dataSource = self
        table.delegate = self
        table.rowHeight = 400.0
        table.allowsSelection = true

        fetchMovies()
    }

    private func fetchMovies() {
        Task {
            let movieListResult = await MovieFetcherBuddy.shared.getMovieList()
            switch movieListResult {
            case let .success(movieList):
                movies.removeAll()
                movies.append(contentsOf: movieList)
                table.reloadData()

                guard let delegate = delegate else {
                    return
                }

                guard delegate.isRegular(), movies.count > 0 else {
                    return
                }
                delegate.movieSelected(movies[0])

            case .failure:
                errorAlert(msg: "Could not load list of movies.") {}
            }
        }
    }
}

// MARK: UITableViewDataSource

extension MasterVC: UITableViewDataSource {
    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return movies.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MovieListCell.id, for: indexPath) as? MovieListCell else {
            fatalError("improper provisioning of table")
        }

        guard movies.count > indexPath.item else {
            return cell
        }

        cell.movie = movies[indexPath.item]
        return cell
    }
}

// MARK: UITableViewDelegate

extension MasterVC: UITableViewDelegate {
    func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let delegate = delegate else {
            return
        }

        guard movies.count > indexPath.item else {
            return
        }
        let movie = movies[indexPath.item]

        delegate.movieSelected(movie)
    }
}
