//
//  MovieListCell.swift
//  TubiTest
//
//  Created by Don Clore on 7/30/22.
//

import Kingfisher
import UIKit

class MovieListCell: UITableViewCell {
    enum Constants {
        static let stackSpacing: CGFloat = 32.0
        static let verticalPad: CGFloat = 32.0
    }

    static let id = String(describing: MovieListCell.self)

    lazy var movieDetailsView: MovieDetailsView = {
        let v = MovieDetailsView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()

    var movie: MovieListItem? {
        didSet {
            guard let movie = movie else {
                return
            }

            movieDetailsView.movie = movie
        }
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        contentView.addSubview(movieDetailsView)
        NSLayoutConstraint.activate([
            movieDetailsView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Constants.verticalPad),
            movieDetailsView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            movieDetailsView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            movieDetailsView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Constants.verticalPad),
        ])

        movieDetailsView.setIndexLabel(visible: false)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        movieDetailsView.setPlaceholderImage()
    }
}
