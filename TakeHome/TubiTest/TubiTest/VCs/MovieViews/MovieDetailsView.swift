//
//  MovieDetailsView.swift
//  TubiTest
//
//  Created by Don Clore on 7/30/22.
//

import Kingfisher
import UIKit

class MovieDetailsView: UIView {
    enum Constants {
        static let stackSpacing: CGFloat = 32.0
        static let verticalPad: CGFloat = 32.0
    }

    private let placeHolderImage: UIImage = .init(named: "movieplaceholder")!

    private lazy var image: UIImageView = {
        let iv = UIImageView(image: placeHolderImage)
        iv.contentMode = UIView.ContentMode.scaleAspectFill
        return iv
    }()

    private lazy var title: UILabel = {
        let l = UILabel()
        l.font = UIFont.preferredFont(forTextStyle: .title1)
        l.textColor = UIColor.black

        return l
    }()

    private lazy var index: UILabel = {
        let l = UILabel()
        l.font = UIFont.preferredFont(forTextStyle: .title2)
        l.textColor = UIColor.black
        return l
    }()

    private lazy var stack: UIStackView = {
        let s = UIStackView(arrangedSubviews: [image, title, index])
        s.axis = .vertical
        s.spacing = Constants.stackSpacing
        s.distribution = UIStackView.Distribution.fillProportionally
        s.translatesAutoresizingMaskIntoConstraints = false
        s.alignment = .center
        return s
    }()

    // Use the same view object to do movieDetails as movie list items.
    // This could have been done with a protocol, but overkill
    var movie: MovieListItem? {
        didSet {
            guard let movie = movie else {
                return
            }

            set(image: movie.image, andTitle: movie.title)
        }
    }

    var movieDetails: MovieDetails? {
        didSet {
            guard let movieDetails = movieDetails else {
                return
            }

            set(image: movieDetails.image, andTitle: movieDetails.title)

            index.text = "Index: \(movieDetails.index)"
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        [stack].forEach {
            addSubview($0)
        }

        NSLayoutConstraint.activate([
            stack.topAnchor.constraint(equalTo: topAnchor),
            stack.leftAnchor.constraint(equalTo: leftAnchor),
            stack.rightAnchor.constraint(equalTo: rightAnchor),
            stack.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func set(image: URL, andTitle movieTitle: String) {
        title.text = movieTitle

        KingfisherManager.shared.retrieveImage(with: image) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case let .success(kingFisherDownloadResult):
                self.image.image = kingFisherDownloadResult.image

            case let .failure(error):
                print("\(#function) - error \(error)")
                // just keep showing the placholder.
            }
        }
    }

    func setPlaceholderImage() {
        image.image = placeHolderImage
    }

    func setIndexLabel(visible: Bool) {
        index.isHidden = !visible
    }
}
