//
//  ViewController.swift
//  TubiTest
//
//  Created by Don Clore on 7/25/22.
//

import UIKit

class RootVC: UISplitViewController {
    let masterNav: UINavigationController = .init()
    let detailsNav: UINavigationController = .init()
    let detailsVC: DetailsVC = .init()

    override func viewDidLoad() {
        super.viewDidLoad()

        viewControllers = [masterNav, detailsNav]
        #if targetEnvironment(macCatalyst)
            preferredDisplayMode = UISplitViewController.DisplayMode.allVisible
        #else
            preferredDisplayMode = UISplitViewController.DisplayMode.oneBesideSecondary
        #endif
        delegate = self

        let selector = MasterVC()
        selector.delegate = self

        masterNav.show(selector, sender: self)
        masterNav.setNavigationBarHidden(true, animated: false)
        detailsNav.viewControllers = [detailsVC]
    }
}

// MARK: UISplitViewControllerDelegate

extension RootVC: UISplitViewControllerDelegate {
    func splitViewController(_: UISplitViewController,
                             collapseSecondary secondaryViewController: UIViewController,
                             onto primaryViewController: UIViewController) -> Bool
    {
        guard let _ = secondaryViewController as? DetailsVC else {
            // Fallback to the default
            return true
        }

        if let primaryAsNavController = primaryViewController as? UINavigationController {
            primaryAsNavController.setNavigationBarHidden(false, animated: false)
        }

        return false
    }

    func primaryViewController(forExpanding _: UISplitViewController) -> UIViewController? {
        masterNav.setNavigationBarHidden(true, animated: true)
        return masterNav
    }
}

// MARK: MasterVCDelegate

extension RootVC: MasterVCDelegate {
    func isRegular() -> Bool {
        return traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular
    }

    func movieSelected(_ movie: MovieListItem) {
        let id = movie.id
        Task {
            let detailsResult = await MovieFetcherBuddy.shared.getMovieDetails(id: id)
            switch detailsResult {
            case let .success(details):
                launchDetails(details)

            case .failure:
                errorAlert(msg: "Failed to fetch movie details for \(movie.title)") {}
            }
        }
    }

    @MainActor
    private func launchDetails(_ movieDetails: MovieDetails) {
        detailsVC.navigationItem.leftBarButtonItem = displayModeButtonItem
        detailsVC.navigationItem.leftItemsSupplementBackButton = true
        detailsVC.movieDetails = movieDetails
        showDetailViewController(detailsVC, sender: nil)
    }
}
