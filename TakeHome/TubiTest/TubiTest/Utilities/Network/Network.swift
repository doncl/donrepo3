//
//  Network.swift
//  TubiTest
//
//  Created by Don Clore on 7/30/22.
//

import Foundation

enum NetworkError: Error {
    case nilResult
    case unexpectedError(String)
    case httpError(Int, String)
    case decodingError(Error)
}

struct Network {
    static var shared: Network = .init()

    var session = URLSession.shared

    private init() {}

    func getSomething<T: Codable>(url: URL) async -> Result<T, Error> {
        guard let tuple: (data: Data, response: URLResponse) = try? await session.data(from: url, delegate: nil) else {
            return .failure(NetworkError.nilResult)
        }

        guard let httpResp = tuple.response as? HTTPURLResponse else {
            return .failure(NetworkError.unexpectedError("Response is not an HTTPURLResponse, what gives?"))
        }

        guard 200 ..< 300 ~= httpResp.statusCode else {
            return .failure(NetworkError.httpError(httpResp.statusCode, "HTTPS error"))
        }

        let decoder = JSONDecoder()

        do {
            let entity = try decoder.decode(T.self, from: tuple.data)
            return .success(entity)
        } catch {
            return .failure(NetworkError.decodingError(error))
        }
    }
}
