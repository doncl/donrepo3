//
//  UIViewController+ErrorAlert.swift
//  TubiTest
//
//  Created by Don Clore on 7/30/22.
//

import UIKit

extension UIViewController {
    @MainActor
    func errorAlert(msg: String, completion: @escaping () -> Void) {
        let ac = UIAlertController(title: "Error", message: msg, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default) { _ in
            completion()
        }
        ac.addAction(ok)

        present(ac, animated: true)
    }
}
