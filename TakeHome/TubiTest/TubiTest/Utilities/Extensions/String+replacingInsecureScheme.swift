//
//  String+replacingInsecureScheme.swift
//  TubiTest
//
//  Created by Don Clore on 7/30/22.
//

import Foundation

extension String {
    func replacingInsecureScheme() -> String {
        guard !starts(with: "http://") else {
            return replacingOccurrences(of: "http://", with: "https://")
        }
        guard !starts(with: "https://") else {
            return "https://\(self)"
        }
        return self
    }
}
