//
//  LRU.swift
//  TubiTest
//
//  Created by Don Clore on 7/25/22.
//

import Foundation

// with a nod to:
// https://github.com/RinniSwift/Computer-Science-with-iOS/blob/main/DataStructures/LRUCache.playground/Contents.swift
// This is implemented substantially differently, but it was a good inspiration.

protocol CacheItemProtocol {
    associatedtype Key
    associatedtype Value

    var key: Key { get set }
    var value: Value { get set }
}

struct CacheItem<KeyType: Hashable, ValueType>: CacheItemProtocol {
    var key: KeyType
    var value: ValueType

    init(key: KeyType, value: ValueType) {
        self.key = key
        self.value = value
    }
}

// So we can implement the Queue.moveToFront easily.
extension CacheItem: Equatable {
    static func == (lhs: CacheItem<KeyType, ValueType>, rhs: CacheItem<KeyType, ValueType>) -> Bool {
        return lhs.key == rhs.key
    }
}

class LRUNode<T: CacheItemProtocol> {
    var item: T

    init(item: T) {
        self.item = item
    }
}

struct LRUCache<T: Hashable, U> {
    private(set) var q: Queue<CacheItem<T, U>>
    private(set) var dict = [T: LRUNode<CacheItem<T, U>>]()

    var isEmpty: Bool {
        assertIntegrity()
        assert(dict.isEmpty == q.isEmpty)
        return q.isEmpty
    }

    var count: Int {
        assertIntegrity()
        return q.count
    }

    var peek: U? {
        assertIntegrity()
        guard let peekedItem = q.peek else {
            return nil
        }
        return peekedItem.value
    }

    init(size: Int) {
        q = Queue<CacheItem<T, U>>(maxItems: size)
    }

    // Reading the homework instructions, I interpret the statement:
    // "isValid: check if a item is still valid based on the key"
    // must mean, 'is the object still in the cache'?   If it means something else, sorry, my friends.
    func isValid(key: T) -> Bool {
        return dict[key] != nil
    }

    mutating func get(atKey key: T) -> U? {
        assertIntegrity()

        guard let existing = dict[key] else {
            return nil
        }

        let item = existing.item

        q.moveToFront(item: item)

        return item.value
    }

    mutating func set(value: U, forKey key: T) {
        let cacheItem = CacheItem(key: key, value: value)
        let cacheNode = LRUNode(item: cacheItem)

        if let existingNode = dict[key] {
            let updatedItem = CacheItem(key: key, value: value)
            existingNode.item = updatedItem
            q.moveToFront(item: existingNode.item)
            dict[key] = existingNode
        } else {
            let possiblyEjectedItem = q.enqueue(cacheItem)
            dict[key] = cacheNode
            if let item = possiblyEjectedItem {
                let keyToRemove = item.key
                dict.removeValue(forKey: keyToRemove)
            }
        }
        assertIntegrity()
    }

    private func assertIntegrity() {
        #if DEBUG
            assert(q.count == dict.count)
        #endif
    }
}
