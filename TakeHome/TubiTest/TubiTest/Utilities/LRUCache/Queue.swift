//
//  Queue.swift
//  TubiTest
//
//  Created by Don Clore on 7/25/22.
//

import Foundation

struct Queue<T: Equatable> {
    private var left: [T] = []
    private var right: [T] = []

    let maxItems: Int

    public var isEmpty: Bool {
        return left.isEmpty && right.isEmpty
    }

    public var peek: T? {
        return !left.isEmpty ? left.last : right.first
    }

    public var count: Int {
        return left.count + right.count
    }

    public init(maxItems: Int) {
        self.maxItems = maxItems
    }

    @discardableResult
    public mutating func enqueue(_ element: T) -> T? {
        right.append(element)
        if count <= maxItems {
            return nil
        }
        return dequeue() // remove one.
    }

    @discardableResult
    public mutating func dequeue() -> T? {
        if left.isEmpty {
            left = right.reversed()
            right.removeAll()
        }
        return left.popLast()
    }

    public mutating func moveToFront(item: T) {
        var found = false
        if let index = right.firstIndex(of: item) {
            right.remove(at: index)
            found = true
        } else if let index = left.firstIndex(of: item) {
            left.remove(at: index)
            found = true
        }
        assert(found)
        right.insert(item, at: 0)
    }
}

extension Queue: CustomStringConvertible {
    public var description: String {
        return String(describing: left.reversed() + right)
    }
}
