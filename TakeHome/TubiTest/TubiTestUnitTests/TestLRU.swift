//
//  TestLRU.swift
//  TubiTestTests
//
//  Created by Don Clore on 7/29/22.
//

@testable import TubiTest
import XCTest

// with a nod to:
// https://github.com/RinniSwift/Computer-Science-with-iOS/blob/main/DataStructures/LRUCache.playground/Contents.swift
// I coulda figured out something similar, but this is a nice assertion of basic functionality with some useful edge
// case testing.

class TestLRU: XCTestCase {
    func testEmptyCache() {
        let c = LRUCache<String, Int>(size: 5)

        XCTAssert(c.isEmpty)
    }

    func testAddingOneObject() {
        var c = LRUCache<String, Int>(size: 5)

        c.set(value: 21, forKey: "Calvin")

        XCTAssertEqual(c.count, 1)
        XCTAssertEqual(c.peek, 21)
    }

    func testAddingTwoObjects() {
        var c = LRUCache<String, Int>(size: 5)

        c.set(value: 21, forKey: "Calvin")
        c.set(value: 42, forKey: "Hobbes")

        XCTAssertEqual(c.count, 2)

        XCTAssertEqual(c.peek, 21) // first item enqueued should still be at front.

        XCTAssertEqual(c.get(atKey: "Calvin"), 21)
        XCTAssertEqual(c.get(atKey: "Hobbes"), 42)
    }

    func testAddingDuplicateObject() {
        var c = LRUCache<String, Int>(size: 5)
        c.set(value: 21, forKey: "Calvin")
        c.set(value: 42, forKey: "Hobbes")
        c.set(value: 41, forKey: "Hobbes")

        XCTAssertEqual(c.count, 2)
        XCTAssertEqual(c.get(atKey: "Calvin"), 21)
        XCTAssertEqual(c.get(atKey: "Hobbes"), 41)
        XCTAssertEqual(c.peek, 41)
    }

    func testAddingDuplicateObjectToAlmostFullCache() {
        var c = LRUCache<String, Int>(size: 5)
        c.set(value: 21, forKey: "Calvin")
        c.set(value: 42, forKey: "Hobbes")
        c.set(value: 99, forKey: "Ms. Marvel")
        c.set(value: 7, forKey: "Lucky Charms")
        c.set(value: 41, forKey: "Hobbes")

        XCTAssertEqual(c.get(atKey: "Calvin"), 21)
        XCTAssertEqual(c.get(atKey: "Ms. Marvel"), 99)
        XCTAssertEqual(c.get(atKey: "Lucky Charms"), 7)
        XCTAssertEqual(c.get(atKey: "Hobbes"), 41)

        XCTAssertEqual(c.count, 4)
        XCTAssertEqual(c.peek, 41)
    }

    func testAddingDuplicateObjectToFullCache() {
        var c = LRUCache<String, Int>(size: 5)
        c.set(value: 21, forKey: "Calvin")
        c.set(value: 42, forKey: "Hobbes")
        c.set(value: 99, forKey: "Ms. Marvel")
        c.set(value: 7, forKey: "Lucky Charms")
        c.set(value: 75, forKey: "Superior")
        c.set(value: 41, forKey: "Hobbes")

        XCTAssertEqual(c.get(atKey: "Calvin"), 21)
        XCTAssertEqual(c.get(atKey: "Ms. Marvel"), 99)
        XCTAssertEqual(c.get(atKey: "Lucky Charms"), 7)
        XCTAssertEqual(c.get(atKey: "Superior"), 75)
        XCTAssertEqual(c.get(atKey: "Hobbes"), 41)

        XCTAssertEqual(c.count, 5)

        XCTAssertEqual(c.peek, 41)
    }

    func testAddingObjectToFullCache() {
        var c = LRUCache<String, Int>(size: 5)
        c.set(value: 21, forKey: "Calvin")
        c.set(value: 42, forKey: "Hobbes")
        c.set(value: 99, forKey: "Ms. Marvel")
        c.set(value: 7, forKey: "Lucky Charms")
        c.set(value: 75, forKey: "Superior")
        c.set(value: -42, forKey: "OverTheTop")

        XCTAssertNil(c.get(atKey: "Calvin"))
        XCTAssertEqual(c.get(atKey: "Hobbes"), 42)
        XCTAssertEqual(c.get(atKey: "Ms. Marvel"), 99)
        XCTAssertEqual(c.get(atKey: "Lucky Charms"), 7)
        XCTAssertEqual(c.get(atKey: "Superior"), 75)

        XCTAssertEqual(c.count, 5)
        XCTAssertEqual(c.peek, -42)
    }

    func testAddingDuplicateObjectToFullerCache() {
        var c = LRUCache<String, Int>(size: 5)

        c.set(value: 21, forKey: "Calvin")
        c.set(value: 42, forKey: "Hobbes")
        c.set(value: 99, forKey: "Ms. Marvel")
        c.set(value: 7, forKey: "Lucky Charms")
        c.set(value: 75, forKey: "Superior")
        c.set(value: 140, forKey: "buttonWidth")
        c.set(value: 999, forKey: "Ms. Marvel") // 7th item - this is dup.

        XCTAssertEqual(c.count, 5)

        XCTAssertNil(c.get(atKey: "Calvin"))

        XCTAssertEqual(c.get(atKey: "Hobbes"), 42)
        XCTAssertEqual(c.get(atKey: "Lucky Charms"), 7)
        XCTAssertEqual(c.get(atKey: "Superior"), 75)
        XCTAssertEqual(c.get(atKey: "buttonWidth"), 140)
        XCTAssertEqual(c.get(atKey: "Ms. Marvel"), 999)
    }
}
