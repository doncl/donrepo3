//
//  TestMovieListEntityDeserialization.swift
//  TubiTestTests
//
//  Created by Don Clore on 7/30/22.
//

@testable import TubiTest
import XCTest

class TestMovieListEntityDeserialization: XCTestCase, ResourceTestable {
    func testDesializingListEntity() {
        let dataFromResourceFile = getResourceData(named: "MovieListTestEntity", withExtension: "json")

        let decoder = JSONDecoder()
        let arrayOfUnsafeMovies = try? decoder.decode([UnsafeMovieListItem].self, from: dataFromResourceFile)
        XCTAssertNotNil(arrayOfUnsafeMovies)

        XCTAssertEqual(arrayOfUnsafeMovies!.count, 50)

        let movieListArray = arrayOfUnsafeMovies!.compactMap { $0.toSafeModel() }

        XCTAssertEqual(movieListArray.count, 50)
    }
}
