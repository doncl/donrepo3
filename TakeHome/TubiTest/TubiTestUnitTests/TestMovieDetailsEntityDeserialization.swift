//
//  TestMovieDetailsEntityDeserialization.swift
//  TubiTestUnitTests
//
//  Created by Don Clore on 7/30/22.
//

@testable import TubiTest
import XCTest

class TestMovieDetailsEntityDeserialization: XCTestCase, ResourceTestable {
    func testMovieDetailsDeserialization() {
        let dataFromResourceFile = getResourceData(named: "MovieDetailsTestEntity", withExtension: "json")

        let decoder = JSONDecoder()
        let unsafeMovieDetails = try? decoder.decode(UnsafeMovieDetails.self, from: dataFromResourceFile)
        XCTAssertNotNil(unsafeMovieDetails)

        let movieDetails = unsafeMovieDetails!.toSafeModel()
        XCTAssertNotNil(movieDetails)
        let m = movieDetails!

        XCTAssertEqual(m.title, "Igor")
        XCTAssertEqual(m.image.absoluteString, "https://images.adrise.tv/mLPoP2m45bK3Rpa92yXQL6anRf8=/214x306/smart/img.adrise.tv/a04564c2-ddd7-4d3a-bcb5-8b1b07317683.jpg")

        XCTAssertEqual(m.id, "334155")
        XCTAssertEqual(m.index, 3)
    }
}
