//
//  TestMovieAndMovieListWithMockedURLProtocol.swift
//  TubiTestUnitTests
//
//  Created by Don Clore on 7/30/22.
//

@testable import TubiTest
import XCTest

class TestMovieAndMovieListWithMockedURLProtocol: XCTestCase, ResourceTestable {
    lazy var testConfig: URLSessionConfiguration = {
        let config = URLSessionConfiguration.default
        config.protocolClasses = [URLTestProtocol.self]
        return config
    }()

    lazy var testConfirmationList: [MovieListItem] = {
        let testListData = getResourceData(named: "MovieListTestEntity", withExtension: "json")
        let decoder = JSONDecoder()
        let unsafeList = try! decoder.decode([UnsafeMovieListItem].self, from: testListData)
        let safeList = unsafeList.compactMap { $0.toSafeModel() }
        return safeList
    }()

    lazy var testConfirmationDetails: MovieDetails = {
        let testDetailsData = getResourceData(named: "MovieDetailsTestEntity", withExtension: "json")
        let decoder = JSONDecoder()
        let unsafeDetails = try! decoder.decode(UnsafeMovieDetails.self, from: testDetailsData)
        let safeDetails = unsafeDetails.toSafeModel()!
        return safeDetails
    }()

    override func setUp() async throws {
        Network.shared.session = URLSession(configuration: testConfig)
    }

    override func tearDown() async throws {
        Network.shared.session = URLSession.shared
    }

    func testMovieListCode() async {
        let listResult = await MovieFetcherBuddy.shared.getMovieList()

        switch listResult {
        case let .success(list):
            // Compare it to list loaded directly from bundle
            XCTAssertEqual(list, testConfirmationList)

        case let .failure(error):
            XCTFail(error.localizedDescription)
        }
    }

    func testMovieDetailsCode() async {
        let detailsResult = await MovieFetcherBuddy.shared.getMovieDetails(id: "334155") // the one in our provisioned test json.

        switch detailsResult {
        case let .success(details):
            // Compare it to details loaded directly from bundle
            XCTAssertEqual(details, testConfirmationDetails)

        case let .failure(error):
            XCTFail(error.localizedDescription)
        }
    }
}
