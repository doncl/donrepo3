//
//  URLTestProtocol.swift
//  TubiTestUnitTests
//
//  Created by Don Clore on 7/30/22.
//

import Foundation
@testable import TubiTest

class URLTestProtocol: URLProtocol, ResourceTestable {
    override class func canInit(with _: URLRequest) -> Bool {
        return true
    }

    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }

    override func startLoading() {
        guard let client = client,
              let url = request.url,
              let response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)
        else {
            fatalError("Improperty provisioned")
        }

        // We only have two service enpoints we can call - one can imagine a much more sophisticated set of mocks, but this gets the point across, I hope.
        let injectedData: Data
        if url == MovieFetcherBuddy.Constants.movieListURL {
            injectedData = getResourceData(named: "MovieListTestEntity", withExtension: "json")
        } else {
            injectedData = getResourceData(named: "MovieDetailsTestEntity", withExtension: "json")
        }

        client.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed)
        client.urlProtocol(self, didLoad: injectedData)
        client.urlProtocolDidFinishLoading(self)
    }

    override func stopLoading() {}
}
