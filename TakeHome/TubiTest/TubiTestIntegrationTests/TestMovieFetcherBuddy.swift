//
//  TestMovieFetcherBuddy.swift
//  TubiTestIntegrationTests
//
//  Created by Don Clore on 7/30/22.
//

@testable import TubiTest
import XCTest

// This is an integration test, presumably you'd do this in some less-frequent stage of your CI/CD, it actually really hits the network.
// This could also be built into some sort of Service Monitor that periodically hits your back-end service to make sure it's still delivering the
// stuff as promised.
class TestMovieFetcherBuddy: XCTestCase {
    enum Constants {
        static let movieID = "334155"
    }

    func testFetcherBuddyReturnsMovieList() async {
        let movieListResult = await MovieFetcherBuddy.shared.getMovieList()
        switch movieListResult {
        case let .success(movieList):
            XCTAssertTrue(movieList.count > 0)
        case let .failure(error):
            XCTFail(error.localizedDescription)
        }
    }

    func testFetcherBuddyReturnsMovie() async {
        let movieDetailsResult = await MovieFetcherBuddy.shared.getMovieDetails(id: Constants.movieID)
        switch movieDetailsResult {
        case let .success(movieDetails):
            XCTAssertEqual(movieDetails.title, "Igor")

        case let .failure(error):
            XCTFail(error.localizedDescription)
        }
    }
}
