//
//  PhoneKitView.swift
//  PickerStyles
//
//  Created by Don Clore on 5/22/23.
//

import SwiftUI

struct PhoneKitView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct PhoneKitView_Previews: PreviewProvider {
    static var previews: some View {
        PhoneKitView()
    }
}
