//
//  NavigationLinkPicker.swift
//  PickerStyles
//
//  Created by Don Clore on 5/22/23.
//

import SwiftUI

struct WheelPicker: View {
    let title: String
    @State private var selection = String(Generator.shared.content[0])
    @State var content: [String] = Generator.shared.content
    
    var body: some View {
        Picker(title, selection: $selection) {
            ForEach(content, id:\.self) {
                Text($0)
            }
        }
        .pickerStyle(.wheel)
    }
    
    init(title: String) {
        self.title = title
    }
}


