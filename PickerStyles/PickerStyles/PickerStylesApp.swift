//
//  PickerStylesApp.swift
//  PickerStyles
//
//  Created by Don Clore on 5/22/23.
//

import SwiftUI

@main
struct PickerStylesApp: App {
    var content: [String] = []
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
    
    init() {
        content = Generator.shared.content
    }
}
