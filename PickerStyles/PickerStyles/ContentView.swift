//
//  ContentView.swift
//  PickerStyles
//
//  Created by Don Clore on 5/22/23.
//

import SwiftUI

enum Tab {
    case inline
    case menu
    case navigationLink
    case radioGroup
    case segmented
    case wheel
}

struct ContentView: View {
    @State private var selectedTab: Tab = .inline
        
    var body: some View {
        TabView(selection: $selectedTab) {
            InlinePicker(title: "Inline")
                .tabItem {
                    Text("Inline")
                }
            MenuPicker(title: "Menu")
                .tabItem {
                    Text("Menu")
                }
            
            WheelPicker(title: "wheel")
                .tabItem {
                    Text("Wheel")
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
