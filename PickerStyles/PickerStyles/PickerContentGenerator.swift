//
//  PickerContentGenerator.swift
//  PickerStyles
//
//  Created by Don Clore on 5/22/23.
//

import Foundation
import SwiftUI


class Generator {
    private init() {}
    static let shared = Generator()
    
    lazy var content: [String] = {
       return generateContent()
    }()
    
    private func generateContent() -> [String] {
        let bundle = Bundle(for: type(of: self))
        guard let url = bundle.url(forResource: "CountryCodes", withExtension: "txt") else {
            return []
        }
        do {
            var ret: [String] = []
            let text = try String(contentsOf: url, encoding: .utf8)
            let lines = text.split(whereSeparator: \.isNewline)
            for line in lines {
                let tokens = line.split(separator: ",")
                if tokens.count < 4 {
                    continue
                }
                let countryName = tokens[0]
                let code = tokens[3]
                let retLine = "\(code) \(countryName)"
                ret.append(retLine)
            }
            return ret
        } catch {
            return []
        }
    }

}

