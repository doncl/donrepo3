//
//  PickerView.swift
//  PickerStyles
//
//  Created by Don Clore on 5/22/23.
//

import SwiftUI


struct InlinePicker: View {
    let title: String
    @State private var selection = String(Generator.shared.content[0])
    @State var content: [String] = Generator.shared.content
    
    var body: some View {
        Picker(title, selection: $selection) {
            ForEach(content, id:\.self) {
                Text($0)
            }
        }
        .pickerStyle(.inline)
    }
    
    init(title: String) {
        self.title = title
    }
}

