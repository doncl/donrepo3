//
//  Model.swift
//  TestObservable
//
//  Created by Don Clore on 9/13/23.
//

import SwiftUI

struct Book: Identifiable {
  let id: UUID
  let name: String
  let color: Color

  init(_ id: UUID = UUID(), name: String, color: Color) {
    self.id = id
    self.name = name
    self.color = color
  }
  init(_ name: String, _ color: Color) {
    self.init(UUID(), name: name, color: color)
  }
}

@Observable class Model {
  var books: [Book] = [
    Book("Pride and Prejudice", .orange),
    Book("Huckleberry Finn", .blue),
    Book("To Kill a Mockingbird", .green),
    Book("War and Peace", .pink),
  ]
}

extension Model {
  func addBook(_ name: String, _ color: Color) {
    let newBook = Book(name, color)
    books.append(newBook)
  }
}
