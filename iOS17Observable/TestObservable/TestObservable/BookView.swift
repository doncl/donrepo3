//
//  BookView.swift
//  TestObservable
//
//  Created by Don Clore on 9/13/23.
//

import SwiftUI

struct BookView: View {
  let book: Book

  var body: some View {
    HStack {
      Image(systemName: "book.closed.circle.fill")
        .resizable()
        .renderingMode(.original)
        .foregroundColor(book.color)
        .scaledToFit()
        .frame(maxHeight: 60)


      Text(book.name)
        .font(.title3)
        .padding()

      Spacer()
    }
    .padding()
  }
}

#Preview {
  BookView(book: Book("Faux Book", Color.orange))
}
