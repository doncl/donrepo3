//
//  ContentView.swift
//  TestObservable
//
//  Created by Don Clore on 9/13/23.
//

import SwiftUI

struct ContentView: View {
  @Environment(Model.self) private var model: Model
  @State private var modal = false

  var body: some View {
    VStack {
      List(model.books) { book in
        BookView(book: book)
      }

      AddBookButton(onClick: {
        modal = true
      })
    }
    .padding()
    .sheet(isPresented: $modal) {
      AddBookDialog()
    }
  }
}

#Preview {
  ContentView()
    .environment(Model())
}
