//
//  AddBookDialog.swift
//  TestObservable
//
//  Created by Don Clore on 9/13/23.
//

import SwiftUI

struct AddBookDialog: View {
  @Environment(\.dismiss) private var dismiss
  @Environment(Model.self) private var model: Model
  @State private var title = ""
  @State private var color = Color.accentColor

  var body: some View {
    VStack {
      VStack {
        TextField("Title", text: $title)
          .lineLimit(5)
        
        ColorPicker("Color", selection: $color)
      }
      .font(.title)
      .padding()

      AddBookButton(onClick: {
        model.addBook(title, color)
        dismiss()
      })
      .disabled(title.isEmpty)

      Spacer()
    }
    .padding()
  }
}

#Preview {
  AddBookDialog()
    .environment(Model())
}
