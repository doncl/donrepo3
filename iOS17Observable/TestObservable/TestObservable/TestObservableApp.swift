//
//  TestObservableApp.swift
//  TestObservable
//
//  Created by Don Clore on 9/13/23.
//

import SwiftUI

@main
struct TestObservableApp: App {
  @State private var model = Model()

  var body: some Scene {
    WindowGroup {
      ContentView()
        .environment(model)
    }
  }
}
