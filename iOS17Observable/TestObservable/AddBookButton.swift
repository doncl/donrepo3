//
//  AddBookButton.swift
//  TestObservable
//
//  Created by Don Clore on 9/13/23.
//

import SwiftUI

struct AddBookButton: View {
  var onClick: () -> ()

  var body: some View {
    Button {
      onClick()
    } label: {
      Text("Add Book")
        .foregroundStyle(Color.white)
        .font(.headline)
        .padding([.top, .bottom],12)
        .padding([.leading, .trailing], 28)
        .background(Color.blue)
        .clipShape(Capsule())
    }
  }
}

#Preview {
  AddBookButton(onClick: {})
}
