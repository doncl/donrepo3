//
//  SearchCompleterApp.swift
//  SearchCompleter
//
//  Created by Don Clore on 5/30/23.
//

import SwiftUI

@main
struct SearchCompleterApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
