//
//  HasSpinner.swift
//  DocPicker
//
//  Created by Don Clore on 8/23/22.
//

import UIKit

protocol HasSpinner: AnyObject {
    var desiredLocation: CGPoint { get }
    var spinner: UIActivityIndicatorView { get }
    func stopSpinner()
    func setSpinner(on parentView: UIView)
}

extension HasSpinner {
    func setSpinner(on parentView: UIView) {
        spinner.center = desiredLocation
        spinner.isHidden = false
        spinner.startAnimating()
        parentView.addSubview(spinner)
        parentView.bringSubviewToFront(spinner)
    }

    func stopSpinner() {
        spinner.stopAnimating()
        spinner.isHidden = true
        guard let _ = spinner.superview else {
            return
        }

        spinner.removeFromSuperview()
    }
}
