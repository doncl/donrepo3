//
//  ItemCell.swift
//  DocPicker
//
//  Created by Don Clore on 8/23/22.
//

import UIKit

class ItemCell: UICollectionViewCell {
    static let id: String = .init(describing: ItemCell.self)

    lazy var label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.black
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.numberOfLines = 0
        label.allowsDefaultTighteningForTruncation = true
        label.adjustsFontSizeToFitWidth = true
        label.lineBreakMode = NSLineBreakMode.byWordWrapping

        return label
    }()

    lazy var filmIcon: UIImage = {
        let fi = UIImage(named: "film")!
        return fi
    }()

    lazy var image: UIImageView = {
        let iv = UIImageView(image: filmIcon)
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = UIView.ContentMode.scaleAspectFill
        return iv
    }()

    var item: URL? {
        didSet {
            guard let item = item else {
                return
            }

            let fileName = item.lastPathComponent
            label.text = fileName
            label.sizeToFit()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        [image, label].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview($0)
        }

        NSLayoutConstraint.activate([
            image.topAnchor.constraint(equalTo: contentView.topAnchor),
            image.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            image.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            image.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.6),

            label.topAnchor.constraint(equalTo: image.bottomAnchor),
            label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
        ])
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
