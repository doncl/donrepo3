//
//  UIResponder+Scene.swift
//  DocPicker
//
//  Created by Don Clore on 8/23/22.
//

import UIKit

@available(iOS 13.0, *)
extension UIResponder {
    @objc var scene: UIScene? {
        return nil
    }
}

@available(iOS 13.0, *)
extension UIScene {
    @objc override var scene: UIScene? {
        return self
    }
}

@available(iOS 13.0, *)
extension UIView {
    @objc override var scene: UIScene? {
        if let window = window {
            return window.windowScene
        } else {
            return next?.scene
        }
    }
}

@available(iOS 13.0, *)
extension UIViewController {
    @objc override var scene: UIScene? {
        // Try walking the responder chain
        var res = next?.scene
        if res == nil {
            // That didn't work. Try asking my parent view controller
            res = parent?.scene
        }
        if res == nil {
            // That didn't work. Try asking my presenting view controller
            res = presentingViewController?.scene
        }

        return res
    }
}
