//
//  ViewController.swift
//  DocPicker
//
//  Created by Don Clore on 8/22/22.
//

import MobileCoreServices
import UIKit
import UniformTypeIdentifiers

class ViewController: UIViewController {
    enum Constants {
        static let buttonWidth: CGFloat = 70
        static let gutters: CGFloat = 16
        static let minimumInterItem: CGFloat = 8
        static let itemsPerRow: CGFloat = 4
        static let rowSpace: CGFloat = 24
        static let aspectRatio = 1.6
        static let cvInsets: UIEdgeInsets = .init(top: 0, left: 24, bottom: 0, right: 24)
    }

    lazy var spinner: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
        spinner.isHidden = true
        spinner.style = .large
        spinner.color = .purple
        return spinner
    }()

    lazy var queue: OperationQueue = {
        let q = OperationQueue()
        return q
    }()

    lazy var fileCoordinator: NSFileCoordinator = {
        let coor = NSFileCoordinator(filePresenter: nil)
        return coor
    }()

    lazy var flow: UICollectionViewFlowLayout = {
        let flow = UICollectionViewFlowLayout()
        return flow
    }()

    lazy var cv: UICollectionView = {
        let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: flow)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = .white
        cv.contentInset = UIEdgeInsets.zero
        cv.register(ItemCell.self, forCellWithReuseIdentifier: ItemCell.id)
        cv.contentInset = Constants.cvInsets
        return cv
    }()

    var items: [URL] = []

    var cvLaidOut: Bool = false

    lazy var button: UIButton = {
        let b = UIButton(type: .custom)
        b.backgroundColor = UIColor.purple
        b.setTitleColor(UIColor.white, for: .normal)
        b.setTitle("Browse", for: .normal)
        b.layer.cornerRadius = 10
        b.layer.borderWidth = 3.0
        b.layer.borderColor = UIColor.white.cgColor

        b.addTarget(self, action: #selector(ViewController.buttonPressed(_:)), for: UIControl.Event.touchUpInside)
        return b
    }()

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        colorStatusBar()

        navigationItem.title = "Download from Cloud"
        view.backgroundColor = UIColor.white

        [button, cv].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
        }

        let actionButton = UIBarButtonItem(customView: button)
        navigationItem.rightBarButtonItem = actionButton

        view.addSubview(cv)

        NSLayoutConstraint.activate([
            button.widthAnchor.constraint(equalToConstant: Constants.buttonWidth),

            cv.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            cv.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
            cv.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
            cv.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
        ])

        cv.dataSource = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let nav = navigationController else {
            return
        }
        nav.navigationBar.barTintColor = UIColor.purple
        nav.navigationBar.backgroundColor = UIColor.purple
        nav.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 18),
        ]
        configureFlow()
    }

    private func configureFlow() {
        guard !cvLaidOut else {
            return
        }

        cvLaidOut = true

        flow.minimumLineSpacing = Constants.rowSpace
        flow.minimumInteritemSpacing = Constants.minimumInterItem

        let remainingWidth = view.bounds.width -
            (((Constants.itemsPerRow - 1) * Constants.minimumInterItem) +
                (Constants.gutters * 2) +
                (Constants.cvInsets.left + Constants.cvInsets.right))

        let itemWidth = remainingWidth / Constants.itemsPerRow
        let itemHeight = itemWidth * Constants.aspectRatio

        flow.itemSize = CGSize(width: itemWidth, height: itemHeight)
    }

    private func colorStatusBar() {
        if let windowScene = view.scene as? UIWindowScene, let mgr = windowScene.statusBarManager {
            let statusBarHeight: CGFloat = mgr.statusBarFrame.height
            let statusbarView = UIView()
            statusbarView.backgroundColor = UIColor.purple
            view.addSubview(statusbarView)

            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
        }
    }

    @objc func buttonPressed(_: UIButton) {
        let picker = UIDocumentPickerViewController(
            forOpeningContentTypes:
            [
                UTType.mpeg4Movie,
                UTType.mpeg,
                UTType.movie,
                UTType.avi,
                UTType.quickTimeMovie,
            ],
            asCopy: false
        )

        picker.allowsMultipleSelection = false
        picker.shouldShowFileExtensions = true
        picker.modalPresentationStyle = .formSheet
        picker.preferredContentSize = CGSize(width: view.bounds.width * 0.8, height: view.bounds.height * 0.8)
        picker.delegate = self
        present(picker, animated: true)
    }
}

extension ViewController: UIDocumentPickerDelegate {
    public func documentPicker(_: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let url = urls.first else {
            return
        }

        setSpinner(on: view)
        let isSecurityScoped = url.startAccessingSecurityScopedResource()
        print("\(#function) - iSecurityScoped = \(isSecurityScoped)")

        print("\(#function) - document at \(url)")

        let filename = String(UUID().uuidString.suffix(6)) + "_" + url.lastPathComponent
        let newURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(filename)

        let readingIntent = NSFileAccessIntent.readingIntent(with: url, options: .withoutChanges)

        fileCoordinator.coordinate(with: [readingIntent], queue: queue) { [weak self] error in
            guard let self = self else {
                if isSecurityScoped {
                    url.stopAccessingSecurityScopedResource()
                }
                return
            }

            defer {
                if isSecurityScoped {
                    url.stopAccessingSecurityScopedResource()
                }
            }

            DispatchQueue.main.async {
                self.stopSpinner()
            }

            if let error = error {
                print("\(#function) - \(error)")
                self.showDlg(success: false, msg: error.localizedDescription)
                return
            }

            let safeURL = readingIntent.url

            do {
                let fileData = try Data(contentsOf: safeURL)
                try fileData.write(to: newURL, options: .atomic)
                print("\(#function) - SUCCESS - newURL = \(newURL)")
                self.showDlg(success: true, msg: "It worked!")
                DispatchQueue.main.async {
                    self.cv.reloadData()
                }
            } catch {
                print("\(#function) - NOOOOO - \(error)")
                self.showDlg(success: false, msg: error.localizedDescription)
            }
        }
    }

    private func showDlg(success: Bool, msg: String) {
        DispatchQueue.main.async {
            let ac = UIAlertController(title: success ? "SUCCESS" : "FAILURE", message: msg, preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default)
            ac.addAction(ok)
            self.present(ac, animated: true)
        }
    }
}

extension ViewController: UICollectionViewDataSource {
    private func getItems() {
        items.removeAll()
        let docDirURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]

        do {
            let fileURLs = try FileManager.default.contentsOfDirectory(at: docDirURL, includingPropertiesForKeys: nil)
            fileURLs.forEach {
                items.append($0)
            }
        } catch {
            print("Error while enumerating files \(error)")
        }
    }

    func collectionView(_: UICollectionView, numberOfItemsInSection _: Int) -> Int {
        getItems()
        return items.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ItemCell.id, for: indexPath) as? ItemCell else {
            fatalError()
        }

        guard items.count > indexPath.item else {
            return cell
        }

        let item = items[indexPath.item]
        cell.item = item

        return cell
    }
}

// MARK: HasSpinner

extension ViewController: HasSpinner {
    var desiredLocation: CGPoint {
        return view.center
    }
}
