/// Copyright (c) 2022 Razeware LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// This project and source code may use libraries or frameworks that are
/// released under various Open-Source licenses. Use of those libraries and
/// frameworks are governed by their own individual licenses.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import Foundation

class Book: Hashable, Identifiable, Codable, ObservableObject {
    let id: UUID
    let imagePrefix: String
    let tagline: String
    let synopsis: String
    let amazonURL: URL
    let characters: [Character]

    @Published var title: String
    @Published var notes: [String]

    init(
        identifier: UUID,
        title: String,
        imagePrefix: String,
        tagline: String,
        synopsis: String,
        notes: [String],
        amazonURL: URL,
        characters: [Character]
    ) {
        id = identifier
        self.title = title
        self.imagePrefix = imagePrefix
        self.tagline = tagline
        self.synopsis = synopsis
        self.notes = notes
        self.amazonURL = amazonURL
        self.characters = characters
    }

    // MARK: - Equatable

    static func == (lhs: Book, rhs: Book) -> Bool {
        lhs.id == rhs.id
    }

    // MARK: - Hashable

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    // MARK: - Codable

    private enum CoderKeys: String, CodingKey {
        case identifier
        case title
        case imagePrefix
        case tagline
        case synopsis
        case notes
        case amazonURL
        case characters
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CoderKeys.self)
        try container.encode(id, forKey: .identifier)
        try container.encode(title, forKey: .title)
        try container.encode(imagePrefix, forKey: .imagePrefix)
        try container.encode(tagline, forKey: .tagline)
        try container.encode(synopsis, forKey: .synopsis)
        try container.encode(notes, forKey: .notes)
        try container.encode(amazonURL, forKey: .amazonURL)
        try container.encode(characters, forKey: .characters)
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CoderKeys.self)
        id = try container.decode(UUID.self, forKey: .identifier)
        title = try container.decode(String.self, forKey: .title)
        imagePrefix = try container.decode(String.self, forKey: .imagePrefix)
        tagline = try container.decode(String.self, forKey: .tagline)
        synopsis = try container.decode(String.self, forKey: .synopsis)
        notes = try container.decode([String].self, forKey: .notes)
        amazonURL = try container.decode(URL.self, forKey: .amazonURL)
        characters = try container.decode([Character].self, forKey: .characters)
    }
}
