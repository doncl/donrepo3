/// Copyright (c) 2022 Razeware LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// This project and source code may use libraries or frameworks that are
/// released under various Open-Source licenses. Use of those libraries and
/// frameworks are governed by their own individual licenses.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import SwiftUI

struct BookView: View {
    static let viewingCharacterDetailActivityType = "com.raywenderlich.hawknotes.staterestore.characterDetail"

    @EnvironmentObject var model: BooksModel
    @Environment(\.scenePhase) var scenePhase

    @State var path: [Character] = []

    @SceneStorage("BookView.ShowingAmazonPage") var isShowingAmazonPage = false
    @SceneStorage("BookView.SelectedCharacter") var encodedCharacterPath: String?
    @SceneStorage("BookView.newNote") var newNote: String = ""

    @Binding var book: Book

    let isCurrentlySelectedBook: Bool

    init(book: Binding<Book>, currentlySelectedTab: String) {
        _book = book
        isCurrentlySelectedBook = currentlySelectedTab == book.id.uuidString
    }

    var body: some View {
        GeometryReader { geoReader in
            NavigationStack(path: $path) {
                ScrollView {
                    VStack(alignment: .leading, spacing: 0) {
                        BookOverviewView(
                            image: Image("\(book.imagePrefix)_Cover"),
                            tagline: book.tagline,
                            synopsis: book.synopsis
                        )

                        Text("View in Amazon")
                            .foregroundColor(Color("AmazonOrange"))
                            .padding()
                            .frame(width: geoReader.size.width)
                            .onTapGesture {
                                isShowingAmazonPage.toggle()
                            }

                        NotesListView(
                            notes: $book.notes,
                            newNote: $newNote,
                            backgroundColor: Color("\(book.imagePrefix)_Primary")
                        )

                        ForEach(book.characters) { character in
                            NavigationLink(value: character) {
                                CharacterListRowView(character: character, imagePrefix: book.imagePrefix)
                                    .padding()
                                    .onDrag {
                                        let userActivity = NSUserActivity(activityType: BookView.viewingCharacterDetailActivityType)

                                        userActivity.title = character.name
                                        userActivity.targetContentIdentifier = character.id.uuidString

                                        try? userActivity.setTypedPayload(character)

                                        return NSItemProvider(object: userActivity)
                                    }
                            }
                        }
                        .navigationDestination(for: Character.self) { character in
                            CharacterView(character: character, imagePrefix: book.imagePrefix)
                        }
                    }
                }
            }
        }
        .accentColor(Color("\(book.imagePrefix)_Primary"))
        .fullScreenCover(isPresented: $isShowingAmazonPage) {
            SFSafariViewWrapper(url: book.amazonURL)
        }
        .onContinueUserActivity(BookView.viewingCharacterDetailActivityType) { userActivity in
            if let character = try? userActivity.typedPayload(Character.self) {
                path = [character]
            }
        }
        .onDisappear {
            model.save()
        }
        .onChange(of: scenePhase) { newScenePhase in
            if newScenePhase == .inactive {
                if isCurrentlySelectedBook {
                    if path.isEmpty {
                        encodedCharacterPath = nil
                    }

                    if let currentCharacter = path.first {
                        encodedCharacterPath = model.encodePathFor(character: currentCharacter, from: book)
                    }
                }
            }

            if newScenePhase == .active {
                if let characterPath = encodedCharacterPath,
                   let (stateRestoredBook, stateRestoredCharacter) = try? model.decodePathForCharacterFromBookUsing(characterPath)
                {
                    if stateRestoredBook.id == book.id {
                        path = [stateRestoredCharacter]
                    }
                }
            }
        }
    }
}

struct BookView_Previews: PreviewProvider {
    static let id = UUID()
    static var previews: some View {
        BookView(
            book: .constant(Book(
                identifier: id,
                title: "The Good Hawk",
                imagePrefix: "TGH_Cover",
                tagline: "This is a tagline",
                synopsis: "This is a synopsis",
                notes: [],
                // swiftlint:disable force_unwrapping
                amazonURL: URL(string: "https://www.amazon.com/Burning-Swift-Shadow-Three-Trilogy/dp/1536207497")!,
                characters: []
            )),
            currentlySelectedTab: id.uuidString
        )
    }
}
