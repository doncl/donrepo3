/// Copyright (c) 2022 Razeware LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// This project and source code may use libraries or frameworks that are
/// released under various Open-Source licenses. Use of those libraries and
/// frameworks are governed by their own individual licenses.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import Foundation

enum BookModelError: Error {
    case invalidPathEncoding
}

class BooksModel: Codable, ObservableObject {
    @Published var books: [Book] = []
    @Published var favoriteCharacters: [Character] = []

    private enum CodingKeys: String, CodingKey {
        case books
    }

    private let booksStorageFile = "HawkNotesBooks"

    init() {
        if let codedData = try? Data(contentsOf: dataModelURL()) {
            let decoder = JSONDecoder()
            if let decoded = try? decoder.decode([Book].self, from: codedData) {
                books = decoded
            }
        } else {
            books = Bundle.main.decode("books.json")
            save()
        }

        publishFavorites()
    }

    // MARK: - Public

    func encodePathFor(character: Character, from book: Book) -> String {
        "b|\(book.id)::c|\(character.id)"
    }

    func decodePathForCharacterFromBookUsing(_ path: String) throws -> (Book, Character) {
        let bookCharacter = path.components(separatedBy: "::")

        guard bookCharacter.count == 2 else {
            throw BookModelError.invalidPathEncoding
        }

        let bookComponents = bookCharacter[0].components(separatedBy: "|")
        let characterComponents = bookCharacter[1].components(separatedBy: "|")

        guard case bookComponents.count = 2,
              bookComponents[0] == "b",
              case characterComponents.count = 2,
              characterComponents[0] == "c"
        else {
            throw BookModelError.invalidPathEncoding
        }

        let bookID = bookComponents[1]
        let characterID = characterComponents[1]

        guard let book = books.first(where: { $0.id.uuidString == bookID }) else {
            throw BookModelError.invalidPathEncoding
        }

        guard let character = book.characters.first(where: { $0.id.uuidString == characterID }) else {
            throw BookModelError.invalidPathEncoding
        }

        return (book, character)
    }

    func characterBy(id: UUID) -> Character? {
        let allCharacters = books.reduce([]) { $0 + $1.characters }
        guard let character = allCharacters.first(where: { $0.id == id }) else {
            return nil
        }
        return character
    }

    func book(introducing character: Character) -> Book? {
        for book in books {
            if book.characters.first(where: { $0.id.uuidString == character.id.uuidString }) != nil {
                return book
            }
        }

        return nil
    }

    // MARK: Private

    func publishFavorites() {
        favoriteCharacters = books.reduce([]) { $0 + $1.characters.filter { $0.isFavorite } }
    }

    // MARK: - Codable

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(books, forKey: .books)
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        books = try values.decode(Array.self, forKey: .books)
    }

    private func documentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }

    private func dataModelURL() -> URL {
        let docURL = documentsDirectory()
        return docURL.appendingPathComponent(booksStorageFile)
    }

    func save() {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(books) {
            do {
                try encoded.write(to: dataModelURL())
            } catch {
                print("Couldn't write to save file: " + error.localizedDescription)
            }
        }

        publishFavorites()
    }
}

// MARK: Bundle

extension Bundle {
    func decode(_ file: String) -> [Book] {
        guard let url = url(forResource: file, withExtension: nil) else {
            fatalError("Failed to locate \(file) in bundle.")
        }
        guard let data = try? Data(contentsOf: url) else {
            fatalError("Failed to load \(file) from bundle.")
        }
        let decoder = JSONDecoder()
        guard let decodedBooks = try? decoder.decode([Book].self, from: data) else {
            fatalError("Failed to decode \(file) from bundle.")
        }
        return decodedBooks
    }
}
