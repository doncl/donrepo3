//
//  ContentView.swift
//  ref
//
//  Created by Don Clore on 4/15/23.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    VStack {
      List {
        ForEach(1..<101) { n in
          Text("item \(n)")
        }
      }
      .refreshable {
        try? await Task.sleep(nanoseconds: 5_000_000_000)        
      }
    }
    .padding()
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
