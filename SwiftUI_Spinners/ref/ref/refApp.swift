//
//  refApp.swift
//  ref
//
//  Created by Don Clore on 4/15/23.
//

import SwiftUI

@main
struct refApp: App {
  var body: some Scene {
    WindowGroup {
      ContentView()
    }
  }
}
