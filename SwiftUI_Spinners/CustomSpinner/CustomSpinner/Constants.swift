//
//  Constants.swift
//  CustomSpinner
//
//  Created by Don Clore on 4/16/23.
//

import Foundation

enum Constants {
  static let maxOffset = 100.0
  static let spinnerSize = 42.0
  static let spinnerSpacing = 8.0
  static let animationPhase2Duration = 0.35
  static let animationPhase3Duration = 0.3
  static let animationPhase1Duration = 1.0
}
