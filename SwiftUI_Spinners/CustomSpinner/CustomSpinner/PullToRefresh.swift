//
//  PullToRefresh.swift
//  CustomSpinner
//
//  Created by Don Clore on 4/16/23.
//

import Foundation

enum AnimationState: Int {
  case idle = 0
  case pulling = 1
  case ongoing = 2
  case preparingToFinish = 3
  case finishing = 4
}

struct PullToRefresh: Equatable {
  var progress: Double
  var state: AnimationState {
    didSet {
      print("Setting to State to \(state)")
    }
  }
}
