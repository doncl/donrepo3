//
//  ContentView.swift
//  CustomSpinner
//
//  Created by Don Clore on 4/16/23.
//

import SwiftUI

struct ContentView: View {
  @State var pullToRefresh = PullToRefresh(progress: 0, state: .idle)

  private let ease: Animation = .easeInOut(duration: Constants.animationPhase3Duration)

  private let spring: Animation = .interpolatingSpring(stiffness: 80, damping: 4)

  var body: some View {
    ScrollView {
      ScrollViewGeometryReader(pullToRefresh: $pullToRefresh) {
        await update()
      }
      ZStack(alignment: .top) {
        CustomSpinner(pullToRefresh: $pullToRefresh)
        LazyVStack(alignment: .leading) {
          ForEach(1 ..< 101) { n in
            Text("item \(n)")
              .padding(CGFloat(floatLiteral: 4))
          }
        }
        .padding(.leading)
        .offset(y: pullToRefresh.state != .idle ? Constants.maxOffset : 0)
        .animation(
          pullToRefresh.state != .finishing ? spring : ease, value: pullToRefresh.state
        )
      }
    }
  }

  @MainActor
  func update() async {
    try? await Task.sleep(nanoseconds: 5_000_000_000)
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
