//
//  CustomSpinner.swift
//  CustomSpinner
//
//  Created by Don Clore on 4/16/23.
//

import SwiftUI

struct Leaf: View {
  let rotation: Angle
  let isCurrent: Bool
  let isCompleting: Bool

  var body: some View {
    Capsule()
      .stroke(isCurrent ? Color.white : Color.gray, lineWidth: 8)
      .frame(width: 20, height: isCompleting ? 20 : 50)
      .offset(x: isCurrent ? 10 : 0, y: isCurrent ? 40 : 70)
      .animation(.easeInOut(duration: 1.5), value: isCurrent)
      .rotationEffect(isCompleting ? .zero : rotation)
      .animation(.easeInOut(duration: 1.5), value: isCurrent)
  }
}

struct CustomSpinner: View {
  let leavesCount: Int = 12
  @State private var currentIndex: Int?
  @State var completed = false
  @State var isVisible = true
  @State var currentOffset = CGSize.zero

  @Binding var pullToRefresh: PullToRefresh

  var body: some View {
    spinningLeavesSpinner()
      .opacity(pullToRefresh.state == .idle ? 0.0 : 1.0)
      .animation(.easeInOut(duration: 0.4), value: pullToRefresh.state)
  }

  private func spinningLeavesSpinner() -> some View {
    return ZStack {
      ForEach(0 ..< leavesCount) { index in
        Leaf(
          rotation: Angle(degrees: (Double(index) / Double(self.leavesCount)) * 360.0),
          isCurrent: index == currentIndex,
          isCompleting: self.completed
        )
      }
    }
    .onAppear(perform: animate)
    .offset(currentOffset)
    .blur(radius: currentOffset == .zero ? 0 : 10)
    .animation(.easeInOut(duration: 1.0), value: currentOffset)
    .scaleEffect(CGSize(width: 0.4, height: 0.4))
  }

  func animate() {
//    var iteration = 0

    Timer.scheduledTimer(withTimeInterval: 0.15, repeats: true) { _ in
      if let current = self.currentIndex {
        self.currentIndex = (current + 1) % self.leavesCount
      } else {
        self.currentIndex = 0
      }

//      iteration += 1
//      if iteration == 60 {
//        timer.invalidate()
//        self.complete()
//      }
    }
  }

  func complete() {
    guard !completed else {
      return
    }

    completed = true
    currentIndex = nil
    delay(seconds: 2) {
      withAnimation {
        self.isVisible = false
      }
    }
  }
}

struct CustomSpinner_Previews: PreviewProvider {
  static var pullToRefresh = PullToRefresh(progress: 0, state: .idle)
  static var previews: some View {
    let spinner = CustomSpinner(pullToRefresh: .constant(pullToRefresh))
    spinner.isVisible = true
    return spinner
  }
}

func delay(seconds: TimeInterval, block: @escaping () -> Void) {
  DispatchQueue.main.asyncAfter(deadline: .now() + seconds, execute: block)
}
