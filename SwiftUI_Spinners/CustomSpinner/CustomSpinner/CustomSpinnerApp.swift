//
//  CustomSpinnerApp.swift
//  CustomSpinner
//
//  Created by Don Clore on 4/16/23.
//

import SwiftUI

@main
struct CustomSpinnerApp: App {
  var body: some Scene {
    WindowGroup {
      ContentView()
    }
  }
}
