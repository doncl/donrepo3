//
//  ScrollViewGeometryReader.swift
//  CustomSpinner
//
//  Created by Don Clore on 4/16/23.
//

import SwiftUI

struct ScrollViewGeometryReader: View {
  @Binding var pullToRefresh: PullToRefresh
  let update: () async -> Void
  @State private var startOffset: CGFloat = 0

  var body: some View {
    GeometryReader<Color> { proxy in
      Task {
        calculateOffset(from: proxy)
      }
      return Color.clear // 2
    }
    .task { // 3
      await update()
    }
  }

  private func calculateOffset(from proxy: GeometryProxy) {
    let currentOffset = proxy.frame(in: .global).minY
//    let currentOffset = proxy.frame(in: .local).minY
    print("\(#function) - currentOffset = \(currentOffset)")

    switch pullToRefresh.state {
      case .idle:
        if startOffset == 0 {
          startOffset = currentOffset // 1
        } else {
          pullToRefresh.state = .pulling // 2
        }

      case .pulling where pullToRefresh.progress < 1: // 3
        pullToRefresh.progress = min(1, (currentOffset - startOffset) / Constants.maxOffset)

      case .pulling: // 4
        pullToRefresh.state = .ongoing
        pullToRefresh.progress = 0
        Task {
          await update()
          pullToRefresh.state = .preparingToFinish // 1
          after(Constants.animationPhase3Duration) {
            pullToRefresh.state = .finishing // 2
            after(Constants.animationPhase1Duration) {
              pullToRefresh.state = .idle // 3
              startOffset = 0
            }
          }
        }

      default: return
    }
  }

  func after(
    _ seconds: Double,
    execute: @escaping () -> Void
  ) {
    Task {
      let delay = UInt64(seconds * Double(NSEC_PER_SEC))
      try await Task<Never, Never>
        .sleep(nanoseconds: delay)
      execute()
    }
  }
}
