//
//  WrappedCustomSpinner.swift
//  UIKitWrappedSpinners
//
//  Created by Don Clore on 4/17/23.
//

import SwiftUI
import UIKit

struct WrappedCustomSpinner: UIViewRepresentable {
  typealias UIViewType = UIKitCustomSpinner

  func makeUIView(context _: Context) -> UIKitCustomSpinner {
    defer {
      start()
    }
    return spinner
  }

  func updateUIView(_: UIKitCustomSpinner, context _: Context) {}

  let spinner: UIKitCustomSpinner

  init(frame: CGRect, color: UIColor) {
    spinner = UIKitCustomSpinner(frame: frame, color: color)
  }
  
  func stop() {
    spinner.stop()
  }
  
  func start() {
    spinner.start()
  }
}
