//
//  UIKitCustomSpinner.swift
//  UIKitWrappedSpinners
//
//  Created by Don Clore on 4/17/23.
//

import UIKit

class UIKitCustomSpinner: UIView {
  enum Constants {
    static let numberOfMarkers: Double = 12.0
    static let defaultSpread: CGFloat = 70.0
    static let defaultThickness: CGFloat = 14.0
    static let defaultLength: CGFloat = 70.0
    static let defaultSpeed: TimeInterval = 1.0

    static let defaultHUDSide: CGFloat = 200.0
    static let defaultHUDColor: UIColor = .init(white: 0.0, alpha: 0.5)
    static let markerAnimationKey: String = "MarkerAnimationKey"
  }

  let color: UIColor
  let marker: CALayer = .init()
  let spinnerReplicator: CAReplicatorLayer = .init()
  let fade: CABasicAnimation = .init(keyPath: "opacity")

  init(frame: CGRect, color: UIColor) {
    self.color = color
    super.init(frame: frame)

    marker.bounds = CGRect(x: 0, y: 0, width: Constants.defaultThickness, height: Constants.defaultLength)
    marker.cornerRadius = Constants.defaultThickness * 0.5
    marker.backgroundColor = color.cgColor
    marker.position = CGPoint(x: Constants.defaultHUDSide * 0.5, y: Constants.defaultHUDSide * 0.5 + Constants.defaultSpread)

    spinnerReplicator.bounds = CGRect(x: 0, y: 0, width: Constants.defaultHUDSide, height: Constants.defaultHUDSide)
    spinnerReplicator.cornerRadius = 10.0
    spinnerReplicator.backgroundColor = UIColor.clear.cgColor
    spinnerReplicator.position = CGPoint(x: frame.midX, y: frame.midY)

    let angle = CGFloat(2 * Double.pi / Constants.numberOfMarkers)
    let instanceRotation: CATransform3D = CATransform3DMakeRotation(angle, 0, 0, 1)
    spinnerReplicator.instanceTransform = instanceRotation

    spinnerReplicator.addSublayer(marker)
    layer.addSublayer(spinnerReplicator)

    marker.opacity = 0.0
    fade.fromValue = NSNumber(floatLiteral: 1.0)
    fade.toValue = NSNumber(floatLiteral: 0.0)
    fade.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
    fade.repeatCount = Float.greatestFiniteMagnitude
    fade.duration = Constants.defaultSpeed
    let markerAnimationDuration = Constants.defaultSpeed / Constants.numberOfMarkers
    spinnerReplicator.instanceDelay = markerAnimationDuration
  }

  @available(*, unavailable)
  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func start() {
    spinnerReplicator.instanceCount = Int(Constants.numberOfMarkers)
    marker.add(fade, forKey: Constants.markerAnimationKey)
  }

  func stop() {
    marker.removeAllAnimations()
    spinnerReplicator.instanceCount = 0

    removeFromSuperview()
  }
}
