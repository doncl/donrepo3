//
//  UIKitWrappedSpinnersApp.swift
//  UIKitWrappedSpinners
//
//  Created by Don Clore on 4/17/23.
//

import SwiftUI

@main
struct UIKitWrappedSpinnersApp: App {
  var body: some Scene {
    WindowGroup {
      ContentView()
    }
  }
}
