//
//  ContentView.swift
//  UIKitWrappedSpinners
//
//  Created by Don Clore on 4/17/23.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    GeometryReader { geo in
      let frame = CGRect(x: 0, y: 0, width: geo.size.width, height: geo.size.height)
      
      WrappedCustomSpinner(frame: frame, color: .gray)
    }
    .padding()
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
