//
//  FeedApp.swift
//  Shared
//
//  Created by Don Clore on 6/12/22.
//

import SwiftUI

@main
struct FeedApp: App {
  var body: some Scene {
    WindowGroup {
      FeedView()
    }
  }
}
