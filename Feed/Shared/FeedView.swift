//
//  ContentView.swift
//  Shared
//
//  Created by Don Clore on 6/12/22.
//

import SwiftUI

struct FeedView: View {
  var seedColors: [Color] = [.black, .blue, .brown, .cyan, .gray, .indigo, .mint,
                             .orange, .pink, .purple, .red, .teal, .white, .yellow,  ]

  var body: some View {
    let colors = getColors()
    return GeometryReader { proxy in
      TabView {
        ForEach(colors, id: \.self) { color in
          color // Your cell content
        }
        .rotationEffect(.degrees(-90)) // Rotate content
        .frame(
          width: proxy.size.width,
          height: proxy.size.height
        )
      }
      .frame(
        width: proxy.size.height, // Height & width swap
        height: proxy.size.width
      )
      .rotationEffect(.degrees(90), anchor: .topLeading) // Rotate TabView
      .offset(x: proxy.size.width) // Offset back into screens bounds
      .tabViewStyle(
        PageTabViewStyle(indexDisplayMode: .never)
      )
    }
  }

  private func getColors() -> [Color] {
    var ret: [Color] = []
    for _ in 0..<100 {
      for color in seedColors {
        ret.append(color)
      }
    }
    return ret
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    FeedView()
  }
}
