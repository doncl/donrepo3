//
//  Cell.swift
//  Feed
//
//  Created by Don Clore on 6/12/22.
//

import SwiftUI

struct Cell: View {
  let color: Color

  var body: some View {
    GeometryReader { geo in
      Rectangle()
        .fill(color)
        .frame(width: geo.size.width, height: geo.size.height)
        .fixedSize(horizontal: true, vertical: true)
    }
  }
}

struct Cell_Previews: PreviewProvider {
  static var previews: some View {
    Cell(color: Color.blue)
  }
}
