//
//  Coordinator.swift
//  Coordinators
//
//  Created by Don Clore on 6/15/22.
//

import Foundation
import UIKit

protocol Coordinator: AnyObject {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }

    func start()
}
