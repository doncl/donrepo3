//
//  BuyCoordinator.swift
//  Coordinators
//
//  Created by Don Clore on 6/15/22.
//

import Foundation
import UIKit

class BuyCoordinator: Coordinator {
    weak var parentCoordinator: MainCoordinator?

    var product: Int = 0

    var childCoordinators: [Coordinator] = []

    var navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        let vc = BuyViewController.instantiate()
        vc.coordinator = self
        vc.selectedProduct = product
        navigationController.pushViewController(vc, animated: true)
    }
}
