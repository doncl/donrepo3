//
//  ViewController.swift
//  Coordinators
//
//  Created by Don Clore on 6/15/22.
//

import UIKit

class ViewController: UIViewController, Storyboarded {
    weak var coordinator: MainCoordinator?

    @IBOutlet var product: UISegmentedControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func buyTapped(_: UIButton) {
        guard let coordinator = coordinator else {
            return
        }

        let product: Int = product.selectedSegmentIndex

        coordinator.buySubscription(to: product)
    }

    @IBAction func createAccountTapped(_: UIButton) {
        guard let coordinator = coordinator else {
            return
        }

        coordinator.createAccount()
    }
}
