//
//  BuyViewController.swift
//  Coordinators
//
//  Created by Don Clore on 6/15/22.
//

import UIKit

class BuyViewController: UIViewController, Storyboarded {
    weak var coordinator: BuyCoordinator?

    var selectedProduct = 0

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
