//
//  CloudsView.swift
//  Weather
//
//  Created by Don Clore on 7/23/22.
//

import SwiftUI

struct CloudsView: View {
    var cloudGroup: CloudGroup

    var body: some View {
        TimelineView(.animation) { _ in
            Canvas { context, _ in
                context.opacity = cloudGroup.opacity
            }
        }
        .ignoresSafeArea()
        .frame(maxWidth: .infinity, maxHeight: .infinity)
    }

    init(thickness: Cloud.Thickness) {
        cloudGroup = CloudGroup(thickness: thickness)
    }
}

struct CloudsView_Previews: PreviewProvider {
    static var previews: some View {
        CloudsView(thickness: Cloud.Thickness.regular)
    }
}
