//
//  WeatherApp.swift
//  Weather
//
//  Created by Don Clore on 7/23/22.
//

import SwiftUI

@main
struct WeatherApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
