//
//  LayoutAndIdentityApp.swift
//  LayoutAndIdentity
//
//  Created by Don Clore on 4/20/23.
//

import SwiftUI

@main
struct LayoutAndIdentityApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
