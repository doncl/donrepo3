//
//  Explicit.swift
//  OverridingAnimations
//
//  Created by Don Clore on 4/20/23.
//

import SwiftUI

struct Explicit: View {
    @State var scale = 1.0
    
    var body: some View {
        Button("Tap Me") {
            withMotionAnimation {
                scale += 1
            }
        }
        .scaleEffect(scale)
    }
}

struct Explicit_Previews: PreviewProvider {
    static var previews: some View {
        Explicit()
    }
}


func withMotionAnimation<Result>(_ animation: Animation? = .default, _ body: () throws -> Result) rethrows -> Result {
    if UIAccessibility.isReduceMotionEnabled {
        return try body()
    } else {
        return try withAnimation(animation, body)
    }
}
