//
//  Implicit.swift
//  OverridingAnimations
//
//  Created by Don Clore on 4/20/23.
//

import SwiftUI

struct Implicit: View {
    @State var scale = 1.0
    
    var body: some View {
        Button("Tap Me") {
            scale += 1
        }
        .scaleEffect(scale)
        .motionAnimation(.default, value: scale)
    }
}

struct Implicit_Previews: PreviewProvider {
    static var previews: some View {
        Implicit()
    }
}

struct MotionAnimationModifier<V: Equatable>: ViewModifier {
    @Environment(\.accessibilityReduceMotion) var accessibilityReduceMotion
    
    let animation: Animation?
    let value: V
    
    func body(content: Content) -> some View {
        if accessibilityReduceMotion {
            content
        } else {
            content.animation(animation, value: value)
        }
    }
}

extension View {
    func motionAnimation<V: Equatable>(_ animation: Animation?, value: V) -> some View {
        self.modifier(MotionAnimationModifier(animation: animation, value: value))
    }
}
