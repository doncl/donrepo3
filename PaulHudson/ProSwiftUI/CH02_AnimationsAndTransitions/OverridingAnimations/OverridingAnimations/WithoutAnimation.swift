//
//  WithoutAnimation.swift
//  OverridingAnimations
//
//  Created by Don Clore on 4/20/23.
//

import SwiftUI

struct WithoutAnimation: View {
    @State private var scale = 1.0
    @State private var colorIndex = 0
    
    let colors = [Color.gray, Color.red, Color.orange, Color.green, Color.white, Color.blue]
    
    var body: some View {
        VStack {
            Button("Tap Me") {
                withoutAnimation {
                    scale += 1
                }
                colorIndex = colorIndex == colors.count - 1 ? 0 : colorIndex + 1
            }
            .padding()
            .background(Color.white)
            .foregroundColor(.black)
            .scaleEffect(scale)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(colors[colorIndex])
        .animation(.easeOut(duration: 2.0), value: colorIndex)
        
    }
}

struct WithoutAnimation_Previews: PreviewProvider {
    static var previews: some View {
        WithoutAnimation()
    }
}

func withoutAnimation<Result>(_ body: () throws -> Result) rethrows -> Result {
    var transaction = Transaction()
    transaction.disablesAnimations = true
    return try withTransaction(transaction, body)
}

