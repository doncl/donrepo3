//
//  OverridingAnimationsApp.swift
//  OverridingAnimations
//
//  Created by Don Clore on 4/20/23.
//

import SwiftUI

@main
struct OverridingAnimationsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
