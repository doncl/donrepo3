//
//  ContentView.swift
//  OverridingAnimations
//
//  Created by Don Clore on 4/20/23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        TabView {
            Explicit()
                .tabItem {
                    Text("Explicit")
                }
            Implicit()
                .tabItem {
                    Text("Implicit")
                }
            WithoutAnimation()
                .tabItem {
                    Text("Sans")
                }
            HighPri()
                .tabItem {
                    Text("HiPri")
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
