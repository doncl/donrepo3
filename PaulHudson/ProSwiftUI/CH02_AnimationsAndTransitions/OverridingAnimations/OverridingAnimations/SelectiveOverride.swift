//
//  SelectiveOverride.swift
//  OverridingAnimations
//
//  Created by Don Clore on 4/20/23.
//

import SwiftUI

struct SelectiveOverride: View {
    @State var useRedFill = false
        
    var body: some View {
        VStack {
            CircleGrid(useRedFill: useRedFill)
            Spacer()
            Button("Toggle Color") {
                withAnimation(.easeInOut) {
                    useRedFill.toggle()
                }
            }
        }
    }
}

struct SelectiveOverride_Previews: PreviewProvider {
    static var previews: some View {
        SelectiveOverride()
    }
}

struct CircleGrid: View {
    var useRedFill = false
    
    var body: some View {
        LazyVGrid(columns: [.init(.adaptive(minimum: 64))]) {
            ForEach(0..<30) { i in
                Circle()
                    .fill(useRedFill ? .red : .blue)
                    .frame(height: 64)
                    .transaction { transaction in
                        transaction.animation = transaction.animation?.delay(Double(i) / 10)
                    }
            }
        }
    }
}
