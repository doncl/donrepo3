//
//  AdvancedTransitionsApp.swift
//  AdvancedTransitions
//
//  Created by Don Clore on 4/20/23.
//

import SwiftUI

@main
struct AdvancedTransitionsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
