//
//  ContentView.swift
//  CustomTimingCurves
//
//  Created by Don Clore on 4/20/23.
//

import SwiftUI

struct ContentView: View {
    @State private var offset = -100.0

    var body: some View {
        Text("Hello, world!")
            .offset(y: offset)
            .onTapGesture {
                withAnimation(.edgeBounce(duration: 1).repeatForever(autoreverses: true)) {
                    offset = 100
                }
            }
    }

}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

extension Animation {
    static var edgeBounce: Animation {
        Animation.timingCurve(0, 1, 1, 0)
    }

    static func edgeBounce(duration: TimeInterval = 0.2) -> Animation {
        Animation.timingCurve(0, 1, 1, 0, duration: duration)
    }
}

