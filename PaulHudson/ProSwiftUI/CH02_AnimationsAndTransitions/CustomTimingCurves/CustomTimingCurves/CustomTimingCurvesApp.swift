//
//  CustomTimingCurvesApp.swift
//  CustomTimingCurves
//
//  Created by Don Clore on 4/20/23.
//

import SwiftUI

@main
struct CustomTimingCurvesApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
