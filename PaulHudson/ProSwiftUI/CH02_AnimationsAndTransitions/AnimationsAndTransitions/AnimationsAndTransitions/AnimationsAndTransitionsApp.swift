//
//  AnimationsAndTransitionsApp.swift
//  AnimationsAndTransitions
//
//  Created by Don Clore on 4/20/23.
//

import SwiftUI

@main
struct AnimationsAndTransitionsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
