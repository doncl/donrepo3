//
//  ContentView.swift
//  AnimationsAndTransitions
//
//  Created by Don Clore on 4/20/23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        TabView {
            ExplicitAnimation()
                .tabItem {
                    Label("Explicit", systemImage: "arrow.up.and.down.and.arrow.left.and.right")
                }
            ImplicitAnimation()
                .tabItem {
                    Label("Implicit", systemImage: "arrow.up.left.and.down.right.and.arrow.up.right.and.down.left")
                }
            ZAxisNonWorkingAnimation()
                .tabItem {
                    Label("ZNot", systemImage: "exclamationmark.triangle")
                }
            ZAxisWorkingAnimation()
                .tabItem {
                    Label("ZYes", systemImage: "eyes")
                }
            TypeWriter()
                .tabItem {
                    Label("TypeWriter", systemImage: "keyboard")
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
