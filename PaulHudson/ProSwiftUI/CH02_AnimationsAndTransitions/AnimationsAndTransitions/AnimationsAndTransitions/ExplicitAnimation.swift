//
//  ExplicitAnimation.swift
//  AnimationsAndTransitions
//
//  Created by Don Clore on 4/20/23.
//

import SwiftUI

struct ExplicitAnimation: View {
    @State private var scale = 1.0

    var body: some View {
        Text("Hello, World!")
            .scaleEffect(scale)
            .onTapGesture {
                withAnimation {
                    scale += 1
                }
            }
    }
}

struct ExplicitAnimation_Previews: PreviewProvider {
    static var previews: some View {
        ExplicitAnimation()
    }
}
