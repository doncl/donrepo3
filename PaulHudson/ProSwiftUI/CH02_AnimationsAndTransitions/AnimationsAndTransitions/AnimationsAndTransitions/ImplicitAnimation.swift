//
//  ImplicitAnimation.swift
//  AnimationsAndTransitions
//
//  Created by Don Clore on 4/20/23.
//

import SwiftUI

struct ImplicitAnimation: View {
    @State private var scale = 1.0

        var body: some View {
            Text("Hello, World!")
                .scaleEffect(scale)
                .onTapGesture {
                    scale += 1
                }
                .animation(.default, value: scale)
        }
}

struct ImplicitAnimation_Previews: PreviewProvider {
    static var previews: some View {
        ImplicitAnimation()
    }
}
