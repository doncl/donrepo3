//
//  ZAxisNonWorkingAnimation.swift
//  AnimationsAndTransitions
//
//  Created by Don Clore on 4/20/23.
//

import SwiftUI

struct ZAxisNonWorkingAnimation: View {
     @State private var redAtFront = false
    let colors: [Color] = [.blue, .green, .orange, .purple, .mint]

    var body: some View {
        VStack {
            Button("Toggle zIndex - Doesn't animate!") {
                withAnimation(.linear(duration: 1)) {
                    redAtFront.toggle()
                }
            }

            ZStack {
                RoundedRectangle(cornerRadius: 25)
                    .fill(.red)
                    .zIndex(redAtFront ? 6 : 0)

                ForEach(0..<5) { i in
                    RoundedRectangle(cornerRadius: 25)
                        .fill(colors[i])
                        .offset(x: Double(i + 1) * 20, y: Double(i + 1) * 20)
                        .zIndex(Double(i))
                }
            }
            .frame(width: 200, height: 200)
        }
    }
}

struct ZAxisNonWorkingAnimation_Previews: PreviewProvider {
    static var previews: some View {
        ZAxisNonWorkingAnimation()
    }
}
