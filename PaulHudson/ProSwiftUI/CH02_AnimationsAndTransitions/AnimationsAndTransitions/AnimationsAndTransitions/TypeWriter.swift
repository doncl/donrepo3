//
//  TypeWriter.swift
//  AnimationsAndTransitions
//
//  Created by Don Clore on 4/20/23.
//

import SwiftUI

struct TypeWriter: View {
    @State private var value = 0
    let message = "This is a very long piece of text that appears letter by letter"
    
    var body: some View {
        VStack {
            TypewriterText(string: message, count: value)
                .frame(width: 300, alignment: .leading)
            
            Button("Type!") {
                withAnimation(.linear(duration: 2)) {
                    value = message.count
                }
            }
            Button("Reset") {
                value = 0
            }
        }
    }
}

struct TypeWriter_Previews: PreviewProvider {
    static var previews: some View {
        TypeWriter()
    }
}


struct TypewriterText: View, Animatable {
    @Environment(\.accessibilityVoiceOverEnabled) var accessibilityVoiceOverEnabled
    @Environment(\.accessibilityReduceMotion) var accessibilityReduceMotion
    

    var string: String
    var count = 0

    var animatableData: Double {
        get { Double(count) }
        set { count = Int(max(0, newValue)) }
    }

    var body: some View {
        let stringToShow = String(string.prefix(count))
        
        if accessibilityVoiceOverEnabled || accessibilityReduceMotion {
            Text(string)
        } else {
            ZStack {
                Text(string)
                    .hidden()
                    .overlay(
                        Text(stringToShow),
                        alignment: .topLeading
                    )
            }
        }
    }
}

