//
//  PaulAsymGridApp.swift
//  PaulAsymGrid
//
//  Created by Don Clore on 10/5/22.
//

import SwiftUI

@main
struct PaulAsymGridApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
