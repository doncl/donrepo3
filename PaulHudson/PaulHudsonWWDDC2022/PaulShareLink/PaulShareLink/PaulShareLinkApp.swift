//
//  PaulShareLinkApp.swift
//  PaulShareLink
//
//  Created by Don Clore on 10/5/22.
//

import SwiftUI

@main
struct PaulShareLinkApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
