//
//  ContentView.swift
//  PaulShareLink
//
//  Created by Don Clore on 10/5/22.
//

import SwiftUI

struct ContentView: View {
    let link = URL(string: "https://www.hackingwithswift.com")!
    var body: some View {
        ShareLink(
            item: link,
            preview: SharePreview("Switzerland's flag: it's a big plus", image: Image(systemName: "plus"))
        )

//    VStack(spacing: 20 ) {
//      ShareLink(item: link, message:Text("You can add a message to the link"))
//      ShareLink(item: link)
//      ShareLink("Learn Swift link", item: link)
//      ShareLink(item: link) {
//        Label("Learn Swift Here", systemImage: "swift")
//      }
//    }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
