//
//  ContentView.swift
//  PaulBoldItalicToggling
//
//  Created by Don Clore on 10/6/22.
//

import SwiftUI

struct ContentView: View {
    @State private var useBold = false
    @State private var useItalic = false

    var body: some View {
        VStack {
            Text("Welcome to SwiftUI 4.0")
                .bold(useBold)
                .italic(useItalic)

            Toggle("Use Bold", isOn: $useBold)
            Toggle("Use Italic", isOn: $useItalic)
        }
        .padding()
        .font(.title)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
