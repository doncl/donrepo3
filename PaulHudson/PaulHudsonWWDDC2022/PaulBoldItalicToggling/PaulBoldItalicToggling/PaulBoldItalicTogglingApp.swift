//
//  PaulBoldItalicTogglingApp.swift
//  PaulBoldItalicToggling
//
//  Created by Don Clore on 10/6/22.
//

import SwiftUI

@main
struct PaulBoldItalicTogglingApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
