//
//  PaulTextAnimationApp.swift
//  PaulTextAnimation
//
//  Created by Don Clore on 10/6/22.
//

import SwiftUI

@main
struct PaulTextAnimationApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
