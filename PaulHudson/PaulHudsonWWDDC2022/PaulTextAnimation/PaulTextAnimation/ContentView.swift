//
//  ContentView.swift
//  PaulTextAnimation
//
//  Created by Don Clore on 10/6/22.
//

import SwiftUI

struct ContentView: View {
    @State private var useBlack = false

    var body: some View {
        Text("Hello, world")
            .fontWeight(useBlack ? .black : .ultraLight)
            .onTapGesture {
                withAnimation {
                    useBlack.toggle()
                }
            }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
