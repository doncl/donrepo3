//
//  ContentView.swift
//  PaulTextLineLimitRange
//
//  Created by Don Clore on 10/6/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("This is some long text that is limited to a specific line range, so anything beyond that range will cause the text to clip")
            .lineLimit(3 ... 6)
            .frame(width: 200)
            .font(.title)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
