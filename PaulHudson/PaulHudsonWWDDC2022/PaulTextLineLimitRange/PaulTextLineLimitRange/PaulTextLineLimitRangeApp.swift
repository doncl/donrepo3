//
//  PaulTextLineLimitRangeApp.swift
//  PaulTextLineLimitRange
//
//  Created by Don Clore on 10/6/22.
//

import SwiftUI

@main
struct PaulTextLineLimitRangeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
