//
//  PaulMultiDatePickerApp.swift
//  PaulMultiDatePicker
//
//  Created by Don Clore on 10/6/22.
//

import SwiftUI

@main
struct PaulMultiDatePickerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
