//
//  ContentView.swift
//  PaulMultiDatePicker
//
//  Created by Don Clore on 10/6/22.
//

import SwiftUI

struct ContentView: View {
    @State private var dates = Set<DateComponents>()
    @Environment(\.calendar) var calendar

    var summary: String {
        dates.compactMap { components in
            calendar.date(from: components)?.formatted(date: .long, time: .omitted)
        }.formatted()
    }

    var body: some View {
        VStack {
            MultiDatePicker("Selected your preferred dates", selection: $dates, in: Date.now...)
            Text(summary)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
