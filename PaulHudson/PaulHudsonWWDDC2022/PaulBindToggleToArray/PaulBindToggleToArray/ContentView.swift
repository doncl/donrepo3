//
//  ContentView.swift
//  PaulBindToggleToArray
//
//  Created by Don Clore on 10/6/22.
//

import SwiftUI

struct EmailList: Identifiable {
    var id: String
    var isSubscribed: Bool = false
}

struct ContentView: View {
    @State private var lists = [
        EmailList(id: "Monthly Updates", isSubscribed: true),
        EmailList(id: "Newsflashes", isSubscribed: true),
        EmailList(id: "Special Offers", isSubscribed: true),
    ]

    @State private var bools: [Bool] = [true, true]
    var body: some View {
        Form {
            Section {
                ForEach($lists) { $list in
                    Toggle(list.id, isOn: $list.isSubscribed)
                }
            }

            // Doesn't seem to work
//      Section {
//        Toggle(isOn: $lists.map(\.isSubscribed)) {
//          Text("Subscribe to all")
//        }
//      }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
