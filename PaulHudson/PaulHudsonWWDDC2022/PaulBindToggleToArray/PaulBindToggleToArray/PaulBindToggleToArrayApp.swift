//
//  PaulBindToggleToArrayApp.swift
//  PaulBindToggleToArray
//
//  Created by Don Clore on 10/6/22.
//

import SwiftUI

@main
struct PaulBindToggleToArrayApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
