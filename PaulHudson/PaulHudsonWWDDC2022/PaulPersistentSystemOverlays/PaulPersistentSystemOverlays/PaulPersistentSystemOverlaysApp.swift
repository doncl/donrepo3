//
//  PaulPersistentSystemOverlaysApp.swift
//  PaulPersistentSystemOverlays
//
//  Created by Don Clore on 10/6/22.
//

import SwiftUI

@main
struct PaulPersistentSystemOverlaysApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
