//
//  ContentView.swift
//  PaulOnTapGesture
//
//  Created by Don Clore on 10/5/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Circle()
            .fill(.red)
            .frame(width: 100, height: 100)
            .onTapGesture { location in
                print("I tapped it at \(location)")
            }
//      .onTapGesture(coordinateSpace: .global) { location in
//        print("I tapped it at globally \(location)")
//      }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
