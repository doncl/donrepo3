//
//  PaulOnTapGestureApp.swift
//  PaulOnTapGesture
//
//  Created by Don Clore on 10/5/22.
//

import SwiftUI

@main
struct PaulOnTapGestureApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
