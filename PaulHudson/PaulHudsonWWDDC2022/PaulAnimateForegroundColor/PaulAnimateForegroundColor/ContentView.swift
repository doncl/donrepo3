//
//  ContentView.swift
//  PaulAnimateForegroundColor
//
//  Created by Don Clore on 10/6/22.
//

import SwiftUI

struct ContentView: View {
    @State private var useRed = false

    var body: some View {
        Text("WWDC22")
            .font(.largeTitle.bold())
            .foregroundColor(useRed ? .red : .black)
            .onTapGesture {
                withAnimation {
                    useRed.toggle()
                }
            }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
