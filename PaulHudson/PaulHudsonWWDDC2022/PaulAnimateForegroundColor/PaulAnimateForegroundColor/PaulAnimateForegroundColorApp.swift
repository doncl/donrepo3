//
//  PaulAnimateForegroundColorApp.swift
//  PaulAnimateForegroundColor
//
//  Created by Don Clore on 10/6/22.
//

import SwiftUI

@main
struct PaulAnimateForegroundColorApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
