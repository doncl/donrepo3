//
//  PaulTicTacToeApp.swift
//  PaulTicTacToe
//
//  Created by Don Clore on 10/5/22.
//

import SwiftUI

@main
struct PaulTicTacToeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
