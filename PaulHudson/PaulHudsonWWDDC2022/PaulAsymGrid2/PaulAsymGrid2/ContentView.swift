//
//  ContentView.swift
//  PaulAsymGrid2
//
//  Created by Don Clore on 10/5/22.
//

import SwiftUI

struct ContentView: View {
    @State private var redScore = 0
    @State private var blueScore = 0

    var body: some View {
        Grid {
            GridRow {
                Text("Food")
                Text("$200")
            }

            GridRow {
                Text("Rent")
                Text("$800")
            }

            GridRow {
                Text("Candles")
                Text("$3600")
            }

            Divider()

            GridRow {
                Text("$4600")
                    .gridCellColumns(2)
                    .multilineTextAlignment(.trailing)
            }
        }
        .font(.title)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
