//
//  PaulAsymGrid2App.swift
//  PaulAsymGrid2
//
//  Created by Don Clore on 10/5/22.
//

import SwiftUI

@main
struct PaulAsymGrid2App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
