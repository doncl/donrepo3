//
//  PaulAnyLayoutApp.swift
//  PaulAnyLayout
//
//  Created by Don Clore on 10/6/22.
//

import SwiftUI

@main
struct PaulAnyLayoutApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
