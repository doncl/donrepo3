//
//  PaulSearchableScopeApp.swift
//  PaulSearchableScope
//
//  Created by Don Clore on 10/6/22.
//

import SwiftUI

@main
struct PaulSearchableScopeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
