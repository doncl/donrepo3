//
//  ContentView.swift
//  PaulSearchableScope
//
//  Created by Don Clore on 10/6/22.
//

import SwiftUI

struct Message: Identifiable, Codable {
    let id: Int
    var user: String
    var text: String
}

enum SearchScope: String, CaseIterable {
    case inbox
    case favorites
}

struct ContentView: View {
    @State private var messages = [Message]()
    @State private var searchText = ""
    @State private var searchScope = SearchScope.inbox

    var filteredMessages: [Message] {
        if searchText.isEmpty {
            return messages
        } else {
            return messages.filter { $0.text.localizedCaseInsensitiveContains(searchText) }
        }
    }

    var body: some View {
        NavigationStack {
            List {
                ForEach(filteredMessages) { message in
                    VStack(alignment: .leading) {
                        Text(message.user)
                            .font(.headline)

                        Text(message.text)
                    }
                }
            }
            .searchable(text: $searchText)
            .searchScopes($searchScope) {
                ForEach(SearchScope.allCases, id: \.self) { scope in
                    Text(scope.rawValue.capitalized)
                        .tag(scope)
                }
            }
            .navigationTitle("Messages")
        }
        .onSubmit(of: .search, runSearch)
        .onChange(of: searchScope) { _ in
            runSearch()
        }
    }

    private func runSearch() {
        Task {
            messages.removeAll()
            guard let url = URL(string: "https://hws.dev/\(searchScope.rawValue).json") else {
                return
            }

            print("\(#function) - searchScope is \(searchScope.rawValue)")
            print("\(#function) - URL is \(url.absoluteString)")

            let (data, _) = try await URLSession.shared.data(from: url)
            messages = try JSONDecoder().decode([Message].self, from: data)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
