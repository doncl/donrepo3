//
//  PaulBottomSheetsApp.swift
//  PaulBottomSheets
//
//  Created by Don Clore on 10/5/22.
//

import SwiftUI

@main
struct PaulBottomSheetsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
