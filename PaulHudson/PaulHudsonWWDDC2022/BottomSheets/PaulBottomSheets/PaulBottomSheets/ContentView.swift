//
//  ContentView.swift
//  PaulBottomSheets
//
//  Created by Don Clore on 10/5/22.
//

import SwiftUI

struct ContentView: View {
    @State private var showingCredits = false

    var body: some View {
        Button("Show Credits") {
            showingCredits.toggle()
        }
        .sheet(isPresented: $showingCredits) {
            Text("This app was brought to you from the Apple Developer Center")
                // .presentationDetents([.medium, .large])
//        .presentationDetents([.height(300)])
                .presentationDetents([.fraction(0.2), .fraction(0.65), .large])
                .presentationDragIndicator(.visible) // don't use this unless you're sure.
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
