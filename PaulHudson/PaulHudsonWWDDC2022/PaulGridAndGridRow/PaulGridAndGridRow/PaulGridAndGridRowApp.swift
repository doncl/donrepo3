//
//  PaulGridAndGridRowApp.swift
//  PaulGridAndGridRow
//
//  Created by Don Clore on 10/5/22.
//

import SwiftUI

@main
struct PaulGridAndGridRowApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
