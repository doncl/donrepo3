//
//  ContentView.swift
//  PaulGridAndGridRow
//
//  Created by Don Clore on 10/5/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Grid(alignment: .leading) {
            GridRow {
                Text("Top Leading")
                    .background(.red)

                Text("Top Trailing")
                    .background(.orange)
            }
            GridRow {
                GridRow {
                    Text("Bottom Leading")
                        .background(.blue)

                    Text("Bottom Trailing")
                        .background(.green)
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
