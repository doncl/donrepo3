//
//  ContentView.swift
//  PaulSFSymbols4
//
//  Created by Don Clore on 10/6/22.
//

import SwiftUI

struct ContentView: View {
    @State private var value = 0.0

    var body: some View {
        VStack {
            HStack {
                Image(systemName: "aqi.low", variableValue: value)
                Image(systemName: "wifi", variableValue: value)
                Image(systemName: "chart.bar.fill", variableValue: value)
                Image(systemName: "waveform", variableValue: value)
            }

            Slider(value: $value)
        }
        .font(.system(size: 72))
        .foregroundColor(.blue)
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
