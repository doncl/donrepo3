//
//  PaulSFSymbols4App.swift
//  PaulSFSymbols4
//
//  Created by Don Clore on 10/6/22.
//

import SwiftUI

@main
struct PaulSFSymbols4App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
