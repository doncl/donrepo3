//
//  ContentView.swift
//  PaulRequestUserReview
//
//  Created by Don Clore on 10/6/22.
//

import StoreKit
import SwiftUI

struct ContentView: View {
    @Environment(\.requestReview) var requestReview

    var body: some View {
        // Do this in a smart, non-spammy way, not like this.
        Button {
            requestReview()
        } label: {
            Text("Review the App")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
