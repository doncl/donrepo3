//
//  PaulRequestUserReviewApp.swift
//  PaulRequestUserReview
//
//  Created by Don Clore on 10/6/22.
//

import SwiftUI

@main
struct PaulRequestUserReviewApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
