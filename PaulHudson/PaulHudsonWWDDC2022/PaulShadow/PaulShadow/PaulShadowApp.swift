//
//  PaulShadowApp.swift
//  PaulShadow
//
//  Created by Don Clore on 10/6/22.
//

import SwiftUI

@main
struct PaulShadowApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
