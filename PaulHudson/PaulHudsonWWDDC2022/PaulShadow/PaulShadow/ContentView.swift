//
//  ContentView.swift
//  PaulShadow
//
//  Created by Don Clore on 10/6/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Circle()
                .fill(.red.shadow(.drop(color: .black, radius: 20)))
                .padding()

            Circle()
                .fill(.red.shadow(.inner(color: .black, radius: 20)))
                .padding()

            Circle()
                .fill(.red.gradient.shadow(.drop(color: .black, radius: 20, x: 10, y: 10)))
                .padding()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
