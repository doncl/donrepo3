//
//  PaulApp.swift
//  Paul
//
//  Created by Don Clore on 10/3/22.
//

import SwiftUI

@main
struct PaulApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
