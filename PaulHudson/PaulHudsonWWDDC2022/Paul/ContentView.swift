//
//  ContentView.swift
//  Paul
//
//  Created by Don Clore on 10/3/22.
//

import SwiftUI

struct Team: Identifiable, Hashable {
    let id = UUID()
    var name: String
    var players: [String]
}

struct ContentView: View {
    @State private var teams = [
        Team(name: "AFC Richmond", players: ["Dani", "Jamie", "Roy"]),
    ]

    @State private var selectedTeam: Team?
    @State private var selectedPlayer: String?

    var body: some View {
        NavigationSplitView {
            List(teams, selection: $selectedTeam) { team in
                Text(team.name)
                    .tag(team)
            }
            .navigationSplitViewColumnWidth(250)
        } content: {
            List(selectedTeam?.players ?? [], id: \.self, selection: $selectedPlayer, rowContent: Text.init)
        } detail: {
            Text(selectedPlayer ?? "Please choose a player")
        }
        .navigationSplitViewStyle(.balanced)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
