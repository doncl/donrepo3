//
//  PaulGradientApp.swift
//  PaulGradient
//
//  Created by Don Clore on 10/6/22.
//

import SwiftUI

@main
struct PaulGradientApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
