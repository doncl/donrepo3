//
//  ContentView.swift
//  PaulGradient
//
//  Created by Don Clore on 10/6/22.
//

import SwiftUI

struct ContentView: View {
    let colors: [Color] = [.blue, .mint, .green, .yellow, .orange, .red, .purple, .indigo]

    var body: some View {
        VStack {
            ForEach(colors, id: \.self) { color in
                Rectangle().fill(color.gradient)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
