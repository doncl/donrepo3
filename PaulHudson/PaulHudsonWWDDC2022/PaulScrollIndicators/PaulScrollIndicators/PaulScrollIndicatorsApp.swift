//
//  PaulScrollIndicatorsApp.swift
//  PaulScrollIndicators
//
//  Created by Don Clore on 10/6/22.
//

import SwiftUI

@main
struct PaulScrollIndicatorsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
