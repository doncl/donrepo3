//
//  PaulScrollDismissesKeyboardApp.swift
//  PaulScrollDismissesKeyboard
//
//  Created by Don Clore on 10/6/22.
//

import SwiftUI

@main
struct PaulScrollDismissesKeyboardApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
