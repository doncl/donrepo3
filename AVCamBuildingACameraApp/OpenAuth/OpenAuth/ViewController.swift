//
//  ViewController.swift
//  OpenAuth
//
//  Created by Don Clore on 3/19/22.
//

import UIKit

class ViewController: UIViewController {
    let email = UITextField()
    let password = UITextField()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        email.placeholder = "email"
        password.placeholder = "password"

        [email, password].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview($0)
        }

        NSLayoutConstraint.activate([
            email.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 100),
            email.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 32),
            email.heightAnchor.constraint(equalToConstant: 60),
            email.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -100),

            password.topAnchor.constraint(equalTo: email.bottomAnchor, constant: 32),
            password.leadingAnchor.constraint(equalTo: email.leadingAnchor),
            password.trailingAnchor.constraint(equalTo: email.trailingAnchor),
            password.heightAnchor.constraint(equalTo: email.heightAnchor),
        ])
    }
}
