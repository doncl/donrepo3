//
//  SceneDelegate.swift
//  CostcoHomework
//
//  Created by Don Clore on 2/15/23.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo _: UISceneSession, options _: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else {
            return
        }

        let win = UIWindow(windowScene: windowScene)
        window = win

        let vc = HomeVC(viewModel: HomeViewModel())
        let rootVC = RootVC(rootViewController: vc)
        win.rootViewController = rootVC
        win.makeKeyAndVisible()
    }
}
