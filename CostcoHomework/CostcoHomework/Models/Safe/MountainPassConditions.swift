//
//  MountainPassConditions.swift
//  CostcoHomework
//
//  Created by Don Clore on 2/15/23.
//

import Foundation

struct MountainPassConditions {
    struct Restriction {
        let restrictionText: String
        let travelDirection: String
    }

    let dateUpdated: String
    let elevationInFeet: Int
    let latitude: Float
    let longitude: Float
    let mountainPassID: Int
    let mountainPassName: String
    let restrictions: [Restriction]
    let roadCondition: String
    let temperatureInFahrenheit: Int
    let travelAdvisoryActive: Bool
    let weatherCondition: String
}

extension MountainPassConditions.Restriction: Hashable {}
