//
//  Coordinator.swift
//  CostcoHomework
//
//  Created by Don Clore on 2/15/23.
//

import UIKit

// This is a little different from the way people normally do coordinators - it doesn't
// hold on to a UINavigationController, or a tree of parent/child navCtrollers.
// The job of this coordinator is just to guide and direct the navigation choices.
// The act of actually modally presenting or push/showing a vc is done by the VC who is
// asking the coordinator for guidance.

enum Destination: String {
    case snoqualmieRoadConditions = "snoqualmie"
    case stevensPass = "stevens"
    case manastashRidge = "manastash"
}

// This is imperfect, in a perfect world, we'd have the associated data for .modal and .push be some sort of
// navigatable object, in order to keep the Coordinator from having to know anything about UIKit.
// It's out of scope, I'm doing this just to show my intent here.

enum Navigation {
    case modal(UIViewController, URL?) // we could stick modalpresentationtype, and transitioning delegates, etc. in associated data.
    case push(UIViewController, URL?) // use navigationController on your vc to effect the launch.
}

protocol Coordinator {
    func navigateTo(_ destination: Destination, completion: @escaping ((Result<Navigation, Error>) -> Void))
}

struct HomeCoordinator: Coordinator {
    static var shared: Coordinator = HomeCoordinator()
    private var implementation: Coordinator

    private init() {
        implementation = CoordinatorDefaultImplementation()
    }

    func navigateTo(_ destination: Destination, completion: @escaping ((Result<Navigation, Error>) -> Void)) {
        implementation.navigateTo(destination, completion: completion)
    }

    mutating func inject(implementation: Coordinator) {
        self.implementation = implementation
    }
}
