//
//  CoordinatorError.swift
//  CostcoHomework
//
//  Created by Don Clore on 2/15/23.
//

import Foundation

enum CoordinatorError: Error {
    case noMatchingConditionsForDestination
}
