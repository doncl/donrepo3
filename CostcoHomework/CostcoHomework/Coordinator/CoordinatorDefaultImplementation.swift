//
//  CoordinatorDefaultImplementation.swift
//  CostcoHomework
//
//  Created by Don Clore on 2/15/23.
//

import Foundation
import SwiftUI

struct CoordinatorDefaultImplementation: Coordinator {
    func navigateTo(_ destination: Destination, completion: @escaping ((Result<Navigation, Error>) -> Void)) {
        // This is a bit of a crude hack, and but it seems outside of scope to make it production-worthy.
        // The fact is, we probably would have some low-priority background thread that polled the conditions
        // on some appropriate quantum (1 minute, 1 hour, 12 hours, no idea), and then would cache the
        // data.   Probably.  There would be state management issues about going into background, and all
        // the usual.

        // But this code just brute-force calls the API, gets all the conditions, and then figures out, crudely,
        // which one of the array of road conditions returns is the one that is desired.

        MountainPassConditionsController.shared.getConditions { result in
            switch result {
            case let .success(conditions):
                handle(conditions: conditions, forDestination: destination, completion: completion)

            case let .failure(error):
                completion(.failure(error))
            }
        }
    }

    private func handle(
        conditions: [MountainPassConditions],
        forDestination destination: Destination,
        completion: @escaping ((Result<Navigation, Error>) -> Void)
    ) {
        guard !conditions.isEmpty else {
            completion(.failure(CoordinatorError.noMatchingConditionsForDestination))
            return
        }

        for condition in conditions {
            if condition.mountainPassName.lowercased().contains(destination.rawValue) {
                // OK, in real code I'd figure out how to keep the SwiftUI stuff out of this, but for now, this is good.
                let swiftUIView = MountainPassView(conditions: condition)
                let vc = UIHostingController(rootView: swiftUIView)
                let navigation = Navigation.push(vc, nil)
                completion(.success(navigation))
            }
        }
    }
}
