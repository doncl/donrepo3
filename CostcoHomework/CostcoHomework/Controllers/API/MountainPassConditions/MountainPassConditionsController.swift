//
//  MountainPassConditionsController.swift
//  CostcoHomework
//
//  Created by Don Clore on 2/15/23.
//

import Foundation

typealias MountainPassConditionsResponse = [MountainPassConditionsUnsafeEntity]

enum MountainPassConditionsError: Error {
    case failedToFormURL(String)
    case jsonDecodingError(Error)
    case emptyReturn
}

protocol MountainPassConditionsProtocol {
    func getConditions(completion: @escaping ((Result<[MountainPassConditions], Error>) -> Void))
}

struct MountainPassConditionsDefaultImplementation: MountainPassConditionsProtocol {
    private enum Constants {
        static let apiKey = "3e6583ec-e1b2-4fea-be2e-3dbc0b4a766b"
        static let getConditionsURLBase = """
        https://wsdot.wa.gov/Traffic/api/MountainPassConditions/MountainPassConditionsREST.svc/GetMountainPassConditionsAsJson?AccessCode=
        """
    }

    func getConditions(completion: @escaping ((Result<[MountainPassConditions], Error>) -> Void)) {
        let urlString = "\(Constants.getConditionsURLBase)\(Constants.apiKey)"
        guard let url = URL(string: urlString) else {
            completion(.failure(MountainPassConditionsError.failedToFormURL(urlString)))
            return
        }

        Network.shared.getData(url: url, headers: [:]) { result in
            DispatchQueue.main.async {
                switch result {
                case let .success(data):
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .millisecondsSince1970

                    do {
                        let response = try decoder.decode(MountainPassConditionsResponse.self, from: data)
                        let returnValue: [MountainPassConditions] = response.compactMap { $0.toModel() }
                        guard !returnValue.isEmpty else {
                            completion(.failure(MountainPassConditionsError.emptyReturn))
                            return
                        }
                        completion(.success(returnValue))

                    } catch {
                        completion(.failure(MountainPassConditionsError.jsonDecodingError(error)))
                    }

                case let .failure(error):
                    completion(.failure(error))
                }
            }
        }
    }
}

struct MountainPassConditionsController: MountainPassConditionsProtocol {
    static let shared = MountainPassConditionsController()

    private var implementation: MountainPassConditionsProtocol

    private init() {
        implementation = MountainPassConditionsDefaultImplementation()
    }

    func getConditions(completion: @escaping ((Result<[MountainPassConditions], Error>) -> Void)) {
        implementation.getConditions(completion: completion)
    }

    mutating func inject(implementation: MountainPassConditionsProtocol) {
        self.implementation = implementation
    }
}
