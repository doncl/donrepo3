//
//  MountainPassConditionsUnsafeEntity.swift
//  CostcoHomework
//
//  Created by Don Clore on 2/15/23.
//

import Foundation

struct MountainPassConditionsUnsafeEntity: Codable {
    struct RestrictionResponse: Codable {
        enum CodingKeys: String, CodingKey {
            case restrictionText = "RestrictionText"
            case travelDirection = "TravelDirection"
        }

        let restrictionText: String?
        let travelDirection: String?

        func toModel() -> MountainPassConditions.Restriction? {
            guard let restrictionText, let travelDirection else {
                return nil
            }

            return MountainPassConditions.Restriction(restrictionText: restrictionText, travelDirection: travelDirection)
        }
    }

    enum CodingKeys: String, CodingKey {
        case dateUpdated = "DateUpdated"
        case elevationInFeet = "ElevationInFeet"
        case latitude = "Latitude"
        case longitude = "Longitude"
        case mountainPassID = "MountainPassId"
        case mountainPassName = "MountainPassName"
        case restrictionOne = "RestrictionOne"
        case restrictionTwo = "RestrictionTwo"
        case roadCondition = "RoadCondition"
        case temperatureInFahrenheit = "TemperatureInFahrenheit"
        case travelAdvisoryActive = "TravelAdvisoryActive"
        case weatherCondition = "WeatherCondition"
    }

    let dateUpdated: String?
    let elevationInFeet: Int?
    let latitude: Float?
    let longitude: Float?
    let mountainPassID: Int?
    let mountainPassName: String?
    let restrictionOne: RestrictionResponse?
    let restrictionTwo: RestrictionResponse?
    let roadCondition: String?
    let temperatureInFahrenheit: Int?
    let travelAdvisoryActive: Bool?
    let weatherCondition: String?
}

extension MountainPassConditionsUnsafeEntity {
    func toModel() -> MountainPassConditions? {
        // this is where you decide which properties are required for this to work.
        // For this homework assignment, I'm just going to pretend they all are but the restrictions

        guard let dateUpdated,
              let elevationInFeet,
              let latitude,
              let longitude,
              let mountainPassID,
              let mountainPassName,
              let roadCondition,
              let temperatureInFahrenheit,
              let travelAdvisoryActive,
              let weatherCondition
        else {
            return nil
        }

        var restrictions: [MountainPassConditions.Restriction] = []
        if let restrictionOne, let model = restrictionOne.toModel() {
            restrictions.append(model)
        }
        if let restrictionTwo, let model = restrictionTwo.toModel() {
            restrictions.append(model)
        }
        let model = MountainPassConditions(
            dateUpdated: dateUpdated,
            elevationInFeet: elevationInFeet,
            latitude: latitude,
            longitude: longitude,
            mountainPassID: mountainPassID,
            mountainPassName: mountainPassName,
            restrictions: restrictions,
            roadCondition: roadCondition,
            temperatureInFahrenheit: temperatureInFahrenheit,
            travelAdvisoryActive: travelAdvisoryActive,
            weatherCondition: weatherCondition
        )

        return model
    }
}
