//
//  HomeVC.swift
//  CostcoHomework
//
//  Created by Don Clore on 2/15/23.
//

import UIKit
import WebKit

class HomeVC: UIViewController {
    lazy var webConfig: WKWebViewConfiguration = {
        let config = WKWebViewConfiguration()
        return config
    }()

    lazy var webVu: WKWebView = {
        let vu = WKWebView(frame: .zero, configuration: self.webConfig)
        vu.translatesAutoresizingMaskIntoConstraints = false
        vu.navigationDelegate = self
        let request = URLRequest(url: URL(string: "https://www.costco.com")!)

        vu.load(request)

        return vu
    }()

    let viewModel: HomeViewModel

    init(viewModel: HomeViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(webVu)
        navigationItem.title = "Costco main view"

        NSLayoutConstraint.activate([
            webVu.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            webVu.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            webVu.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            webVu.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
        ])
    }
}

extension HomeVC: WKNavigationDelegate {
    func webView(
        _: WKWebView,
        decidePolicyFor navigationAction: WKNavigationAction,
        decisionHandler: @escaping (WKNavigationActionPolicy) -> Void
    ) {
        guard let url = navigationAction.request.url else {
            decisionHandler(.allow)
            return
        }

        let handled = viewModel.handled(url: url) { [weak self] result in
            guard let self else { return }

            switch result {
            case let .success(navigation):
                switch navigation {
                case let .modal(vc, _):
                    self.present(vc, animated: true)

                case let .push(vc, _):
                    guard let nav = self.navigationController else {
                        return
                    }
                    nav.pushViewController(vc, animated: true)
                }
            case let .failure(error):
                print("\(#function) \(error)") // this is where things get hairy.
            }
        }
        let policy: WKNavigationActionPolicy = handled ? .cancel : .allow
        decisionHandler(policy)
    }
}
