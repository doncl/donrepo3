//
//  HomeViewModel.swift
//  CostcoHomework
//
//  Created by Don Clore on 2/15/23.
//

import UIKit

class HomeViewModel {
    func handled(
        url: URL,
        completion: @escaping ((Result<Navigation, Error>) -> Void)
    ) -> Bool {
        let ret: Bool

        if url.absoluteString.contains("warehouse") {
            navigateTo(.snoqualmieRoadConditions, completion: completion)
            ret = true
        } else if url.absoluteString.contains("LogonForm") {
            navigateTo(.manastashRidge, completion: completion)
            ret = true
        } else {
            ret = false
        }

        return ret
    }

    func navigateTo(
        _ destination: Destination,
        completion: @escaping ((Result<Navigation, Error>) -> Void)
    ) {
        HomeCoordinator.shared.navigateTo(destination, completion: completion)
    }
}
