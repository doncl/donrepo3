//
//  MountainPassView.swift
//  CostcoHomework
//
//  Created by Don Clore on 2/15/23.
//

import SwiftUI

struct MountainPassView: View {
    let conditions: MountainPassConditions
    lazy var df: DateFormatter = {
        let df = DateFormatter()
        df.dateStyle = .short
        return df
    }()

    var body: some View {
        VStack {
            HStack {
                Text(conditions.mountainPassName)
                    .font(.headline)
            }.padding(.top)

            Form {
                HStack {
                    Text("Name:")
                        .font(.headline)

                    Text(conditions.mountainPassName)
                        .font(.body)
                }

                HStack {
                    Text("Elevation:")
                        .font(.headline)

                    Text("\(conditions.elevationInFeet) ft.")
                        .font(.body)
                }

                HStack {
                    Text("Latitude:")
                        .font(.headline)

                    Text(String(format: "%.2f", conditions.latitude))
                        .font(.body)
                }

                HStack {
                    Text("Longitude:")
                        .font(.headline)
                    Text(String(format: "%.2f", conditions.longitude))
                        .font(.body)
                }

                VStack(alignment: .leading) {
                    Text("Road Condition:")
                        .font(.headline)

                    Text(conditions.roadCondition)
                        .font(.body)
                }
            }

            Form {
                Text("Restrictions")
                    .font(.headline)
                ForEach(conditions.restrictions, id: \.self) { restriction in
                    VStack(alignment: .leading) {
                        HStack {
                            Text("Notes: ")
                                .font(.headline)

                            Text(restriction.restrictionText)
                                .font(.body)
                        }

                        HStack {
                            Text("Travel Direction: ")
                                .font(.headline)

                            Text(restriction.travelDirection)
                                .font(.body)
                        }
                    }
                }
            }
        }
    }

    init(conditions: MountainPassConditions) {
        self.conditions = conditions
    }

    private func dateStringFromConditions() -> String {
        let df = DateFormatter()
        df.dateStyle = .short
        let dateString = conditions.dateUpdated
        return dateString
    }
}

struct MountainPassView_Previews: PreviewProvider {
    static let conditions = MountainPassConditions(
        dateUpdated: "/Date(1676526174447-0800)/",
        elevationInFeet: 10000,
        latitude: 47,
        longitude: -121,
        mountainPassID: 42,
        mountainPassName: "Some wacky mountainPass",
        restrictions: [
            MountainPassConditions.Restriction(
                restrictionText: "Don't go there",
                travelDirection: "North"
            ),
            MountainPassConditions.Restriction(
                restrictionText: "It's bad",
                travelDirection: "South"
            ),
        ],
        roadCondition: "Terrible",
        temperatureInFahrenheit: 4,
        travelAdvisoryActive: true,
        weatherCondition: "Scary"
    )

    static var previews: some View {
        MountainPassView(conditions: conditions)
    }
}
