//
//  RootVC.swift
//  CostcoHomework
//
//  Created by Don Clore on 2/15/23.
//

import UIKit

class RootVC: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
    }
}
