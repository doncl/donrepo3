//
//  NetworkController.swift
//  CostcoHomework
//
//  Created by Don Clore on 2/15/23.
//

import Foundation

enum NetworkError: Error {
    case nilResult
    case serviceReturnedError(Error)
    case unexpectedError(String)
    case httpError(Int, String)
}

struct Network {
    static var shared: Network = .init()

    var session = URLSession.shared

    private init() {}

    // This is old-school Result<T, Error> returning, unfairly called 'callback hell' by detractors.
    // The fact is, it still works better in the Xcode debugger than async await calls do.
    func getData(url: URL, headers: [String: String], completion: @escaping ((Result<Data, Error>) -> Void)) {
        var req = URLRequest(url: url)

        for header in headers {
            req.addValue(header.value, forHTTPHeaderField: header.key)
        }

        let task = session.dataTask(with: req) { data, response, error in
            if let error = error {
                completion(.failure(NetworkError.serviceReturnedError(error)))
                return
            }

            guard let httpResp = response as? HTTPURLResponse else {
                completion(.failure(NetworkError.unexpectedError("Response is not an HTTPURLResponse, what gives?")))
                return
            }

            guard 200 ..< 300 ~= httpResp.statusCode else {
                completion(.failure(NetworkError.httpError(httpResp.statusCode, "HTTPS error")))
                return
            }

            guard let data = data else {
                completion(.failure(NetworkError.nilResult))
                return
            }

            completion(.success(data))
        }

        task.resume()
    }

    // Async-await version (FDD)
    // Do I really care about this syntatic sugar?
    // Nope.   I have no problems with callbacks; I'm agnostic.
    // But some do, and so I'm showing I know how to make this happen.
    // As far as I can tell, there is not an async/await version of URLSession methods that lets you add headers.
    // I may be mistaken, but there is probably value in showing that I know how to do this anyway, I reckon.
    func getSomethingAsync(url: URL, headers: [String: String]) async -> Result<Data, Error> {
        var req = URLRequest(url: url)

        for header in headers {
            req.addValue(header.value, forHTTPHeaderField: header.key)
        }

        do {
            let tuple: (data: Data, response: URLResponse) = try await session.data(fromURLRequest: req)
            guard let httpResp = tuple.response as? HTTPURLResponse else {
                return .failure(NetworkError.unexpectedError("Response is not an HTTPURLResponse, what gives?"))
            }

            guard 200 ..< 300 ~= httpResp.statusCode else {
                return .failure(NetworkError.httpError(httpResp.statusCode, "HTTPS error"))
            }

            return .success(tuple.data)
        } catch {
            return .failure(error)
        }
    }
}

extension URLSession {
    func data(fromURLRequest request: URLRequest) async throws -> (Data, URLResponse) {
        try await withCheckedThrowingContinuation { continuation in
            let task = self.dataTask(with: request) { data, response, error in
                guard let data = data, let response = response else {
                    let error = error ?? URLError(.badServerResponse)
                    return continuation.resume(throwing: error)
                }
                continuation.resume(returning: (data, response))
            }

            task.resume()
        }
    }
}
