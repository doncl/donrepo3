//
//  ModalSheetBoy.swift
//  TransitionDemo
//
//  Created by Don Clore on 5/23/23.
//

import SwiftUI

struct ModalSheetBoy: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct ModalSheetBoy_Previews: PreviewProvider {
    static var previews: some View {
        ModalSheetBoy()
    }
}
