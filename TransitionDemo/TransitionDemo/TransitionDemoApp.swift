//
//  TransitionDemoApp.swift
//  TransitionDemo
//
//  Created by Don Clore on 5/23/23.
//

import SwiftUI

@main
struct TransitionDemoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
