//
//  ContentView.swift
//  TransitionDemo
//
//  Created by Don Clore on 5/23/23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        TabView {
            NavExampleLinks()
                .tabItem {
                    Text("Nav")
                }
            
            TabTwoPlaceholderView()
                .tabItem {
                    Text("2nd tab")
                }
            
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
