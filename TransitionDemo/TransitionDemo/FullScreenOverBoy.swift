//
//  FullScreenOverBoy.swift
//  TransitionDemo
//
//  Created by Don Clore on 5/23/23.
//

import SwiftUI

struct FullScreenOverBoy: View {
    var body: some View {
        VStack {
            Text("Over FULL SCREEN!")
                .font(.largeTitle)
                .foregroundColor(.black)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Color.purple)
        
    }
}

struct FullScreenOverBoy_Previews: PreviewProvider {
    static var previews: some View {
        FullScreenOverBoy()
    }
}
