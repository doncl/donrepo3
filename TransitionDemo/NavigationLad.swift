//
//  NavigationLad.swift
//  TransitionDemo
//
//  Created by Don Clore on 5/23/23.
//

import SwiftUI

struct NavigationLad: View {
    var body: some View {
        VStack {
//            Text("I'm a hierarchical push! Dig on my navbar")
//                .font(.largeTitle)
//                .foregroundColor(.white)
            
            List(1..<1000) { i in
                NavigationLink("item \(i)") {
                    NavTargetKid(n: i)
                }
            }
        }
        //.frame(maxWidth: .infinity, maxHeight: .infinity)
        .toolbarBackground(Color.purple)
        .background(Color.gray)
        .navigationBarTitleDisplayMode(.large)
        
    }
}

struct NavigationLad_Previews: PreviewProvider {
    static var previews: some View {
        NavigationLad()
    }
}
