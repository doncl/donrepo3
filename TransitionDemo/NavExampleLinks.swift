//
//  NavExampleLinks.swift
//  TransitionDemo
//
//  Created by Don Clore on 5/23/23.
//

import SwiftUI

struct NavExampleLinks: View {
    @State private var path = NavigationPath()
    @State private var showingFullScreenBoy = false
    @State private var showingSheet = false
    
    var body: some View {
        VStack(alignment: .leading) {
            NavigationStack(path: $path) {
                NavigationLink{
                    NavigationLad()
                } label: {
                    Text("Push Navigation")
                }
                .padding()
                
                Button {
                    showingFullScreenBoy = true
                } label: {
                    Text("Over full Screen")
                }
                .padding()
                
                Button {
                    showingSheet = true
                } label: {
                    Text("Modal Sheet")
                }
                .padding()
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)

        .fullScreenCover(isPresented: $showingFullScreenBoy) {
             FullScreenOverBoy()
         }
         .sheet(isPresented: $showingSheet) {
              ModalSheetBoy()
        }
    }
}

struct NavExampleLinks_Previews: PreviewProvider {
    static var previews: some View {
        NavExampleLinks()
    }
}
