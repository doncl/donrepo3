//
//  NavTargetKid.swift
//  TransitionDemo
//
//  Created by Don Clore on 5/23/23.
//

import SwiftUI

struct NavTargetKid: View {
    let n: Int
    var body: some View {
        Text("I'm nav target #\(n)")
            .font(.title3)
    }
}

struct NavTargetKid_Previews: PreviewProvider {
    static var previews: some View {
        NavTargetKid(n: 10)
    }
}
