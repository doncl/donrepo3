//
//  ModalSheetGirl.swift
//  TransitionDemo
//
//  Created by Don Clore on 5/23/23.
//

import SwiftUI

struct ModalSheetGirl: View {
    var body: some View {
        VStack {
            Text("I'm Modal Sheet Woman! Hear me roar!")
                .font(.largeTitle)
                .foregroundColor(.black)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Color.purple)
    }
}

struct ModalSheetGirl_Previews: PreviewProvider {
    static var previews: some View {
        ModalSheetGirl()
    }
}
