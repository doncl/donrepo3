//
//  TabTwoPlaceholderView.swift
//  TransitionDemo
//
//  Created by Don Clore on 5/23/23.
//

import SwiftUI

struct TabTwoPlaceholderView: View {
    var body: some View {
        Text("Tab Two Placeholder")
            .font(.largeTitle)
            .foregroundColor(.black)
            .background(Color.orange)
            .frame(maxWidth: .infinity, maxHeight: .infinity)
    }
}

struct TabTwoPlaceholderView_Previews: PreviewProvider {
    static var previews: some View {
        TabTwoPlaceholderView()
    }
}
