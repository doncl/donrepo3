//
//  ResultsView.swift
//  Project4 Watch App
//
//  Created by Don Clore on 9/18/22.
//

import Combine
import SwiftUI

struct ResultsView: View {
    enum FetchState {
        case fetching
        case success
        case failed
    }

    let appID: String = "990c8e2f6eaa40378c1d7c0d779b54f9"

    @State private var fetchState = FetchState.fetching
    @State private var fetchedCurrencies = [(symbol: String, rate: Double)]()
    @State private var request: AnyCancellable?

    let amount: Double
    let baseCurrency: String

    var body: some View {
        Group {
            if fetchState == .success {
                List {
                    ForEach(fetchedCurrencies, id: \.symbol) { currency in
                        Text(rate(for: currency))
                    }
                }
            } else {
                Text(fetchState == .fetching ? "Fetching..." : "Fetch failed")
            }
        }
        .onAppear {
            fetchData()
        }
        .navigationTitle("\(Int(amount)) \(baseCurrency)")
    }

    private func parse(result: CurrencyResult) {
        if result.rates.isEmpty {
            fetchState = .failed
        } else {
            fetchState = .success

            let selectedCurrencies = UserDefaults.standard.array(forKey: ContentView.selectedCurrenciesKey) as? [String] ?? ContentView.defaultCurrencies

            for symbol in result.rates {
                guard selectedCurrencies.contains(symbol.key) else {
                    continue
                }

                let rateName = symbol.key
                let rateValue = symbol.value
                fetchedCurrencies.append((symbol: rateName, rate: rateValue))
            }

            fetchedCurrencies.sort { $0.symbol < $1.symbol }
        }
    }

    private func rate(for currency: (symbol: String, rate: Double)) -> String {
        let value = currency.rate * amount
        let rate = String(format: "%.2f", value)
        return "\(currency.symbol) \(rate)"
    }

    private func fetchData() {
        guard let url = URL(string: "https://openexchangerates.org/api/latest.json?app_id=\(appID)&base=\(baseCurrency)") else {
            return
        }
        let publisher = URLSession.shared.dataTaskPublisher(for: url)
            .map(\.data)
            .breakpoint(receiveOutput: { data -> Bool in
                if let s = String(data: data, encoding: .utf8) {
                    print(s)
                }
                return true
            })
            .decode(type: CurrencyResult.self, decoder: JSONDecoder())
            .replaceError(with: CurrencyResult(base: "", rates: [:]))
            .receive(on: DispatchQueue.main)

        request = publisher.handleEvents(receiveSubscription: { _ in

        }, receiveOutput: { output in
            print(output)
        }, receiveCompletion: { _ in
            print("received completion")
        }, receiveCancel: {
            print("Cancelled")
        }).sink(receiveValue: parse(result:))

//    let req = URLRequest(url: url)
//    let task = URLSession.shared.dataTask(with: req) { data, response, error in
//      if let error = error {
//        print(error)
//        fatalError(error.localizedDescription)
//      }
//
//      if let httpResp = response as? HTTPURLResponse, let data = data {
//        guard let s = String(data: data, encoding: .utf8) else {
//          print("can't encode to string")
//          return
//        }
//
//        guard 200..<300 ~= httpResp.statusCode else {
//          fatalError("http error = \(httpResp.statusCode)")
//        }
//
//
//      }
//    }
//
//    task.resume()
//
    }
}

struct ResultsView_Previews: PreviewProvider {
    static var previews: some View {
        ResultsView(amount: 500, baseCurrency: "USD")
    }
}
