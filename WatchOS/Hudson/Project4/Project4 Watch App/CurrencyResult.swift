//
//  CurrencyResult.swift
//  Project4 Watch App
//
//  Created by Don Clore on 9/18/22.
//

import Foundation

struct CurrencyResult: Codable {
    let base: String
    let rates: [String: Double]
}
