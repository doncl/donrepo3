//
//  ContentView.swift
//  Project4 Watch App
//
//  Created by Don Clore on 9/18/22.
//

import SwiftUI

struct ContentView: View {
    @State private var amount = 500.0
    @State private var selectedCurrency = "USE"

    static let selectedCurrenciesKey = "SelectedCurrencies"
    static let defaultCurrencies = ["USD", "EUR"]

    static let currencies = ["USD", "AUD", "CAD", "CHF", "CNY", "EUR", "GBP", "HKD", "JPY", "SGD"]

    var body: some View {
        GeometryReader { geo in
            VStack(spacing: 0) {
                Text("\(Int(amount))")
                    .font(.system(size: 52))
                    .frame(height: geo.size.height / 3)

                Slider(value: $amount, in: 0 ... 1000, step: 20)
                    .accentColor(.green)
                    .frame(height: geo.size.height / 3)

                HStack {
                    Picker("Select a currency", selection: $selectedCurrency) {
                        ForEach(Self.currencies, id: \.self) { currency in
                            Text(currency)
                        }
                    }
                    .labelsHidden()
                    .frame(height: geo.size.height / 3)

                    NavigationLink(destination: ResultsView(amount: amount, baseCurrency: selectedCurrency)) {
                        Text("Go")
                    }
                    .frame(height: geo.size.width * 0.4)
                }
            }
        }
        .navigationTitle("WatchFX")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
