//
//  Project4App.swift
//  Project4 Watch App
//
//  Created by Don Clore on 9/18/22.
//

import SwiftUI

@main
struct Project4_Watch_AppApp: App {
    var body: some Scene {
        WindowGroup {
            NavigationView {
                MainView()
            }
        }
    }
}
