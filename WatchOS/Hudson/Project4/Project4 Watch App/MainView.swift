//
//  MainView.swift
//  Project4 Watch App
//
//  Created by Don Clore on 9/18/22.
//

import SwiftUI

struct MainView: View {
    var body: some View {
        TabView {
            ContentView()
            CurrenciesView()
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
