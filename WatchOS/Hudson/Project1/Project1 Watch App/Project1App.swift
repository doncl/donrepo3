//
//  Project1App.swift
//  Project1 Watch App
//
//  Created by Don Clore on 9/18/22.
//

import SwiftUI

@main
struct Project1_Watch_AppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
