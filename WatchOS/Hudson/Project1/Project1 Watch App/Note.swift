//
//  Note.swift
//  Project1 Watch App
//
//  Created by Don Clore on 9/18/22.
//

import SwiftUI

struct Note: Identifiable, Codable {
    let id: UUID
    let text: String
}
