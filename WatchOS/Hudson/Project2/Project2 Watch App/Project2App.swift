//
//  Project2App.swift
//  Project2 Watch App
//
//  Created by Don Clore on 9/18/22.
//

import SwiftUI

@main
struct Project2_Watch_AppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
