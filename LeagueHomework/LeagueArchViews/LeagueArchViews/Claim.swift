//
//  Claim.swift
//  LeagueArchViews
//
//  Created by Don Clore on 8/27/22.
//

import Foundation

class Claim: ObservableObject, Identifiable {
    var id = UUID()
    var name: String
    var date: String
    var imageName: String
    var notes: String

    init(name: String, date: String, imageName: String, notes: String) {
        self.name = name
        self.date = date
        self.imageName = imageName
        self.notes = notes
    }

    static let claims: [Claim] = [
        Claim(name: "Receipt", date: "Nov. 12, 2021", imageName: "Receipt", notes: "Receipt from Widget Memorial Hospital"),
        Claim(name: "Prescription", date: "Dec. 1, 2020", imageName: "prescription", notes: "Stuff you should never take"),
        Claim(name: "Doctor's Note", date: "April 8, 2021", imageName: "doctorsnote", notes: "Donnie is sick"),
    ]
}
