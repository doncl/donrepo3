//
//  CompressionProgressVC.swift
//  LeagueArchViews
//
//  Created by Don Clore on 8/28/22.
//

import UIKit

class CompressionProgressVC: UIViewController {
    enum Constants {
        static let colorVal: CGFloat = 1 / 255
        static let backgroundColor: UIColor = .init(red: colorVal,
                                                    green: colorVal,
                                                    blue: colorVal,
                                                    alpha: 0.9)

        static let greyRectWidth: CGFloat = 289
        static let vertPad: CGFloat = 24
        static let circularProgressDim: CGFloat = 50.0
        static let topToProgress: CGFloat = 24.0
        static let progressToTitle: CGFloat = 16.0
        static let titleToNotifyLabel: CGFloat = 16.0
        static let notifyGutters: CGFloat = 24.0
        static let notifyToClose: CGFloat = 16.0
        static let closeGutters: CGFloat = 16.0
        static let buttonHeight: CGFloat = 60.0
        static let closeToShare: CGFloat = 8.0
    }

    let greyRect: UIView = .init()
    let circularProgress: CircularProgressBar = .init()
    let titleLable: UILabel = .init()
    let notifyLabel: UILabel = .init()

    // MARK: - Overriden methods

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Constants.backgroundColor
        view.accessibilityIdentifier = "upload progress bite"

        imperativeLayout()
    }

    private func imperativeLayout() {
        circularProgress.safePercent = 100
        circularProgress.lineColor = .systemRed
        circularProgress.lineFinishColor = .systemGreen
        circularProgress.lineBackgroundColor = .gray

        [greyRect].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview($0)
        }

        greyRect.backgroundColor = UIColor(named: "blackGray")!

        NSLayoutConstraint.activate([
            greyRect.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
            greyRect.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor),
            greyRect.widthAnchor.constraint(equalToConstant: Constants.greyRectWidth),
        ])

        [circularProgress, titleLable].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            greyRect.addSubview($0)
        }

        NSLayoutConstraint.activate([
            circularProgress.widthAnchor.constraint(equalToConstant: Constants.circularProgressDim),
            circularProgress.heightAnchor.constraint(equalToConstant: Constants.circularProgressDim),
            circularProgress.centerXAnchor.constraint(equalTo: greyRect.centerXAnchor),
            circularProgress.topAnchor.constraint(equalTo: greyRect.topAnchor, constant: Constants.topToProgress),

            titleLable.topAnchor.constraint(equalTo: circularProgress.bottomAnchor, constant: Constants.progressToTitle),
            titleLable.centerXAnchor.constraint(equalTo: greyRect.centerXAnchor),
            titleLable.bottomAnchor.constraint(equalTo: greyRect.bottomAnchor, constant: -Constants.vertPad),
        ])

        titleLable.font = UIFont.systemFont(ofSize: 20)
        titleLable.textColor = UIColor.white
        titleLable.numberOfLines = 1
        titleLable.textAlignment = NSTextAlignment.center
        titleLable.text = "Uploading image...."

        let para = NSMutableParagraphStyle()
        para.alignment = NSTextAlignment.center
    }

    func updateProgress(_ progress: Double, with animation: Bool = false) {
        circularProgress.setProgress(to: progress, withAnimation: animation)
    }

    func updateTitle(_ string: String) {
        titleLable.text = string
    }
}
