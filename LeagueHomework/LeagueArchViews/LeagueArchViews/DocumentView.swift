//
//  DocumentView.swift
//  LeagueArchViews
//
//  Created by Don Clore on 8/27/22.
//

import SwiftUI

struct DocumentView: View {
    var doc: Doc

    var body: some View {
        VStack {
            Image(doc.imageName)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .padding()

            Form {
                HStack {
                    Text("Name:")
                        .font(.headline)
                        .fontWeight(.semibold)

                    Text(doc.name)
                }
                .padding(.bottom, 4)

                HStack {
                    Text("Document Type:")
                        .font(.headline)
                        .fontWeight(.semibold)

                    Text(doc.type)
                }
                .padding(.bottom, 4)

                HStack {
                    Text("Date:")
                        .font(.headline)
                        .fontWeight(.semibold)

                    Text(doc.date)
                }
                .padding(.bottom, 4)

                HStack {
                    Text("Additional Notes:")
                        .font(.headline)
                        .fontWeight(.semibold)

                    Text(doc.notes)
                }
                .padding(.bottom, 4)
            }
        }
        .toolbar {
            Button("Edit") {}
        }
    }
}

struct DocumentView_Previews: PreviewProvider {
    static var previews: some View {
        DocumentView(doc: Doc.docs[1])
    }
}
