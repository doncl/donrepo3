//
//  ViewController.swift
//  LeagueArchViews
//
//  Created by Don Clore on 8/27/22.
//

import SwiftUI
import UIKit

class ViewController: UIViewController {
    lazy var docsBtn: UIButton = {
        let btn = UIButton(type: .custom)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Doc", for: .normal)
        btn.setTitleColor(.black, for: .normal)

        return btn
    }()

    lazy var claimsBtn: UIButton = {
        let btn = UIButton(type: .custom)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Claims", for: .normal)
        btn.setTitleColor(.black, for: .normal)
        return btn
    }()

    lazy var progressBtn: UIButton = {
        let btn = UIButton(type: .custom)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Progress", for: .normal)
        btn.setTitleColor(.black, for: .normal)
        return btn
    }()

    lazy var stack: UIStackView = {
        let s = UIStackView(arrangedSubviews: [docsBtn, claimsBtn, progressBtn])
        s.translatesAutoresizingMaskIntoConstraints = false
        s.axis = .horizontal
        s.spacing = 20
        return s
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        [stack].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview($0)
        }

        NSLayoutConstraint.activate([
            stack.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
            stack.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 80),
        ])

        docsBtn.addTarget(self, action: #selector(ViewController.docBtnTapped(_:)), for: .touchUpInside)
        claimsBtn.addTarget(self, action: #selector(ViewController.claimsBtnTapped(_:)), for: .touchUpInside)
        progressBtn.addTarget(self, action: #selector(ViewController.progressBtnTapped(_:)), for: .touchUpInside)
    }

    @objc func docBtnTapped(_: UIButton) {
        let docVC: UIHostingController<DocumentListView> = UIHostingController(rootView: DocumentListView())
        docVC.modalPresentationStyle = .overFullScreen
        present(docVC, animated: true)
    }

    @objc func claimsBtnTapped(_: UIButton) {
        print("\(#function)")
        let claimsVC: UIHostingController<ClaimListView> = UIHostingController(rootView: ClaimListView())
        claimsVC.modalPresentationStyle = .overFullScreen
        present(claimsVC, animated: true)
    }

    @objc func progressBtnTapped(_: UIButton) {
        let vc = CompressionProgressVC()

        vc.view.alpha = 0
        addChild(vc)
        view.addSubview(vc.view)
        vc.didMove(toParent: self)
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        let v = vc.view!
        NSLayoutConstraint.activate([
            v.topAnchor.constraint(equalTo: view.topAnchor),
            v.leftAnchor.constraint(equalTo: view.leftAnchor),
            v.rightAnchor.constraint(equalTo: view.rightAnchor),
            v.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])

        UIView.transition(with: v, duration: 0.4, options: [.transitionCrossDissolve], animations: {
            v.alpha = 1
        }, completion: { [weak self] _ in
            guard let self = self else { return }
            vc.updateProgress(0.5)
        })
    }
}
