//
//  DocumentList.swift
//  LeagueArchViews
//
//  Created by Don Clore on 8/27/22.
//

import SwiftUI

struct DocumentListView: View {
    var body: some View {
        NavigationView {
            List(Doc.docs) { doc in
                VStack(alignment: .leading) {
                    NavigationLink(destination: DocumentView(doc: doc)) {
                        HStack {
                            Image("doc")
                                .resizable()
                                .aspectRatio(contentMode: ContentMode.fit)
                                .frame(width: 100, height: 100)

                            Text(doc.name)
                                .font(.title2)

                            Spacer()
                            Text(doc.date)
                                .font(.caption)
                        }
                    }
                }
            }
            .navigationTitle("Documents")
            .toolbar {
                ToolbarItem(placement: .primaryAction) {
                    Menu {
                        Button(action: {}) {
                            Label("Take a photo", systemImage: "camera")
                        }

                        Button(action: {}) {
                            Label("Image from library", systemImage: "photo")
                        }
                    }
          label: {
                        Label("Add", systemImage: "plus")
                    }
                }
            }
        }
    }
}

struct DocumentList_Previews: PreviewProvider {
    static var previews: some View {
        DocumentListView()
    }
}
