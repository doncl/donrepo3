//
//  ClaimListView.swift
//  LeagueArchViews
//
//  Created by Don Clore on 8/27/22.
//

import SwiftUI

struct ClaimListView: View {
    var body: some View {
        NavigationView {
            List(Claim.claims) { claim in
                VStack(alignment: .leading) {
                    NavigationLink(destination: ClaimView(claim: claim)) {
                        HStack {
                            Image("claim")
                                .resizable()
                                .aspectRatio(contentMode: ContentMode.fit)
                                .frame(width: 100, height: 100)

                            Text(claim.name)
                                .font(.title2)

                            Spacer()
                            Text(claim.date)
                                .font(.caption)
                        }
                    }
                }
            }
            .navigationTitle("Claims")
            .toolbar {
                ToolbarItem(placement: .primaryAction) {
                    Menu {
                        Button(action: {}) {
                            Label("Take a photo", systemImage: "camera")
                        }

                        Button(action: {}) {
                            Label("Image from library", systemImage: "photo")
                        }
                    }
          label: {
                        Label("Add", systemImage: "plus")
                    }
                }
            }
        }
    }
}

struct ClaimListView_Previews: PreviewProvider {
    static var previews: some View {
        ClaimListView()
    }
}
