//
//  CircularProgressBar.swift
//  LeagueArchViews
//
//  Created by Don Clore on 8/28/22.
//

import UIKit

class CircularProgressBar: UIView {
    // MARK: Public

    public var lineWidth: CGFloat = 5 {
        didSet {
            foregroundLayer.lineWidth = lineWidth
            backgroundLayer.lineWidth = lineWidth - (0.20 * lineWidth)
        }
    }

    public var safePercent: Int = 100 {
        didSet {
            setForegroundLayerColorForSafePercent()
        }
    }

    public var wholeCircleAnimationDuration: Double = 2
    public var lineBackgroundColor: UIColor = .gray
    public var lineColor: UIColor = .systemRed
    public var lineFinishColor: UIColor = .systemGreen

    public var shouldDisplayLabel: Bool = true

    // MARK: - Private

    private let foregroundLayer = CAShapeLayer()
    private let backgroundLayer = CAShapeLayer()
    private var radius: CGFloat {
        if frame.width < frame.height { return (frame.width - lineWidth) / 2 }
        else { return (frame.height - lineWidth) / 2 }
    }

    private var pathCenter: CGPoint { return convert(center, from: superview) }
    // Layout Sublayers
    private var layoutDone = false

    private var label = UILabel()

    private var currentProgress: Int = 0

    // MARK: awakeFromNib

    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
        label.text = "0%"
    }

    override func layoutSublayers(of _: CALayer) {
        if !layoutDone {
            let tempText = label.text
            setupView()
            label.text = tempText
            layoutDone = true
        }
    }

    private func setupView() {
        makeBar()
        if shouldDisplayLabel {
            addSubview(label)
        }
    }

    private func configLabel() {
        label.font = UIFont.systemFont(ofSize: 11)
        label.sizeToFit()
        label.center = pathCenter
        label.textColor = .white
    }

    public func setProgress(to progressConstant: Double, withAnimation: Bool) {
        var progress: Double {
            if progressConstant > 1 { return 1 }
            else if progressConstant < 0 { return 0 }
            else { return progressConstant }
        }

        let animationDuration = wholeCircleAnimationDuration * progress

        foregroundLayer.strokeEnd = CGFloat(progress)

        if withAnimation {
            let animation = CABasicAnimation(keyPath: "strokeEnd")
            animation.fromValue = 0
            animation.toValue = progress
            animation.duration = animationDuration
            foregroundLayer.add(animation, forKey: "foregroundAnimation")

            var currentTime: Double = 0
            let timer = Timer.scheduledTimer(withTimeInterval: 0.05, repeats: true) { [weak self] timer in
                guard let self = self else { return }
                if currentTime >= animationDuration {
                    timer.invalidate()
                } else {
                    currentTime += 0.05
                    let percent = currentTime / 2 * 100
                    self.currentProgress = Int(progress * percent)
                    self.label.text = "\(self.currentProgress)%"
                    self.setForegroundLayerColorForSafePercent()
                    self.configLabel()
                }
            }
            timer.fire()
        }

        currentProgress = Int(progress * 100)
        label.text = "\(currentProgress)%"
        setForegroundLayerColorForSafePercent()
        configLabel()
    }

    // MARK: Private

    private func makeBar() {
        layer.sublayers = nil
        drawBackgroundLayer()
        drawForegroundLayer()
    }

    private func drawBackgroundLayer() {
        let path = UIBezierPath(arcCenter: pathCenter, radius: radius, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        backgroundLayer.path = path.cgPath
        backgroundLayer.strokeColor = lineBackgroundColor.cgColor
        backgroundLayer.lineWidth = lineWidth - (lineWidth * 20 / 100)
        backgroundLayer.fillColor = UIColor.clear.cgColor
        layer.addSublayer(backgroundLayer)
    }

    private func drawForegroundLayer() {
        let startAngle = (-CGFloat.pi / 2)
        let endAngle = 2 * CGFloat.pi + startAngle

        let path = UIBezierPath(arcCenter: pathCenter, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)

        foregroundLayer.lineCap = CAShapeLayerLineCap.round // kCALineCapRound//
        foregroundLayer.path = path.cgPath
        foregroundLayer.lineWidth = lineWidth
        foregroundLayer.fillColor = UIColor.clear.cgColor
        foregroundLayer.strokeColor = lineColor.cgColor
        foregroundLayer.strokeEnd = 0

        layer.addSublayer(foregroundLayer)
    }

    private func setForegroundLayerColorForSafePercent() {
        if currentProgress >= safePercent {
            foregroundLayer.strokeColor = lineFinishColor.cgColor
        } else {
            foregroundLayer.strokeColor = lineColor.cgColor
        }
    }
}
