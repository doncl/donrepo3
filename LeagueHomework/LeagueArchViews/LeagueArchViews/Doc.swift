//
//  Doc.swift
//  LeagueArchViews
//
//  Created by Don Clore on 8/27/22.
//

import Foundation
import SwiftUI

class Doc: ObservableObject, Identifiable {
    var id = UUID()
    var name: String
    var type: String
    var date: String
    var imageName: String
    var notes: String

    init(name: String, type: String, date: String, imageName: String, notes: String) {
        self.name = name
        self.type = type
        self.date = date
        self.imageName = imageName
        self.notes = notes
    }

    static let docs: [Doc] = [
        Doc(name: "Don ID", type: "ID", date: "Nov. 12, 2021", imageName: "ID", notes: "This is the ID for Don"),
        Doc(name: "Steroidal Fitness", type: "Gym MemberShip", date: "Dec. 1, 2020", imageName: "gym", notes: "Gym membership"),
        Doc(name: "Wife gets all", type: "Beneficiary Form", date: "April 8, 2021", imageName: "beneficiary", notes: "Beneficiary information"),
    ]
}
