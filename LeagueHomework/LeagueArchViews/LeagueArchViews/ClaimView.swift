//
//  ClaimView.swift
//  LeagueArchViews
//
//  Created by Don Clore on 8/27/22.
//

import SwiftUI

struct ClaimView: View {
    var claim: Claim

    var body: some View {
        VStack {
            Image(claim.imageName)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .padding()

            Form {
                HStack {
                    Text("Name:")
                        .font(.headline)
                        .fontWeight(.semibold)

                    Text(claim.name)
                }
                .padding(.bottom, 4)

                HStack {
                    Text("Date:")
                        .font(.headline)
                        .fontWeight(.semibold)

                    Text(claim.date)
                }
                .padding(.bottom, 4)

                HStack {
                    Text("Notes:")
                        .font(.headline)
                        .fontWeight(.semibold)

                    Text(claim.notes)
                        .font(.callout)
                        .lineLimit(1)
                }
                .padding(.bottom, 4)
            }
        }
        .toolbar {
            Button("Edit") {}
        }
    }
}

struct ClaimView_Previews: PreviewProvider {
    static var previews: some View {
        ClaimView(claim: Claim.claims[0])
    }
}
