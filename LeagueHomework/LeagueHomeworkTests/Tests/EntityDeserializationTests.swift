//
//  EntityDeserializationTests.swift
//  LeagueHomeworkTests
//
//  Created by Don Clore on 8/20/22.
//

@testable import LeagueHomework
import XCTest

class EntityDeserializationTests: XCTestCase, ResourceTestable {
    func testDeserializeAPIKeyResponse() {
        let data = getResourceData(named: "ApiKeyResponse", withExtension: "json")

        do {
            let tokenResponse = try JSONDecoder().decode(APITokenResponse.self, from: data)
            XCTAssertEqual(tokenResponse.apiKey, "5ECDDC3A21CE53428227A2125B7FCC71")
        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    func testDeserializeUser() {
        let data = getResourceData(named: "UsersResponse", withExtension: "json")

        do {
            let rawUsers = try JSONDecoder().decode([RawUser].self, from: data)
            XCTAssertEqual(rawUsers.count, 1)
            guard let safeUser = SafeUser.fromRaw(rawUsers[0]) else {
                XCTFail("Unable to synthesize safe user from rawUser JSON response")
                return
            }
            EntityDeserializationTests.verifySafeUserProperties(safeUser)

        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    func testDeserializePost() {
        let data = getResourceData(named: "PostsResponse", withExtension: "json")

        do {
            let rawPosts = try JSONDecoder().decode([RawPost].self, from: data)
            XCTAssertEqual(rawPosts.count, 1)

            guard let safePost = SafePost.fromRaw(rawPosts[0]) else {
                XCTFail("Unable to synthesize safe post from rawPost JSON response")
                return
            }

            EntityDeserializationTests.verifySafePostProperties(safePost)

        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    static func verifySafePostProperties(_ safePost: SafePost) {
        XCTAssertEqual(safePost.userID, 3)
        XCTAssertEqual(safePost.id, 26)
        XCTAssertEqual(safePost.title, "est et quae odit qui non")
        XCTAssertEqual(safePost.body, "similique esse doloribus nihil ...")
    }

    static func verifySafeUserProperties(_ safeUser: SafeUser) {
        XCTAssertEqual(safeUser.id, 1)
        XCTAssertEqual(safeUser.avatar, URL(string: "https://i.pravatar.cc/150?u=1")!)
        XCTAssertEqual(safeUser.name, "Leanne Graham")
        XCTAssertEqual(safeUser.userName, "Bret")
        XCTAssertEqual(safeUser.email, "Sincere@april.biz")

        guard let address = safeUser.address else {
            XCTFail("User address should not be nil when deserialized from this test data resource")
            return
        }
        XCTAssertEqual(address.street, "Kulas Light")
        XCTAssertEqual(address.suite, "Apt. 556")
        XCTAssertEqual(address.city, "Gwenborough")
        XCTAssertEqual(address.zipCode, "92998-3874")

        guard let geo = address.geo else {
            XCTFail("Geo should not be nil when deserialized from this test data resource")
            return
        }

        XCTAssertEqual(geo.latitude, -37.3159)
        XCTAssertEqual(geo.longitude, 81.1496)

        XCTAssertEqual(safeUser.phone, "1-770-736-8031 x56442")
        guard let website = safeUser.website else {
            XCTFail("Website should not be nil when deserialized from this test data resource")
            return
        }

        XCTAssertEqual(website, URL(string: "hildegard.org")!)

        guard let company = safeUser.company else {
            XCTFail("Website should not be nil when deserialized from this test data resource")
            return
        }

        XCTAssertEqual(company.name, "Romaguera-Crona")
        XCTAssertEqual(company.catchPhrase, "Multi-layered...")
        XCTAssertEqual(company.bs, "harness real-time e-markets")
    }
}
