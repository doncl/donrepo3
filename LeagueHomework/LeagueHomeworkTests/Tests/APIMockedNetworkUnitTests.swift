//
//  APIMockedNetworkUnitTests.swift
//  LeagueHomeworkTests
//
//  Created by Don Clore on 8/20/22.
//

@testable import LeagueHomework
import XCTest

// These tests exercise, in addition to the deserialization code, the APIManager and network code,
// using the TestURLProtocol, so they don't actually hit the net, but just mock the network
// responses with resources loaded from the bundle.

class APIMockedNetworkUnitTests: XCTestCase {
    lazy var testConfig: URLSessionConfiguration = {
        let config = URLSessionConfiguration.default
        config.protocolClasses = [URLTestProtocol.self]
        return config
    }()

    let mockToken: String = "5ECDDC3A21CE53428227A2125B7FCC71"

    override func setUp() async throws {
        Network.shared.session = URLSession(configuration: testConfig)
        UserDefaults.standard.set(mockToken, forKey: APIManager.Constants.apiTokenKey)
    }

    override func tearDown() async throws {
        Network.shared.session = URLSession.shared
        UserDefaults.standard.removeObject(forKey: APIManager.Constants.apiTokenKey)
    }

    func testLogin() async {
        let tokenResult = await APIManager.shared.login()
        switch tokenResult {
        case let .success(token):
            XCTAssertEqual(token, mockToken)

        case let .failure(error):
            XCTFail(error.localizedDescription)
        }
    }

    func testGetUsers() async {
        let usersResult = await APIManager.shared.getAllUsers()
        switch usersResult {
        case let .success(users):
            XCTAssertEqual(users.count, 1)
            let user = users.first!

            // Might as well reuse this verification code.
            EntityDeserializationTests.verifySafeUserProperties(user)

        case let .failure(error):
            XCTFail(error.localizedDescription)
        }
    }

    func testGetPosts() async {
        let postsResult = await APIManager.shared.getAllPosts()
        switch postsResult {
        case let .success(posts):
            XCTAssertEqual(posts.count, 1)
            let post = posts.first!

            EntityDeserializationTests.verifySafePostProperties(post)

        case let .failure(error):
            XCTFail(error.localizedDescription)
        }
    }
}
