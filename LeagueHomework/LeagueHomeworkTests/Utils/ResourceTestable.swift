//
//  ResourceTestable.swift
//  LeagueHomeworkTests
//
//  Created by Don Clore on 8/20/22.
//

import XCTest

// Just to make it easier to get test data from bundled resources.
protocol ResourceTestable: AnyObject {
    func getResourceData(named name: String, withExtension extension: String) -> Data
}

extension ResourceTestable {
    func getResourceData(named name: String, withExtension ext: String) -> Data {
        let bundle = Bundle(for: type(of: self))
        guard let url = bundle.url(forResource: name, withExtension: ext) else {
            XCTFail("Missing resource")
            return Data()
        }

        guard let data = try? Data(contentsOf: url) else {
            XCTFail("Failed to convert resource \(name) to Data")
            return Data()
        }
        return data
    }
}
