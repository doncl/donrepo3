//
//  URLTestProtocol.swift
//  LeagueHomeworkTests
//
//  Created by Don Clore on 8/20/22.
//

import Foundation
@testable import LeagueHomework

class URLTestProtocol: URLProtocol, ResourceTestable {
    override class func canInit(with _: URLRequest) -> Bool {
        return true
    }

    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }

    override func startLoading() {
        guard let client = client,
              let url = request.url,
              let response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)
        else {
            fatalError("Improperty provisioned")
        }

        let injectedData: Data
        if url == APIManager.Constants.loginURL {
            injectedData = getResourceData(named: "ApiKeyResponse", withExtension: "json")
        } else if url == APIManager.Constants.usersURL {
            injectedData = getResourceData(named: "UsersResponse", withExtension: "json")
        } else if url == APIManager.Constants.postsURL {
            injectedData = getResourceData(named: "PostsResponse", withExtension: "json")
        } else {
            fatalError("Needs to beefed up for more test types")
        }

        client.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed)
        client.urlProtocol(self, didLoad: injectedData)
        client.urlProtocolDidFinishLoading(self)
    }

    override func stopLoading() {}
}
