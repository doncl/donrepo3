//
//  APIManager.swift
//  LeagueHomework
//
//  Created by Don Clore on 8/20/22.
//

import Combine
import Foundation
import UIKit

// N.B. In the real world, there would be multiple 'managers' (or 'services', or whatever you wish to call them)
// possibly, but not necessarily, corresponding to the multiple services on the backend (whether it's monolithic or
// 'microservice', it's gonna probably be divided up into logical groups of services.)
// This homework API documentation has 5 endpoints.  It's overkill to create more than one manager/service entity
// here.

struct APIManager {
    enum Constants {
        static let apiTokenKey: String = "api-token-key"
        private static let baseURL: URL = .init(string: "https://engineering.league.dev/challenge/api")!
        static let loginURL: URL = baseURL.appendingPathComponent("login")
        static let usersURL: URL = baseURL.appendingPathComponent("users")
        static let postsURL: URL = baseURL.appendingPathComponent("posts")
    }

    static let shared: APIManager = .init()

    let errorPublisher: PassthroughSubject<String, Never> = .init()
    let successPublisher: PassthroughSubject<[PostWithUser], Never> = .init()
    let showSpinnerPublisher: CurrentValueSubject<Bool, Never> = .init(false)

    private init() {}

    func login() async -> Result<String, Error> {
        // TBH, I'm not really sure if there's a canonical 'No Auth' Authorization value that the assignment is checking to see if I know.
        // If so, I fail :).  Cause I don't know.  So I'm just copying what is in the assignment specification.
        let headers: [String: String] = [
            "Authorization": "No Auth",
        ]

        showSpinnerPublisher.send(true)

        let result: Result<APITokenResponse, Error> = await Network.shared.getSomething(url: Constants.loginURL, headers: headers)

        showSpinnerPublisher.send(false)

        switch result {
        case let .success(tokenResponse):
            // N.B. in the real world, we'd probably save this to the keychain.  And maybe it would be a JWT, and we'd check it's expiry on each
            // request, and renew it as needed, etc.
            UserDefaults.standard.set(tokenResponse.apiKey, forKey: APIManager.Constants.apiTokenKey)

            return .success(tokenResponse.apiKey)

        case let .failure(error):
            // Very likely there would be APIManager-specific logging here.
            print(error)
            return .failure(error)
        }
    }

    private func getAllUsers() async -> Result<[SafeUser], Error> {
        return await SafeUser.loadAll()
    }

    private func getAllPosts() async -> Result<[SafePost], Error> {
        return await SafePost.loadAll()
    }

    func kickOffGets() {
        showSpinnerPublisher.send(true)

        Task {
            await _ = login()
            assert(UserDefaults.standard.string(forKey: APIManager.Constants.apiTokenKey) != nil)

            async let postsResult = getAllPosts()
            async let usersResult = getAllUsers()

            let results: [Any] = await [postsResult, usersResult]
            guard let pResult = results[0] as? Result<[SafePost], Error>,
                  let uResult = results[1] as? Result<[SafeUser], Error>
            else {
                fatalError("Something every wrong with this Swift concurrent code")
            }

            // Probably a good time to do this - the rest of the processing is very
            // quick, in human-time.
            showSpinnerPublisher.send(false)
            switch pResult {
            case let .success(safePosts):

                switch uResult {
                case let .success(safeUsers):
                    // Processing Post AND users
                    await processPosts(safePosts: safePosts, andUsers: safeUsers)

                case let .failure(error):
                    // Ah, well, only got posts, process those alone.
                    print("\(#function) - \(error) - going to just send back posts")
                    await processPosts(safePosts: safePosts, andUsers: [])
                }

            case let .failure(error):
                print("\(#function) - \(error)")
                // We cannot do anything if we don't have posts.
                errorPublisher.send("Could not get Posts, cannot continue")
                return
            }
        }
    }

    @MainActor
    private func processPosts(safePosts: [SafePost], andUsers safeUsers: [SafeUser]) {
        assert(Thread.isMainThread)

        var postsWithUsers: [PostWithUser] = []
        var userDict: [Int: SafeUser] = [:]
        for user in safeUsers {
            userDict[user.id] = user
        }

        for post in safePosts {
            guard let user = userDict[post.userID] else {
                continue
            }

            let postWithUser = PostWithUser(post: post, user: user)
            postsWithUsers.append(postWithUser)
        }

        successPublisher.send(postsWithUsers)
    }
}
