//
//  MasterVC.swift
//  LeagueHomework
//
//  Created by Don Clore on 8/20/22.
//

import Combine
import UIKit

protocol MasterVCDelegate: AnyObject {
    var isRegular: Bool { get }
    func itemSelected(user: SafeUser)
}

class MasterVC: UIViewController {
    enum Constants {
        static let estimatedRowHeight: CGFloat = 300
        static let spinnerDim: CGFloat = 80
    }

    lazy var spinner: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: Constants.spinnerDim, height: Constants.spinnerDim))
        spinner.isHidden = true
        spinner.style = .large
        return spinner
    }()

    weak var delegate: MasterVCDelegate?

    private var subscriptions: Set<AnyCancellable> = .init()

    private var postsWithUsers: [PostWithUser] = []

    private var table: UITableView = .init()

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.white
        navigationItem.title = "Users"

        [table].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview($0)
        }

        NSLayoutConstraint.activate([
            table.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            table.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
            table.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
            table.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
        ])

        table.register(PostCell.self, forCellReuseIdentifier: PostCell.id)
        table.dataSource = self
        table.delegate = self
        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = Constants.estimatedRowHeight

        APIManager.shared.successPublisher
            .sink { [weak self] postsWithUsers in
                assert(Thread.isMainThread)
                guard let self = self else { return }

                self.postsWithUsers = postsWithUsers
                self.table.reloadData()
            }
            .store(in: &subscriptions)

        APIManager.shared.errorPublisher
            .sink { [weak self] errMsg in
                assert(Thread.isMainThread)
                guard let self = self else { return }
                self.errorDlg(msg: errMsg)
            }
            .store(in: &subscriptions)

        APIManager.shared.showSpinnerPublisher
            .sink { show in
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }

                    switch show {
                    case true:
                        self.setSpinner(on: self.view)
                    case false:
                        self.stopSpinner()
                    }
                }
            }
            .store(in: &subscriptions)

        APIManager.shared.kickOffGets()
    }

    @MainActor
    private func errorDlg(msg: String) {
        assert(Thread.isMainThread)
        let ac = UIAlertController(title: "Error", message: msg, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default)
        ac.addAction(ok)
        present(ac, animated: true)
    }
}

// MARK: UITableViewDataSource

// Just fyi - I do know about UITableViewDiffableDataSource and UITableViewPrefetchDataSource.  It was overkill for this.
// I also am aware of using CompositionalCollectionViews as tables, I have found it more trouble than it's worth, but
// glad to do that way, if needed.
extension MasterVC: UITableViewDataSource {
    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return postsWithUsers.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PostCell.id, for: indexPath) as? PostCell else {
            fatalError("Improper provisioning")
        }
        assert(postsWithUsers.count > indexPath.item)
        let postWithUser = postsWithUsers[indexPath.item]
        cell.postWithUser = postWithUser

        return cell
    }
}

// MARK: UITableViewDelegate

extension MasterVC: UITableViewDelegate {
    func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard postsWithUsers.count > indexPath.item else {
            return
        }
        let post = postsWithUsers[indexPath.item]
        guard let user = post.user else {
            return
        }

        guard let delegate = delegate else {
            return
        }
        delegate.itemSelected(user: user)
    }
}

// MARK: HasSpinner

extension MasterVC: HasSpinner {
    var desiredLocation: CGPoint {
        let center = CGPoint(x: view.frame.midX, y: view.frame.midY)
        return center
    }
}
