//
//  DetailsVC.swift
//  LeagueHomework
//
//  Created by Don Clore on 8/20/22.
//

import SwiftUI
import UIKit

class DetailsVC: UIViewController {
    weak var host: UIHostingController<UserView>?

    private func removeExistingHostAsNeeded() {
        guard let host = host else {
            return
        }

        host.willMove(toParent: nil)
        host.view.removeFromSuperview()
        host.didMove(toParent: nil)
        self.host = nil
    }

    var user: SafeUser? {
        didSet {
            guard let user = user else { return }
            navigationItem.title = user.name

            removeExistingHostAsNeeded()

            let freshHost: UIHostingController<UserView> = UIHostingController(rootView: UserView(user: user))
            self.host = freshHost
            addChild(freshHost)
            freshHost.view.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(freshHost.view)
            freshHost.didMove(toParent: self)

            let v: UIView = freshHost.view
            NSLayoutConstraint.activate([
                v.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                v.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
                v.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
                v.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            ])
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let nav = navigationController else {
            return
        }
        nav.navigationBar.tintColor = UIColor.black
    }
}
