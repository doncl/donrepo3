//
//  ViewController.swift
//  LeagueHomework
//
//  Created by Don Clore on 8/20/22.
//

import UIKit

class RootVC: UISplitViewController {
    let masterNav: UINavigationController = .init()
    let detailsNav: UINavigationController = .init()
    let detailsVC: DetailsVC = .init()

    override func viewDidLoad() {
        super.viewDidLoad()

        viewControllers = [masterNav, detailsNav]
        #if targetEnvironment(macCatalyst)
            preferredDisplayMode = UISplitViewController.DisplayMode.allVisible
        #else
            preferredDisplayMode = UISplitViewController.DisplayMode.oneBesideSecondary
        #endif
        delegate = self

        let selector = MasterVC()
        selector.delegate = self

        masterNav.show(selector, sender: self)
        // masterNav.setNavigationBarHidden(true, animated: false)
        detailsNav.viewControllers = [detailsVC]
    }
}

// MARK: UISplitViewControllerDelegate

extension RootVC: UISplitViewControllerDelegate {
    func splitViewController(_: UISplitViewController,
                             collapseSecondary secondaryViewController: UIViewController,
                             onto primaryViewController: UIViewController) -> Bool
    {
        guard let _ = secondaryViewController as? DetailsVC else {
            // Fallback to the default
            return true
        }

        if let primaryAsNavController = primaryViewController as? UINavigationController {
            primaryAsNavController.setNavigationBarHidden(false, animated: false)
        }

        return false
    }

    func primaryViewController(forExpanding _: UISplitViewController) -> UIViewController? {
        masterNav.setNavigationBarHidden(true, animated: true)
        return masterNav
    }
}

extension RootVC: MasterVCDelegate {
    func itemSelected(user: SafeUser) {
        detailsVC.user = user
        showDetailViewController(detailsVC, sender: nil)
    }

    var isRegular: Bool {
        return traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular
    }
}
