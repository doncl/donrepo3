//
//  PostCell.swift
//  LeagueHomework
//
//  Created by Don Clore on 8/20/22.
//

import Kingfisher
import UIKit

class PostCell: UITableViewCell {
    enum Constants {
        // Dynamic Type / Semantic fonts?  I elected not to think about it for this project; hope that's OK.
        static let userNameFont = UIFont.systemFont(ofSize: 22, weight: UIFont.Weight.semibold)
        static let titleFont = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.semibold)
        static let bodyFont = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)

        static let gutters: CGFloat = 16.0
        static let stackSpacing: CGFloat = 16.0
        static let textColor: UIColor = .black
        static let avatarDim: CGFloat = 42
        static let topPad: CGFloat = 8
        static let interItemPad: CGFloat = 8
        static let bottomPad: CGFloat = 8
        static let stackHeight: CGFloat = 48.0
    }

    static let id: String = .init(describing: PostCell.self)

    lazy var avatar: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.image = UIImage(named: "league")!
        iv.tintColor = UIColor.white
        iv.contentMode = UIView.ContentMode.scaleAspectFill
        iv.layer.cornerRadius = Constants.avatarDim / 2
        iv.clipsToBounds = true
        return iv
    }()

    lazy var userName: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = Constants.userNameFont
        l.textColor = Constants.textColor
        l.textAlignment = NSTextAlignment.left
        return l
    }()

    lazy var userStack: UIStackView = {
        let s = UIStackView(arrangedSubviews: [avatar, userName])
        s.axis = NSLayoutConstraint.Axis.horizontal
        s.distribution = UIStackView.Distribution.fillProportionally
        s.alignment = UIStackView.Alignment.center
        s.spacing = Constants.stackSpacing

        return s
    }()

    lazy var title: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = Constants.titleFont
        l.textColor = Constants.textColor
        l.textAlignment = NSTextAlignment.left
        l.numberOfLines = 1

        // LineBreakMode...hmmmm
        // this is what the pdf looks like, I guess.  I just wanted to know I was paying attention.
        // if this were real, I might (gently) advocate for either allowing multiple lines (preferred) or allowing
        // the label to shrink the font as needed to fit it.
        // This would probably be a case for self-sizing cells, so...it's not like vertical space is at such a premium, I reckon.
        l.lineBreakMode = NSLineBreakMode.byTruncatingTail

        return l
    }()

    lazy var body: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = Constants.bodyFont
        l.textColor = Constants.textColor
        l.textAlignment = NSTextAlignment.left
        l.numberOfLines = 0
        l.lineBreakMode = NSLineBreakMode.byWordWrapping // most usually preferred, I think.
        return l
    }()

    var postWithUser: PostWithUser? {
        didSet {
            guard let postWithUser = postWithUser else {
                return
            }

            let post = postWithUser.post
            title.text = post.title
            body.text = post.body

            if let user = postWithUser.user {
                userName.text = user.name // I guess we don't show the username anywhere?

                // Let's get the image - the following is Kingfisher-specific stuff.  Lotsa libraries, not too hard to do youself,
                // but this app uses Kingfisher.
                avatar.kf.indicatorType = .activity
                let processor = DownsamplingImageProcessor(size: CGSize(width: Constants.avatarDim, height: Constants.avatarDim))
                    |> RoundCornerImageProcessor(cornerRadius: Constants.avatarDim / 2)

                avatar.kf.setImage(with: user.avatar, placeholder: UIImage(named: "league")!, options: [
                    .processor(processor),
                    .scaleFactor(UIScreen.main.scale),
                    .transition(ImageTransition.fade(0.4)),
                    .cacheOriginalImage,
                ])

                userStack.sizeToFit()
            }
        }
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        [userStack, title, body].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview($0)
        }

        NSLayoutConstraint.activate([
            userStack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Constants.gutters),
            userStack.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Constants.topPad),
            userStack.heightAnchor.constraint(equalToConstant: Constants.stackHeight),

            title.topAnchor.constraint(equalTo: userStack.bottomAnchor, constant: Constants.interItemPad),
            title.leadingAnchor.constraint(equalTo: userStack.leadingAnchor),
            title.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Constants.gutters),

            body.topAnchor.constraint(equalTo: title.bottomAnchor, constant: Constants.interItemPad),
            body.leadingAnchor.constraint(equalTo: title.leadingAnchor),
            body.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Constants.gutters),

            contentView.bottomAnchor.constraint(equalTo: body.bottomAnchor, constant: Constants.gutters),
        ])
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
