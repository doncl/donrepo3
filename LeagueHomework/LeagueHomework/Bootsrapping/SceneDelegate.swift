//
//  SceneDelegate.swift
//  LeagueHomework
//
//  Created by Don Clore on 8/20/22.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo _: UISceneSession, options _: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else {
            return
        }

        // This app does not use Interface Builder (I'm not religiously opposed to IB, and can work with it, maintain apps that use it, and
        // create new storyboards if needed, but if given the choice, I usually chose to do my UIKit layouts in imperative code;
        // SwiftUI is its own animal.
        //
        // Hence, there's no Main.storyboard in this app.  Everything boots up here.
        let win = UIWindow(windowScene: windowScene)
        window = win

        let vc = RootVC()
        win.rootViewController = vc
        win.makeKeyAndVisible()
    }
}
