//
//  AppDelegate.swift
//  LeagueHomework
//
//  Created by Don Clore on 8/20/22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    // Keeping all this fairly minimal for this simple homework assignment.
    func application(_: UIApplication, didFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options _: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
}
