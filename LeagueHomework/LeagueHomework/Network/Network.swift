//
//  Network.swift
//  LeagueHomework
//
//  Created by Don Clore on 8/20/22.
//

import Foundation

enum NetworkError: Error {
    case nilResult
    case unexpectedError(String)
    case httpError(Int, String)
    case decodingError(Error)
}

struct Network {
    static var shared: Network = .init()

    var session = URLSession.shared

    private init() {}

    func getSomething<T: Codable>(url: URL, headers: [String: String]) async -> Result<T, Error> {
        var req = URLRequest(url: url)

        for header in headers {
            req.addValue(header.value, forHTTPHeaderField: header.key)
        }

        do {
            let tuple: (data: Data, response: URLResponse) = try await session.data(fromURLRequest: req)
            guard let httpResp = tuple.response as? HTTPURLResponse else {
                return .failure(NetworkError.unexpectedError("Response is not an HTTPURLResponse, what gives?"))
            }

            guard 200 ..< 300 ~= httpResp.statusCode else {
                return .failure(NetworkError.httpError(httpResp.statusCode, "HTTPS error"))
            }

            let decoder = JSONDecoder()

            do {
                let entity = try decoder.decode(T.self, from: tuple.data)
                return .success(entity)
            } catch {
                return .failure(NetworkError.decodingError(error))
            }
        } catch {
            return .failure(error)
        }
    }
}

// Make session.date(fromURLRequest) use async await.
// Do I really care about this syntatic sugar?
// Nope.   I have no problems with callbacks; I'm agnostic.
// But some do, and so I'm showing I know how to make this happen.
// As far as I can tell, there is not an async/await version of URLSession methods that lets you add headers.
// I may be mistaken, but there is probably value in showing that I know how to do this anyway, I reckon.
extension URLSession {
    func data(fromURLRequest request: URLRequest) async throws -> (Data, URLResponse) {
        try await withCheckedThrowingContinuation { continuation in
            let task = self.dataTask(with: request) { data, response, error in
                guard let data = data, let response = response else {
                    let error = error ?? URLError(.badServerResponse)
                    return continuation.resume(throwing: error)
                }
                continuation.resume(returning: (data, response))
            }

            task.resume()
        }
    }
}
