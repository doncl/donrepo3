//
//  APITokenResponse.swift
//  LeagueHomework
//
//  Created by Don Clore on 8/20/22.
//

import Foundation

struct APITokenResponse: Codable {
    enum CodingKeys: String, CodingKey {
        case apiKey = "api_key"
    }

    let apiKey: String
}
