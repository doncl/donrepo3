//
//  SafePost.swift
//  LeagueHomework
//
//  Created by Don Clore on 8/20/22.
//

import Foundation

struct SafePost: SafeEntity {
    static var allURL: URL {
        return APIManager.Constants.postsURL
    }

    let userID: Int
    let id: Int
    let title: String
    let body: String

    // Currently, there are no optional fields, but I'm repeating the pattern from the other entities to return an optional
    // here, not so much for consistency, but because...while (in proper agile fashion), I code only for what I know today...mostly...
    // it's hard to ignore the fact that almost certainly this post entity will get optional properties added to it in a real world
    // scenario.   However, if anyone cares, I could go either way here (it could be defined as returning a non-optional SafePost.
    static func fromRaw(_ rawPost: RawPost) -> SafePost? {
        return SafePost(userID: rawPost.userID, id: rawPost.id, title: rawPost.title, body: rawPost.body)
    }
}
