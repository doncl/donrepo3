//
//  PostWithUser.swift
//  LeagueHomework
//
//  Created by Don Clore on 8/20/22.
//

import Foundation

struct PostWithUser {
    let post: SafePost
    let user: SafeUser?
}
