//
//  SafeUser.swift
//  LeagueHomework
//
//  Created by Don Clore on 8/20/22.
//

import Foundation

// What fields are required, to safely use a user entity? This is very much a product decision, and needs to
// be collaboratively worked out between client engineers, server engineers, and product management.  The
// Design specification (Figma, InVision, XD, whatever), will drive a lot of this.  If it shows some
// property of the user on a screen, then it's got to be required.

// The idea here is that we get the safety and certainty benefits of Optionals (for me, the best feature of Swift),
// without propagating all the manual unwrapping all the way through the codebase....we decide, right in code,
// at the point of converting decoded RawEntities (which are very forgiving, and everything is optional, given
// that there's no physical mechanism to guarantee the server will ensure integrity of these entities), to
// in-memory entities whose required fields are non-optional - all raw entities that don't meet this requirement
// will be thrown out at the Service/Manager level (with a compactMap()).

// I'm not suggesting that this is the only, or the 'right', or even the 'best' way to do all this.
// It is simply one way, and is the approach I'm taking for this homework assignment.
// I am religious about no software engineering principles; the only absolute is that there are no
// absolutes.

struct SafeUser: SafeEntity {
    static var allURL: URL {
        return APIManager.Constants.usersURL
    }

    typealias rawType = RawUser

    // MARK: Required Properties

    let id: Int
    let avatar: URL
    let name: String
    let userName: String
    let email: String

    // MARK: Optional properties.

    let address: Address?
    let phone: String?
    let website: URL?
    let company: Company?

    static func fromRaw(_ rawUser: RawUser) -> SafeUser? {
        // Initial base requirements
        guard let id = rawUser.id,
              let avatarString = rawUser.avatar,
              let avatar = URL(string: avatarString),
              let name = rawUser.name,
              let userName = rawUser.userName,
              let email = rawUser.email
        else {
            return nil // game over
        }

        // Address
        let address = Address.fromRaw(rawUser.address)

        // Website URL
        var website: URL?
        if let websiteString = rawUser.website, let url = URL(string: websiteString) {
            website = url
        }

        // Company
        let company = Company.fromRaw(rawUser.company)

        return SafeUser(
            id: id,
            avatar: avatar,
            name: name,
            userName: userName,
            email: email,
            address: address,
            phone: rawUser.phone,
            website: website,
            company: company
        )
    }
}

extension SafeUser {
    // I've decided, for the purposes of this homework assignment, that for Address, street, city, and zip are required.
    struct Address {
        let street: String
        let suite: String?
        let city: String
        let zipCode: String
        let geo: Geo?

        static func fromRaw(_ rawAddress: RawUser.Address?) -> Address? {
            guard let rawAddress = rawAddress,
                  let street = rawAddress.street,
                  let city = rawAddress.city,
                  let zipCode = rawAddress.zipCode
            else {
                return nil
            }

            let geo = Geo.fromRaw(rawAddress.geo)

            return Address(street: street, suite: rawAddress.suite, city: city, zipCode: zipCode, geo: geo)
        }
    }

    // For 'Company', I'm making the executive decision that *only* name is required.
    struct Company {
        let name: String
        let catchPhrase: String?
        let bs: String?

        static func fromRaw(_ rawCompany: RawUser.Company?) -> Company? {
            guard let rawCompany = rawCompany,
                  let name = rawCompany.name
            else {
                return nil
            }

            return Company(name: name, catchPhrase: rawCompany.catchPhrase, bs: rawCompany.bs)
        }
    }

    // A Geo is pretty useless without both latitude and longitude properties.
    struct Geo {
        let latitude: Double
        let longitude: Double

        static func fromRaw(_ rawGeo: RawUser.Address.Geo?) -> Geo? {
            guard let rawGeo = rawGeo,
                  let stringLat = rawGeo.latitude,
                  let lat = Double(stringLat),
                  let stringLong = rawGeo.longitude,
                  let long = Double(stringLong)
            else {
                return nil
            }

            return Geo(latitude: lat, longitude: long)
        }
    }
}

// MARK: Get All

extension SafeUser {
    typealias rawEntity = RawUser

    //  func getAll(headers: [String: String]) async -> Result<[SafeUser]
}
