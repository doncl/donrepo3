//
//  SafeEntity.swift
//  LeagueHomework
//
//  Created by Don Clore on 8/20/22.
//

import Foundation

enum SafeEntityError: Error {
    case notoken
}

protocol SafeEntity {
    static var allURL: URL { get }
    associatedtype rawType: Codable
    static func fromRaw(_ t: rawType) -> Self?
    static func loadAll() async -> Result<[Self], Error>
}

// I'm a little ambivalent about this....this is POP, which wholeheartedly approve of, but the degree of DRYness,
// for a homework assignment, kind of renders the code inscrutable.   In the real world, would I do this for two entities
// (SafeUser and SafePost)?   Probably not.   I think I'd need more entities that behaved exactly the same way to
// justify this kind of trickiness (I abhor trickness and cleverness; would like my code to be readable to a
// twelve year old who is just learning coding).
extension SafeEntity {
    static func loadAll() async -> Result<[Self], Error> {
        let headersResult = getHeadersWithTokenAdded()
        switch headersResult {
        case let .success(headers):
            let result: Result<[rawType], Error> = await Network.shared.getSomething(url: allURL, headers: headers)

            switch result {
            case let .success(arrayOfRawEntities):
                let arrayOfSafeEntities = arrayOfRawEntities.compactMap { Self.fromRaw($0) }
                return .success(arrayOfSafeEntities)

            case let .failure(error):
                return .failure(error)
            }

        case let .failure(error):
            return .failure(error)
        }
    }

    private static func getHeadersWithTokenAdded() -> Result<[String: String], Error> {
        guard let token = UserDefaults.standard.string(forKey: APIManager.Constants.apiTokenKey) else {
            return .failure(SafeEntityError.notoken)
        }

        let headers: [String: String] = [
            "x-access-token": token,
        ]

        return .success(headers)
    }
}
