//
//  RawUser.swift
//  LeagueHomework
//
//  Created by Don Clore on 8/20/22.
//

import Foundation

// N.B. It is probably not necessary to include all these properties in order to accomplish the homework,
// and it's always important to understand what you need to decode and what you don't need to decode,
// since every property you add is a potential failure point, given the very limited guarantees of RESTful services.
// I'm not critical of RESTful services; but it is very loosely-coupled.
struct RawUser: Codable {
    enum CodingKeys: String, CodingKey {
        case id
        case avatar
        case name
        case userName = "username"
        case email
        case address
        case phone
        case website
        case company
    }

    let id: Int?
    let avatar: String?
    let name: String?
    let userName: String?
    let email: String?
    let address: Address?
    let phone: String?
    let website: String?
    let company: Company?
}

extension RawUser {
    struct Address: Codable {
        enum CodingKeys: String, CodingKey {
            case street
            case suite
            case city
            case zipCode = "zipcode"
            case geo
        }

        let street: String?
        let suite: String?
        let city: String?
        let zipCode: String?
        let geo: Geo?
    }

    struct Company: Codable {
        let name: String?
        let catchPhrase: String?
        let bs: String?
    }
}

extension RawUser.Address {
    struct Geo: Codable {
        enum CodingKeys: String, CodingKey {
            case latitude = "lat"
            case longitude = "lng"
        }

        let latitude: String?
        let longitude: String?
    }
}
