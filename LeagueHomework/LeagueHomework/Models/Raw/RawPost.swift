//
//  File.swift
//  LeagueHomework
//
//  Created by Don Clore on 8/20/22.
//

import Foundation

// It's hard to see how any of this is optional, to be useful, so I'm making all fields required.
struct RawPost: Codable {
    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case id
        case title
        case body
    }

    let userID: Int
    let id: Int
    let title: String
    let body: String
}
