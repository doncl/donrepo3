

//
//  PostDateLikeCountView.swift
//  ViddlUIKit
//
//  Created by Don Clore on 8/8/22.
//
//
import SwiftUI

struct PostDateLikeCountView: View {
    enum Constants {
        static let dateFont: Font = .init(Fonts.get(type: .sfProRegular, size: 12))
        static let darkGrayColor: Color = .init(red: 142 / 255, green: 142 / 255, blue: 147 / 255)
        static let eyeToCountSpacing: CGFloat = 3
        static let horzSpacing: CGFloat = 16
    }

    var postDate: Date
    var likeCount: Int

    private var formattedDate: String {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        df.dateFormat = "MMM dd,yyyy"
        let string: String = df.string(from: postDate)
        return string
    }

    var body: some View {
        HStack(spacing: Constants.horzSpacing) {
            Text(formattedDate)
                .font(Constants.dateFont)
                .foregroundColor(Constants.darkGrayColor)

            HStack(spacing: Constants.eyeToCountSpacing) {
                Image(systemName: "eye")
                Text(String(likeCount))
                    .font(Constants.dateFont)
                    .foregroundColor(Constants.darkGrayColor)
            }
        }
    }
}

struct PostDateLikeCountView_Previews: PreviewProvider {
    static var previews: some View {
        PostDateLikeCountView(postDate: Date(), likeCount: 36)
    }
}
