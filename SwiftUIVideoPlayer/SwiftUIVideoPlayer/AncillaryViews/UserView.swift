//
//  UserView.swift
//  ViddlUIKit
//
//  Created by Don Clore on 8/8/22.
//

import Kingfisher
import SwiftUI

struct UserView: View {
    enum Constants {
        static let imageDim: CGFloat = 30
        static let nameFont: Font = .init(Fonts.get(type: .sfProBold, size: 14))
        static let jobTitleFont: Font = .init(Fonts.get(type: .sfProRegular, size: 12))
        static let darkGrayColor: Color = .init(red: 142 / 255, green: 142 / 255, blue: 147 / 255)
    }

    var userThumbnailURL: String?
    var name: String?
    var jobTitle: String?

    var body: some View {
        HStack {
            if let userThumbnailURLString = userThumbnailURL, let url = URL(string: userThumbnailURLString) {
                KFImage(url)
                    .resizable()
                    .frame(width: Constants.imageDim, height: Constants.imageDim)
                    .cornerRadius(Constants.imageDim / 2)
            }

            VStack {
                if let name = name {
                    Text(name)
                        .font(Constants.nameFont)
                        .foregroundColor(Color.white)
                }

                if let jobTitle = jobTitle {
                    Text(jobTitle)
                        .font(Constants.jobTitleFont)
                        .foregroundColor(Constants.darkGrayColor)
                }
            }
        }
    }
}

struct UserView_Previews: PreviewProvider {
    static let url: String = "https://viddlstagmediaservicestr.blob.core.windows.net/userphotos/2Thumbnail.png"
    static var previews: some View {
        UserView(userThumbnailURL: url, name: "Mandar Kulkarni", jobTitle: "Dev Manager and general stud")
    }
}
