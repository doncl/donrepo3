//
//  TagLozengeView.swift
//  ViddlUIKit
//
//  Created by Don Clore on 8/8/22.
//

import SwiftUI

struct TagLozengeView: View {
    enum Constants {
        static let darkGray: Color = .init(red: 28 / 255, green: 28 / 255, blue: 30 / 255)
        static let tagFont: Font = .init(Fonts.get(type: .sfProRegular, size: 14))
        static let frameHeight: CGFloat = 44
    }

    var tags: [String]
    var body: some View {
        GeometryReader { _ in
            ScrollView(.horizontal, showsIndicators: false) {
                HStack {
                    ForEach(tags, id: \.self) { tag in
                        Text("#\(tag)")
                            .foregroundColor(Color.white)
                            .padding()
                            .background(Constants.darkGray)
                            .cornerRadius(Constants.frameHeight / 2)
                    }
                }
            }
            .frame(height: Constants.frameHeight)
        }
    }
}

struct TagLozengeView_Previews: PreviewProvider {
    static var previews: some View {
        TagLozengeView(tags: ["winloss", "sales", "saleswin", "tag3", "science", "apple", "orange"])
    }
}
