//
//  SwiftUIVideoPlayerApp.swift
//  SwiftUIVideoPlayer
//
//  Created by Don Clore on 8/9/22.
//

import AVFoundation
import Foundation
import SwiftUI

@main
struct SwiftUIVideoPlayerApp: App {
    //  let asset = AVURLAsset(url: URL(string: "https://viddlstagingmediaservice-usw22.streaming.media.azure.net/e877a642-a083-4b4f-82e3-165e90062534/4xvjzlj8rg.ism/manifest(format=m3u8-aapl,audio-only=false)")!)

    let asset = AVURLAsset(url: URL(string:
        "https://viddlstagingmediaservice-usw22.streaming.media.azure.net/38184adc-c3c8-4d6c-b442-80c5fe4bddcf/vo72pzjbnb.ism/manifest(format=m3u8-aapl,audio-only=false)")!)

    var body: some Scene {
        WindowGroup {
            // ContentView(asset: AVURLAsset(url: Bundle.main.url(forResource: "SampleVideo", withExtension: "mp4")!))
            ContentView(asset: asset)
        }
    }
}
