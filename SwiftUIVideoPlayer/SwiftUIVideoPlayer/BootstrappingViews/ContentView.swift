//
//  ContentView.swift
//  SwiftUIVideoPlayer
//
//  Created by Don Clore on 8/9/22.
//

import AVKit
import SwiftUI
import UIKit

struct ContentView: View {
    enum Constants {
        static let titleFont: Font = .init(Fonts.get(type: .sfProBold, size: 22))
        static let descFont: Font = .init(Fonts.get(type: .sfProRegular, size: 14))
        static let dateFont: Font = .init(Fonts.get(type: .sfProRegular, size: 12))
        static let darkGrayColor: Color = .init(red: 142 / 255, green: 142 / 255, blue: 147 / 255)
        static let vstackSpacing: CGFloat = 12
        static let designerBlack: Color = .init(red: 0.004 / 255, green: 0.004 / 255, blue: 0.004 / 255)
    }

    @Environment(\.verticalSizeClass) var verticalSizeClass: UserInterfaceSizeClass?
    @Environment(\.horizontalSizeClass) var horizontalSizeClass: UserInterfaceSizeClass?

    let player: AVPlayer = .init(url: Bundle.main.url(forResource: "SampleVideo", withExtension: "mp4")!)
    let tags: [String] = ["winloss", "sales", "saleswin", "tag3", "science", "apple", "orange"]

    var asset: AVURLAsset

    var body: some View {
        VStack(alignment: .leading, spacing: Constants.vstackSpacing) {
            if horizontalSizeClass == .compact || (horizontalSizeClass == .regular && verticalSizeClass == .regular) {
                VStack(alignment: .leading) {
                    Text("FAKE VIDDL TITLE")
                        .font(Constants.titleFont)
                        .foregroundColor(.white)

                    UserView(
                        userThumbnailURL: "https://viddlstagmediaservicestr.blob.core.windows.net/userphotos/2Thumbnail.png",
                        name: "Mandar Kulkarni",
                        jobTitle: "Engineering Manager"
                    )
                    .padding(.bottom)

                    Text("A long and careful description of stuff, blah, blah, blah")
                        .font(Constants.descFont)
                        .foregroundColor(Color.white)

                    PostDateLikeCountView(
                        postDate: Date(),
                        likeCount: 42
                    )

                    if !tags.isEmpty {
                        TagLozengeView(tags: tags)
                            .frame(height: TagLozengeView.Constants.frameHeight)
                    }
                }
            }

            SwiftUIVideoPlayer(asset: asset, autoPlay: true, desiredInitialSeekPosition: nil)
                .border(.white, width: 1)
        }
        .padding()
        .background(Constants.designerBlack)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(asset: AVURLAsset(url: Bundle.main.url(forResource: "SampleVideo", withExtension: "mp4")!))
    }
}
