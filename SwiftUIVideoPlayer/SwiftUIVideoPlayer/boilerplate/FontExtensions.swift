//
//  FontExtensions.swift
//  SwiftUIVideoPlayer
//
//  Created by Don Clore on 8/9/22.
//

import SwiftUI
import UIKit

public extension Font {
    init(uiFont: UIFont) {
        self = Font(uiFont as CTFont)
    }
}
