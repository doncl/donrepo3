//
//  Font.swift
//  SwiftUIVideoPlayer
//
//  Created by Don Clore on 8/9/22.
//

import Foundation
import UIKit

public enum Fonts {
    public static func get(type: FontName, size: Double) -> UIFont {
        let font = UIFont(name: type.name, size: CGFloat(size))
        if let font = font {
            return font
        }

        return UIFont.systemFont(ofSize: CGFloat(size))
    }
}

// HEAVY and LIGHT are not in the bundle - don't have a case for them, if we're not gonna install the dang things!
public enum FontName {
    case system
    case sfProMedium
//    case sfProHeavy
    case sfProSemibold
    case sfProRegular
//    case sfProLight
    case sfProBold

    public var name: String {
        switch self {
        case .system: return "System"
        case .sfProMedium: return "SFProText-Medium"
//        case .sfProHeavy:      return "SFProText-Heavy"
        case .sfProSemibold: return "SFProText-Semibold"
        case .sfProRegular: return "SFProText-Regular"
        //      case .sfProLight:      return "SFProText-Light"
        case .sfProBold: return "SFProText-Bold"
        }
    }
}
