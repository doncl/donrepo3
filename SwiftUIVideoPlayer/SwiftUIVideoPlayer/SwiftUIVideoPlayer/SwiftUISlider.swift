//
//  VideoSliderView.swift
//  ViddlUIKit
//
//  Created by Don Clore on 8/10/22.
//

import SwiftUI
import UIKit

struct SwiftUISlider: UIViewRepresentable {
    final class Coordinator: NSObject {
        // The class property value is a binding: It’s a reference to the SwiftUISlider
        // value, which receives a reference to a @State variable value in the parent view
        var value: Binding<Double>

        // Create the binding when you initialize the Coordinator
        init(value: Binding<Double>) {
            self.value = value
        }

        // Create a valueChanged(_:) action
        @objc func valueChanged(_ sender: UISlider) {
            value.wrappedValue = Double(sender.value)
        }
    }

    var minTrackColor: UIColor?
    var maxTrackColor: UIColor?
    var thumbImage: UIImage?

    @Binding var value: Double

    func makeUIView(context: Context) -> UISlider {
        let slider = UISlider(frame: .zero)
        slider.setThumbImage(thumbImage, for: UIControl.State.normal)
        slider.minimumTrackTintColor = minTrackColor
        slider.maximumTrackTintColor = maxTrackColor
        slider.value = Float(value)

        slider.addTarget(
            context.coordinator,
            action: #selector(Coordinator.valueChanged(_:)),
            for: .valueChanged
        )

        return slider
    }

    func updateUIView(_ uiView: UISlider, context _: Context) {
        // Coordinating data between UIView and SwiftUI view
        uiView.value = Float(value)
    }

    func makeCoordinator() -> SwiftUISlider.Coordinator {
        Coordinator(value: $value)
    }
}

#if DEBUG
    struct SwiftUISlider_Previews: PreviewProvider {
        static var previews: some View {
            SwiftUISlider(
                minTrackColor: UIColor(named: "slider")!,
                maxTrackColor: UIColor.white,
                thumbImage: UIImage(named: "VideoSliderThumbIcon")!,
                value: .constant(0.5)
            )
        }
    }
#endif
