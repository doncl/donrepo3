//
//  VideoPlayerUICore.swift
//  ViddlUIKit
//
//  Created by Don Clore on 8/9/22.
//

import AVFoundation
import Combine
import SwiftUI
import UIKit

enum VideoState {
    case stopped
    case paused
    case playing

    var image: Image {
        switch self {
        case .stopped:
            return Image("playVideo")
        case .paused:
            return Image("playVideo")
        case .playing:
            return Image("pauseVideo")
        }
    }
}

class VideoPlayerUICore: UIView {
    enum Constants {
        static let sliderTopPad: CGFloat = 32.0
        static let gutters: CGFloat = 32
        static let defaultFramesPerSecond: Int32 = 30
    }

    let player: AVPlayer = .init()

    private lazy var playerLayer: AVPlayerLayer = {
        let pl = AVPlayerLayer(player: player)
        return pl
    }()

    private var progressBarHighlightedObserver: NSKeyValueObservation?

    let seekTimePublisher: CurrentValueSubject<CMTime, Never> = .init(CMTime.zero)

    var state: VideoState = .stopped

    var formerState: VideoState = .stopped

    var recorderTimer: Timer? {
        didSet {
            oldValue?.invalidate()
        }
    }

    var durationInSeconds: TimeInterval {
        guard let asset = asset else {
            return 0
        }
        let duration: CMTime = asset.duration
        let durationSeconds: TimeInterval = CMTimeGetSeconds(duration)

        return durationSeconds
    }

    public private(set) var isScrubbingInProgress: Bool = false
    public private(set) var isSeekInProgress = false /// Used to notify whether to update playback progress bar

    var subscriptions: [AnyCancellable] = .init()

    lazy var videoSlider: UISlider = {
        let videoSlider = UISlider()
        videoSlider.setThumbImage(UIImage(named: "VideoSliderThumbIcon")!, for: .normal)
        videoSlider.isUserInteractionEnabled = true
        videoSlider.addTarget(self, action: #selector(VideoPlayerUICore.sliderValueChanged(_:)), for: .valueChanged)
        let sliderOrange = UIColor(named: "slider")!
        videoSlider.tintColor = sliderOrange
        videoSlider.minimumTrackTintColor = sliderOrange
        videoSlider.maximumTrackTintColor = UIColor.white
        return videoSlider
    }()

    var asset: AVURLAsset? {
        didSet {
            guard let asset = asset else {
                return
            }

            let playerItem: AVPlayerItem = .init(asset: asset)
            player.replaceCurrentItem(with: playerItem)
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.black

        playerLayer.contentsGravity = CALayerContentsGravity.resizeAspect
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspect
        layer.addSublayer(playerLayer)
        contentMode = UIView.ContentMode.scaleAspectFit

        playerLayer.player = player
        layoutPlayerLayer()

        trackPlayerProgress()

        videoSlider.translatesAutoresizingMaskIntoConstraints = false
        addSubview(videoSlider)

        NSLayoutConstraint.activate([
            videoSlider.topAnchor.constraint(equalTo: topAnchor, constant: Constants.sliderTopPad),
            videoSlider.leftAnchor.constraint(equalTo: leftAnchor, constant: Constants.gutters),
            videoSlider.rightAnchor.constraint(equalTo: rightAnchor, constant: -Constants.gutters),
        ])

        progressBarHighlightedObserver = videoSlider.observe(\UISlider.isTracking, options: [.new, .old]) { [weak self] _, change in
            guard let self = self else { return }
            if let newValue = change.newValue {
                self.didChangeProgressBarDragging(newValue, self.videoSlider.value)
            }
        }
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func didChangeProgressBarDragging(_ newValue: Bool, _: Float) {
        isScrubbingInProgress = newValue
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layoutPlayerLayer()
    }

    private func layoutPlayerLayer() {
        playerLayer.frame = bounds
    }

    private func seekVideoToMatchSliderValue(_ value: Float) {
        guard let item = player.currentItem else {
            print("\(#function) - bailing")
            return
        }
        let duration = item.duration
        guard duration != CMTime.indefinite else {
            return
        }
        let desiredValue = Float64(value)
        guard desiredValue != Float64.infinity else {
            return
        }

        let desiredLocation = CMTimeMultiplyByFloat64(duration, multiplier: desiredValue)
        player.seek(to: desiredLocation)
    }

    @objc func sliderValueChanged(_ sender: UISlider) {
        let value: Float = sender.value

        guard let item = player.currentItem else {
            print("\(#function) - bailing")
            return
        }
        let duration = item.duration
        guard duration != CMTime.indefinite else {
            return
        }
        let desiredValue = Float64(value)
        guard desiredValue != Float64.infinity else {
            return
        }

        let desiredLocation = CMTimeMultiplyByFloat64(duration, multiplier: desiredValue)
        player.seek(to: desiredLocation)
    }

    func seek(to seekPosition: CMTime?) {
        guard let seekPosition = seekPosition else {
            return
        }

        let currentPosition = player.currentTime()
        let delta = CMTimeGetSeconds(CMTimeSubtract(currentPosition, seekPosition))
        guard abs(delta) > 1 else {
            return
        }

        isSeekInProgress = true
        player.seek(to: seekPosition, toleranceBefore: .zero, toleranceAfter: .zero) { [weak self] _ in
            guard let self = self else { return }
            self.isSeekInProgress = false
        }
    }

    private func seekVideoSlider(atTime time: CMTime) {
        guard !isScrubbingInProgress, !isSeekInProgress else {
            return
        }
        seekTimePublisher.send(time)
        let seconds = CMTimeGetSeconds(time)
        if let duration = getVideoDuration() {
            let durationSeconds = CMTimeGetSeconds(duration)
            videoSlider.value = Float(seconds / durationSeconds)
        }
    }

    private func getVideoDuration() -> CMTime? {
        return player.currentItem?.duration
    }

    func trackPlayerProgress() {
        let interval = CMTime(value: 1, timescale: 2)
        addPeriodicTimeObserver(withInterval: interval) { [weak self] progressTime in
            guard let self = self else { return }
            self.seekVideoSlider(atTime: progressTime)
        }
    }

    private func addPeriodicTimeObserver(withInterval interval: CMTime, completion: @escaping (_ progressTime: CMTime) -> Void) {
        player.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main) { progressTime in
            completion(progressTime)
        }
    }

    func play() {
        state = .playing
        player.play()
    }

    func stop() {
        state = .stopped
        player.rate = 0
    }

    func pause() {
        state = .paused
        player.pause()
    }
}

struct VideoPlayerRepresentable: UIViewRepresentable {
    @EnvironmentObject var viewModel: VideoPlayerViewModel

    class Coordinator: NSObject {
        var parent: VideoPlayerRepresentable
        var subscriptions: Set<AnyCancellable>

        init(parent: VideoPlayerRepresentable) {
            self.parent = parent
            subscriptions = Set<AnyCancellable>()
        }
    }

    typealias UIViewType = VideoPlayerUICore
    typealias Context = UIViewRepresentableContext<Self>

    var asset: AVURLAsset
    var desiredInitialSeekPosition: CMTime?

    var core: VideoPlayerUICore = .init()

    var seekTimePublisher: CurrentValueSubject<CMTime, Never> {
        return core.seekTimePublisher
    }

    init(asset: AVURLAsset, desiredInitialSeekPosition: CMTime?) {
        self.asset = asset
        self.desiredInitialSeekPosition = desiredInitialSeekPosition
    }

    func makeUIView(context _: Context) -> VideoPlayerUICore {
        core.asset = asset
        if let desiredInitialSeekPosition = desiredInitialSeekPosition {
            core.seek(to: desiredInitialSeekPosition)
            let duration = CMTimeGetSeconds(asset.duration)
            if duration > 0 {
                viewModel.sliderPosition = CMTimeGetSeconds(desiredInitialSeekPosition) / duration
            }
        }
        viewModel.duration = asset.duration

        return core
    }

    func updateUIView(_: VideoPlayerUICore, context _: Context) {}

    func makeCoordinator() -> Coordinator {
        let coor = Coordinator(parent: self)
//    seekTimePublisher
//      .sink { time in
//        let seconds = CMTimeGetSeconds(time)
//        coor.parent.viewModel.currentSeekTimePosition = seconds
//      }
//      .store(in: &coor.subscriptions)

        return coor
    }

    func play() {
        core.play()
    }

    func stop() {
        core.stop()
    }

    func pause() {
        core.pause()
    }

    func seek(to seekPosition: CMTime) {
        core.seek(to: seekPosition)
    }
}

class VideoPlayerViewModel: ObservableObject {
    @Published var currentSeekTimePosition: TimeInterval = 0 {
        willSet {
            objectWillChange.send()
        }
        didSet {
            guard let duration = duration else {
                sliderPosition = 0
                return
            }
            let durationInSeconds = CMTimeGetSeconds(duration)
            guard durationInSeconds > 0 else {
                sliderPosition = 0
                return
            }
            sliderPosition = currentSeekTimePosition / durationInSeconds
        }
    }

    @Published var sliderPosition: Double = 0 {
        willSet {
            objectWillChange.send()
        }
    }

    var duration: CMTime?
}
