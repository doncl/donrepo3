//
//  SwiftUIVideoPlayer.swift
//  ViddlUIKit
//
//  Created by Don Clore on 8/9/22.
//

import AVFoundation
import CoreMedia
import SwiftUI

struct SwiftUIVideoPlayer: View {
    var formattedSeekTime: String {
        let intValue = Int(videoViewModel.currentSeekTimePosition)

        return "\(intValue)"
    }

    let autoPlay: Bool
    let initialSeekPosition: CMTime?
    var asset: AVURLAsset
    private var videoPlayer: VideoPlayerRepresentable
    @StateObject var videoViewModel: VideoPlayerViewModel = .init()

    @State private var videoState: VideoState = .stopped

    var body: some View {
        GeometryReader { _ in
            VStack {
                ZStack {
                    videoPlayer
                        .border(.blue, width: 1.0)
                        .onAppear {
                            if autoPlay {
                                print("\(#function) - video player appearing....autoplaying..")
                                videoState = .playing
                                videoPlayer.play()
                            }
                        }
                        .onDisappear {
                            print("\(#function) - video player disappearing....stopping..")
                            videoState = .stopped
                            videoPlayer.stop()
                        }

                    Button {
                        buttonTapped()
                    } label: {
                        videoState.image
                    }
                }
            }
        }
        .onChange(of: videoViewModel.sliderPosition) { position in
            let seekPosition = CMTimeMultiplyByFloat64(asset.duration, multiplier: position)
            videoPlayer.seek(to: seekPosition)
        }
        .environmentObject(videoViewModel)
    }

    private func buttonTapped() {
        switch videoState {
        case .stopped:
            videoState = .playing
            videoPlayer.play()
        case .paused:
            videoState = .playing
            videoPlayer.play()
        case .playing:
            videoState = .paused
            videoPlayer.pause()
        }
    }

    init(asset: AVURLAsset, autoPlay: Bool, desiredInitialSeekPosition: CMTime? = nil) {
        initialSeekPosition = desiredInitialSeekPosition
        self.asset = asset
        self.autoPlay = autoPlay
        videoPlayer = VideoPlayerRepresentable(asset: asset, desiredInitialSeekPosition: desiredInitialSeekPosition)
    }
}

struct SwiftUIVideoPlayer_Previews: PreviewProvider {
    static let asset: AVURLAsset = .init(url: Bundle.main.url(forResource: "piano", withExtension: "mp4")!)

    static var previews: some View {
        SwiftUIVideoPlayer(asset: asset, autoPlay: false)
    }
}
