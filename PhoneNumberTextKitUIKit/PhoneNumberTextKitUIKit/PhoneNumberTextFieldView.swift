//
import UIKit
import PhoneNumberKit
import SwiftUI

typealias PickerCallback = (CountryCodePickerVCRepresentable) -> Void

/// Wraps the PhoneNumberKit library's CountryCodePickerViewController in
/// a UIViewControllerRepresentable so SwiftUI can cope with it.
/// N.B. the delegate for the vc is actually the PhoneNumberTextField itself,
/// which is ready-made for handling all the callbacks.
struct CountryCodePickerVCRepresentable: UIViewControllerRepresentable {
    let phoneNumberKit: PhoneNumberKit
    @State private var delegate: CountryCodePickerDelegate
    
    init(phoneNumberKit: PhoneNumberKit, delegate: CountryCodePickerDelegate) {
        self.phoneNumberKit = phoneNumberKit
        self.delegate = delegate
    }
        
    func makeUIViewController(context: Context) -> CountryCodePickerViewController {
        let vc = CountryCodePickerViewController(phoneNumberKit: phoneNumberKit)
        vc.delegate = delegate
        return vc
    }
    
    // No need for this so far.
    func updateUIViewController(_ uiViewController: CountryCodePickerViewController, context: Context) {
    }
}

///  A subclass of the PhoneNumberTextField that turns off the defaultPickerUI, and adds a
///  tap handler to the flagButton so we can intercede and turn the CountryCodePickerViewController
///  into a UIViewControllerRepresentable that SwiftUI can deal with.
class PhoneNumberFieldAndCountryPickerProvider: PhoneNumberTextField {
    let pickerCallback: PickerCallback
    
    init(pickerCallback: @escaping PickerCallback) {
        self.pickerCallback = pickerCallback
        super.init(frame: .zero)
        withDefaultPickerUI = false // turn this off...
        // ... and add our own target-action.
        flagButton.addTarget(
            self,
            action: #selector(PhoneNumberFieldAndCountryPickerProvider.flagButtonTapped(_:)),
            for: .touchUpInside)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// This handles the flag tap by creating a SwiftUI-compatible wrapped UIViewController of
    /// the CountryCodePickerViewController, and passes it back in the pickerCallback so
    /// SwiftUI can deal with it sanely
    @MainActor
    @objc func flagButtonTapped(_ sender: UIButton) {
        let vcRepresentable = CountryCodePickerVCRepresentable(phoneNumberKit: phoneNumberKit, delegate: self)
        pickerCallback(vcRepresentable)
    }
}

/// A SwiftUI View (UIViewRepresentable) that wraps a PhoneNumberFieldAndCountryPickerProvider,
/// which is a PhoneNumberKit PhoneNumberField with some extra wiring to intercede with the
/// normal UIKit CountryCodePickerViewController presentation process, and instead wrap
/// it up into a UIViewControllerRepresentable, so SwiftUI doesn't go insane.
struct PhoneNumberTextFieldView: UIViewRepresentable {
    private let textField: PhoneNumberFieldAndCountryPickerProvider
    @Binding var becomeFirstResponder: Bool
    
    // Using @State to cause this to persist when the struct is thrown away.
    @State var delegate: PhoneNumberTextFieldDelegate
 
    func makeUIView(context: Context) -> PhoneNumberTextField {
        textField.withExamplePlaceholder = true
        textField.withFlag = true
        textField.withPrefix = true
        textField.withDefaultPickerUI = false // turn this off, because we're doing special handling.
        textField.delegate = self.delegate
        textField.becomeFirstResponder()
        return textField
    }
    
    func updateUIView(_ view: PhoneNumberTextField, context: Context) {
        view.delegate = self.delegate
    }
    
    @MainActor
    init(phoneNumber: Binding<String>, becomeFirstResponder: Binding<Bool>, callback: @escaping EditCallback, pickerCallback: @escaping PickerCallback) {
        
        // This bit of code makes the country picker comply with our design kit.
        UITableView.appearance(whenContainedInInstancesOf: [CountryCodePickerViewController.self]).backgroundColor = UIColor(named: "fill4")
        UITableViewCell.appearance(whenContainedInInstancesOf: [CountryCodePickerViewController.self]).backgroundColor = UIColor(named: "fill1")
        UILabel.appearance(whenContainedInInstancesOf: [CountryCodePickerViewController.self]).textColor = UIColor(named: "primaryText")
        UIView.appearance(whenContainedInInstancesOf: [UITableViewCell.self, CountryCodePickerViewController.self]).backgroundColor = UIColor(named: "fill1")
        UIView.appearance(whenContainedInInstancesOf: [UITableViewCell.self, CountryCodePickerViewController.self]).alpha = 1
        UIView.appearance(whenContainedInInstancesOf: [UITableViewCell.self, CountryCodePickerViewController.self]).isOpaque = true
        UITableView.appearance().backgroundColor = UIColor(named: "fill4")
        UITableView.appearance(whenContainedInInstancesOf: [CountryCodePickerViewController.self]).alpha = 1
                
        self.textField = PhoneNumberFieldAndCountryPickerProvider(pickerCallback: pickerCallback)
        self.textField.backgroundColor = UIColor(named: "fill4")?.withAlphaComponent(1.0)
        
        self._becomeFirstResponder = becomeFirstResponder
        
        // This callback is for handling 'normal' textfield .onCommit (i.e. user hits enter, or other.
        self.delegate = PhoneNumberTextFieldDelegate(phoneNumber: phoneNumber, callback: callback)
    }
}

typealias EditCallback = () -> Void

@MainActor
class PhoneNumberTextFieldDelegate: NSObject, UITextFieldDelegate {
    let editCallback: EditCallback
    @Binding var phoneNumber: String
    
    init(phoneNumber: Binding<String>, callback: @escaping EditCallback) {
        self._phoneNumber = phoneNumber
        self.editCallback = callback
    }
        
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        defer {
            textField.resignFirstResponder()
            editCallback()
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard var text = textField.text else {
            return true
        }
        textField.becomeFirstResponder()
        let start = text.index(text.startIndex, offsetBy: range.location)
        let end = text.index(start, offsetBy: range.length)
        let range = start..<end
        text.replaceSubrange(range, with: string)
        phoneNumber = text
        return true
    }
}

