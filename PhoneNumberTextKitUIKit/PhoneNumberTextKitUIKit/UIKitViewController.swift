//
//  ViewController.swift
//  PhoneNumberTextKitUIKit
//
//  Created by Don Clore on 5/23/23.
//

import UIKit
import PhoneNumberKit

class UIKitViewController: UIViewController {
    
    let phoneNumberKit = PhoneNumberKit()
    let textFieldWithFlagPrefixAndPlaceholder = PhoneNumberTextField()
    let plainTextField = PhoneNumberTextField()
    let textFieldWithPlaceholder = PhoneNumberTextField()
    let textFieldWithFlag = PhoneNumberTextField()
    let textFieldWithPrefix = PhoneNumberTextField()
    
    lazy var button: UIButton = {
        let b = UIButton(type: .custom)
        b.setTitle("Validate fields!", for: .normal)
        b.setTitleColor(.blue, for: .normal)
        b.translatesAutoresizingMaskIntoConstraints = false
        b.addTarget(self, action: #selector(UIKitViewController.buttonPressed(_:)), for: .touchUpInside)
        return b
    }()
    
    lazy var clearButton: UIButton = {
        let b = UIButton(type: .custom)
        b.setTitle("Clear red borders!", for: .normal)
        b.setTitleColor(.blue, for: .normal)
        b.translatesAutoresizingMaskIntoConstraints = false
        b.addTarget(self, action: #selector(UIKitViewController.clearBordersPressed(_:)), for: .touchUpInside)
        return b
    }()
    
    let stack = UIStackView()
    lazy var flagPrefixPlaceholderLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.textColor = .black
        l.font = UIFont.preferredFont(forTextStyle: .subheadline)
        l.textAlignment = .center
        l.text = "Flag, Placeholder"
        return l
    }()
    
    lazy var plainLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.textColor = .black
        l.font = UIFont.preferredFont(forTextStyle: .subheadline)
        l.textAlignment = .center
        l.text = "Plain PhoneNumber Textfield"
        return l
    }()

    lazy var placeholderLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.textColor = .black
        l.font = UIFont.preferredFont(forTextStyle: .subheadline)
        l.textAlignment = .center
        l.text = "Placeholder only"
        return l
    }()
    
    lazy var flagLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.textColor = .black
        l.font = UIFont.preferredFont(forTextStyle: .subheadline)
        l.textAlignment = .center
        l.text = "Flag only"
        return l
    }()

    lazy var prefixLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.textColor = .black
        l.font = UIFont.preferredFont(forTextStyle: .subheadline)
        l.textAlignment = .center
        l.text = "Prefix only"
        return l
    }()
    
    let scroll = UIScrollView()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        stack.axis = .vertical
        stack.distribution = .equalCentering
        stack.alignment = .center
        stack.spacing = 32
        stack.translatesAutoresizingMaskIntoConstraints = false
        scroll.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scroll)
        scroll.addSubview(stack)
        scroll.backgroundColor = UIColor(white: 245 / 255, alpha: 0.7)
        button.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(button)
        view.addSubview(clearButton)
        
        [textFieldWithFlagPrefixAndPlaceholder, plainTextField, textFieldWithPlaceholder, textFieldWithFlag, textFieldWithPrefix].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.font = UIFont.preferredFont(forTextStyle: .largeTitle)
            $0.textColor = .black
            $0.layer.borderColor = UIColor.black.cgColor
            $0.layer.borderWidth = 1.0
        }
        
        // Flag
        [textFieldWithFlagPrefixAndPlaceholder, textFieldWithFlag].forEach {
            $0.withFlag = true
            $0.withDefaultPickerUI = true        
        }
        
        // Prefix
        [textFieldWithPrefix].forEach {
            $0.withPrefix = true
        }
        
        // Placholder
        [textFieldWithFlagPrefixAndPlaceholder, textFieldWithPrefix].forEach {
            $0.withExamplePlaceholder = true
        }
                
        NSLayoutConstraint.activate([
            scroll.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scroll.bottomAnchor.constraint(equalTo: button.safeAreaLayoutGuide.topAnchor, constant: -16),
            scroll.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            scroll.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
                        
            clearButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16),
            clearButton.widthAnchor.constraint(equalToConstant: 200),
            clearButton.heightAnchor.constraint(equalToConstant: 60),
            clearButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),

            button.bottomAnchor.constraint(equalTo: clearButton.topAnchor),
            button.widthAnchor.constraint(equalToConstant: 200),
            button.heightAnchor.constraint(equalToConstant: 60),
            button.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            stack.topAnchor.constraint(equalTo: scroll.topAnchor),
            stack.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            stack.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            stack.bottomAnchor.constraint(equalTo: scroll.bottomAnchor),
            stack.leadingAnchor.constraint(equalTo: scroll.leadingAnchor),
            stack.trailingAnchor.constraint(equalTo: scroll.trailingAnchor),
        ])
        
        [flagPrefixPlaceholderLabel, textFieldWithFlagPrefixAndPlaceholder, plainLabel, plainTextField, placeholderLabel, textFieldWithPlaceholder,
         flagLabel, textFieldWithFlag, prefixLabel, textFieldWithPrefix].forEach {
            stack.addArrangedSubview($0)
            
            NSLayoutConstraint.activate([
                $0.widthAnchor.constraint(equalTo: stack.widthAnchor, multiplier: 1, constant: -32),
                $0.heightAnchor.constraint(equalToConstant: 40),
            ])
        }
        textFieldWithFlagPrefixAndPlaceholder.backgroundColor = UIColor(named: "fill4")
        textFieldWithFlagPrefixAndPlaceholder.clipsToBounds = true
        textFieldWithFlagPrefixAndPlaceholder.layer.cornerRadius = 14
        textFieldWithFlagPrefixAndPlaceholder.layer.allowsEdgeAntialiasing = true
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        [textFieldWithFlagPrefixAndPlaceholder, plainTextField, textFieldWithPlaceholder, textFieldWithFlag, textFieldWithPrefix].forEach {
            if let text = $0.text {
                if !phoneNumberKit.isValidPhoneNumber(text) {
                    $0.layer.borderColor = UIColor.red.cgColor
                    $0.layer.borderWidth = 4.0
                } else {
                    do {
                        let phonenumber = try phoneNumberKit.parse(text)
                        print(phonenumber.countryCode)
                    } catch {
                        print(error)
                    }
                }
            }
        }
    }
    
    @objc func clearBordersPressed(_ sender: UIButton) {
        [textFieldWithFlagPrefixAndPlaceholder, plainTextField, textFieldWithPlaceholder, textFieldWithFlag, textFieldWithPrefix].forEach {
            $0.layer.borderColor = UIColor.black.cgColor
            $0.layer.borderWidth = 1.0
        }
    }
}

