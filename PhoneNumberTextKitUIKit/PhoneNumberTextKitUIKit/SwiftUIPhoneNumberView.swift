//
//  SwiftUIPhoneNumberKit.swift
//  PhoneNumberTextKitUIKit
//
//  Created by Don Clore on 5/23/23.
//

import SwiftUI
import PhoneNumberKit

struct SwiftUIPhoneNumberView: View {
    @Environment(\.dismiss) private var dismiss
    @State private var phoneNumber: String = ""
    @State private var phoneNumberKit = PhoneNumberKit()
    @State private var showAlert = false
    @State private var validatedPhoneNumber: PhoneNumber?
    @State private var becomeFirstResponder: Bool = true
    @State private var picker: CountryCodePickerVCRepresentable?
    @State private var showPicker = false
    
    
    var body: some View {
        let showPickerBinding = makeShowPickerBinding()
        
        ScrollView {
            VStack(alignment: .center, spacing: 60) {
                ZStack {
                    
                    HStack {
                        Spacer()
                        
                        Button {
                            dismiss.callAsFunction()
                        } label: {
                            Label("", sfSymbol: .close)
                                .foregroundColor(.teleportPrimaryText)
                                .font(.system(.body, weight: .bold))
                        }
                        .multilineTextAlignment(.trailing)
                        .frame(width: 23, height: 23)
                        .padding(.trailing, 20.5)
                    }
                }
                HStack {
                    PhoneNumberTextFieldView(
                        phoneNumber: $phoneNumber,
                        becomeFirstResponder: $becomeFirstResponder,
                        callback: {
                            validatePhoneNumber()
                        }, pickerCallback: { picker in
                            self.picker = picker
                            self.showPicker = true
                        }
                    )
                    .background(Color.teleportFill4)
                    .frame(height: 66)
                    .padding([.leading, .trailing], 16)
                    .keyboardType(.phonePad)
                }
                .background(Color.teleportFill4)
                .cornerRadius(24)
                .padding([.leading, .trailing], 16)

            }
            .padding(.top, 40)
            .sheet(
                isPresented: showPickerBinding,
                onDismiss: {
                    showPicker = false
                    picker = nil
                },
                content: {
                    // This is guaranteed to be non-nil by the custom Bool binding 'showPickerBinding'
                    picker!
                }
            )
        }
        .background(Color.teleportFill1)
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .toolbar(.hidden)
        .alert(isPresented: $showAlert) {
            if let message = validatedPhoneNumber {
                return Alert(
                    title: Text("Valid"),
                    message: Text("number: \(message.numberString), countryCode: \(message.countryCode)"),
                    dismissButton: .default(Text("OK"))
                )
            } else {
                return Alert(
                    title: Text("Invalid"),
                    message: Text("number: \(phoneNumber) is not valid"),
                    dismissButton: .default(Text("OK"), action: {
                        becomeFirstResponder = true
                    })
                )
            }
        }
    }
    
    private func makeShowPickerBinding() -> Binding<Bool> {
        Binding<Bool>(
            get: {
                guard picker != nil else {
                    return false
                }
                return showPicker
            },
            set: { _ in
            }
        )
    }
    
    private func validatePhoneNumber() {
        do {
            validatedPhoneNumber = try phoneNumberKit.parse(self.phoneNumber)
            showAlert = true
        } catch {
            print(error)
            validatedPhoneNumber = nil
            showAlert = true
        }
    }
}

struct SwiftUIPhoneNumberKit_Previews: PreviewProvider {
    static var previews: some View {
        SwiftUIPhoneNumberView()
    }
}

extension Label where Title == Text, Icon == Image {
    public init(_ title: any StringProtocol, sfSymbol: SFSymbol) {
        self.init(title, systemImage: sfSymbol.systemName)
    }
}
public enum SFSymbol: String, CaseIterable {
    case back = "arrow.backward"
    case foreward = "arrow.forward"
    case qrCode = "qrcode"
    case chevron = "chevron.right"
    case warning = "exclamationmark.triangle.fill"
    case success = "checkmark.circle.fill"
    case info = "info.circle.fill"
    case locationFill = "location.fill"
    case locationPin = "mappin"
    case locationCar = "car.circle"
    case signature = "signature"
    case car = "car.fill"
    case magnifyingGlass = "magnifyingglass"
    case close = "xmark"
    case creditCard = "creditcard"
    case creditCardFill = "creditcard.fill"
    case walletPass = "wallet.pass"
    case send = "arrow.up"
    case message = "message.fill"
    case copy = "doc.on.doc"
    case reload = "arrow.triangle.2.circlepath"
    case personFill = "person.fill"
    case smallcircleFilledCircle = "smallcircle.filled.circle"
    case figureWave = "figure.wave"
    case flagCheckered = "flag.checkered"
    
    public var systemName: String { rawValue }
}
