//
//  SwiftUI.ColorExtensions.swift
//  PhoneNumberTextKitUIKit
//
//  Created by Don Clore on 5/24/23.
//

import Foundation
import SwiftUI

extension Color {
    public static let teleportFill1 = Color("fill1")
    public static let teleportFill2 = Color("fill2")
    public static let teleportFill3 = Color("fill3")
    public static let teleportFill4 = Color("fill4")
    public static let teleportFill5 = Color("fill5")
    public static let teleportFill6 = Color("fill6")
    public static let teleportFill7 = Color("fill7")
    public static let teleportFill8 = Color("fill8")
    
    public static let teleportPrimaryText = Color("primaryText")
    public static let teleportSecondaryText = Color("secondaryText")
    public static let teleportTertiaryText = Color("tertiaryText")
    public static let teleportPrimaryTextInverted = Color("primaryTextInverted")
    public static let teleportSecondaryTextInverted = Color("secondaryTextInverted")
    public static let teleportTertiaryTextInverted = Color("tertiaryTextInverted")
    
    public static let teleportSystem1 = Color("system1")
    public static let teleportSystem2 = Color("system2")
    public static let teleportSystem3 = Color("system3")
    public static let teleportSystem4 = Color("system4")
    
    public static let teleportDivider1 = Color("divider1")
    public static let teleportDivider2 = Color("divider2")
    
    public static let teleportError = Color("error")
    public static let teleportWarning = Color("warning")
    public static let teleportSuccess = Color("success")
}

struct Color_Previews: PreviewProvider {
    static var allColors: some View {
        VStack {
            VStack(alignment: .leading) {
                Text("Fill")
                HStack {
                    Color.teleportFill1
                    Color.teleportFill2
                    Color.teleportFill3
                    Color.teleportFill4
                    Color.teleportFill5
                    Color.teleportFill6
                    Color.teleportFill7
                    Color.teleportFill8
                }
            }
            VStack(alignment: .leading) {
                Text("Text")
                HStack {
                    Color.teleportPrimaryText
                    Color.teleportSecondaryText
                    Color.teleportTertiaryText
                    Color.teleportPrimaryTextInverted
                    Color.teleportSecondaryTextInverted
                    Color.teleportTertiaryTextInverted
                }
            }
            VStack(alignment: .leading) {
                Text("System")
                HStack {
                    Color.teleportSystem1
                    Color.teleportSystem2
                    Color.teleportSystem3
                    Color.teleportSystem4
                }
            }
            VStack(alignment: .leading) {
                Text("Divider")
                HStack {
                    Color.teleportDivider1
                    Color.teleportDivider2
                }
            }
            VStack(alignment: .leading) {
                Text("Status")
                HStack {
                    Color.teleportError
                    Color.teleportWarning
                    Color.teleportSuccess
                }
            }
        }
    }
    static var previews: some View {
        allColors
            .preferredColorScheme(.dark)
            .previewDisplayName("Dark")
            .padding()
            .background(Color(red: 246/255.0, green: 246/255.0, blue: 246/255.0, opacity: 0.2))
        allColors
            .preferredColorScheme(.light)
            .previewDisplayName("Light")
            .padding()
            .background(Color(red: 207/255.0, green: 208/255.0, blue: 213/255.0, opacity: 0.2))
    }
}

