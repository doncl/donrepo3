//
//  RootVC.swift
//  PhoneNumberTextKitUIKit
//
//  Created by Don Clore on 5/23/23.
//

import UIKit
import SwiftUI

class RootVC: UIViewController {
    let tabBarC = UITabBarController()
    let uiKit = UIKitViewController()
    let swiftUI = UIHostingController(rootView: SwiftUIPhoneNumberView())

    override func viewDidLoad() {
        super.viewDidLoad()

        tabBarC.viewControllers = [uiKit, swiftUI]
        tabBarC.selectedIndex = 0
                
        addChild(tabBarC)
        tabBarC.loadViewIfNeeded()
        tabBarC.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tabBarC.view)
        tabBarC.didMove(toParent: self)
            
        NSLayoutConstraint.activate([
          tabBarC.view.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
          tabBarC.view.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
          tabBarC.view.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
          tabBarC.view.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
        ])
        
        uiKit.tabBarItem = UITabBarItem(title: "UIKit", image: nil, tag: 0)
        swiftUI.tabBarItem = UITabBarItem(title: "SwiftUI", image: nil, tag: 1)
    }
    

}
