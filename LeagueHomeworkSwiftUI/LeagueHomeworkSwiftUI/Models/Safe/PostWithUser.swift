//
//  PostWithUser.swift
//  LeagueHomework
//
//  Created by Don Clore on 8/20/22.
//

import Foundation

struct PostWithUser: Identifiable, Hashable {
    var id = UUID()
    let post: SafePost
    let user: SafeUser
}

let testUser: PostWithUser =
    .init(
        post: SafePost(userID: 3, id: 26, title: "est et quae odit qui non", body: "similique esse doloribus nihil ..."),
        user: SafeUser(
            id: 1,
            avatar: URL(string: "https://i.pravatar.cc/150?u=1")!,
            name: "Leanne Graham",
            userName: "Bret",
            email: "Sincere@april.biz5",
            address: SafeUser.Address(
                street: "Kulas Light",
                suite: "Apt. 556",
                city: "Gwenborough",
                zipCode: "92998-3874",
                geo: SafeUser.Geo(
                    latitude: -37.3159,
                    longitude: 81.1496
                )
            ),
            phone: "1-770-736-8031 x56442",
            website: URL(string: "hildegard.org"),
            company: SafeUser.Company(
                name: "Romaguera-Crona",
                catchPhrase: "Multi-layered...",
                bs: "harness real-time e-markets"
            )
        )
    )
