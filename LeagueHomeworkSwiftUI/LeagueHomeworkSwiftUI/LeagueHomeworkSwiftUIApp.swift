//
//  LeagueHomeworkSwiftUIApp.swift
//  LeagueHomeworkSwiftUI
//
//  Created by Don Clore on 10/2/22.
//

import SwiftUI

@main
struct LeagueHomeworkSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
