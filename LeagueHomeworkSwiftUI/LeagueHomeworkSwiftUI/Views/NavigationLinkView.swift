//
//  NavigationLinkView.swift
//  LeagueHomeworkSwiftUI
//
//  Created by Don Clore on 10/2/22.
//

import Kingfisher
import SwiftUI

struct NavigationLinkView: View {
    enum Constants {
        static let avatarDim: CGFloat = 64
        static let userStackSpacing: CGFloat = 16
        static let titleFontSize: CGFloat = 18
        static let bodyFontSize: CGFloat = 14
    }

    var postWithUser: PostWithUser

    var body: some View {
        VStack(alignment: .leading) {
            HStack(spacing: Constants.userStackSpacing) {
                KFImage(postWithUser.user.avatar)
                    .resizable()
                    .frame(width: Constants.avatarDim, height: Constants.avatarDim)
                    .clipShape(Circle())

                Text(postWithUser.user.name)
                    .font(.system(size: 22, weight: .semibold))

                Spacer()
                NavigationLink("", value: postWithUser)
            }
            .padding(.bottom)

            Text(postWithUser.post.title)
                .font(.system(size: Constants.titleFontSize, weight: .semibold))
                .multilineTextAlignment(.leading)
                .padding(.bottom)

            Text(postWithUser.post.body)
                .font(.system(size: Constants.bodyFontSize, weight: .regular))
                .multilineTextAlignment(.leading)
        }
        .padding()
    }
}

struct NavigationLinkView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationLinkView(postWithUser: testUser)
    }
}
