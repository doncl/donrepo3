//
//  ContentView.swift
//  LeagueHomeworkSwiftUI
//
//  Created by Don Clore on 10/2/22.
//

import Combine
import SwiftUI

struct TwoColumnView: View {
    @EnvironmentObject var vm: ViewModel
    @State private var columnVisibility = NavigationSplitViewVisibility.doubleColumn

    @State var selection: PostWithUser? {
        didSet {
            if let selection = selection {
                selectionPath = [selection]
            }
        }
    }

    @State private var selectionPath: [PostWithUser] = []

    var body: some View {
        return Group {
            if vm.postsWithUsers.isEmpty {
                Spacer()
                ProgressView("Loading....")
                Spacer()
            } else {
                NavigationSplitView(columnVisibility: $columnVisibility, sidebar: {
                    List(vm.postsWithUsers, selection: $selection) { postWithUser in
                        NavigationLinkView(postWithUser: postWithUser)
                    }
                    .navigationTitle("User Posts")
                }, detail: {
                    NavigationStack(path: $selectionPath) {
                        if let selection = selection {
                            UserView(user: selection.user)
                        }
                    }
                })
            }
        }
        .alert(vm.error?.localizedDescription ?? "Ouch", isPresented: $vm.showError) {
            Button("OK", role: .cancel) {}
        }
    }
}

struct TwoColumnView_Previews: PreviewProvider {
    static let vm: ViewModel = .init()
    static var previews: some View {
        TwoColumnView()
            .environmentObject(vm)
            .onAppear {
                vm.kickOffGets()
            }
    }
}
