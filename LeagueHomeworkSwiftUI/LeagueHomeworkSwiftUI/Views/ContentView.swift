//
//  ContentView.swift
//  LeagueHomeworkSwiftUI
//
//  Created by Don Clore on 10/2/22.
//

import SwiftUI

struct ContentView: View {
    @StateObject var vm: ViewModel = .init()
    @State private var selectedSegment: String = "Two Column View"

    var navigationTypes: [String] = ["Two Column View", "Three Column View", "NavigationView (deprecated)"]

    var body: some View {
        VStack {
            Picker("Type of navigation", selection: $selectedSegment) {
                ForEach(navigationTypes, id: \.self) {
                    Text($0)
                }
            }
            .pickerStyle(.segmented)
            .onChange(of: selectedSegment) { selected in
                selectedSegment = selected
            }

            if selectedSegment == navigationTypes[0] {
                TwoColumnView()
            } else if selectedSegment == navigationTypes[1] {
                ThreeColumnView()
            } else {
                NavigationViewView()
            }
        }
        .environmentObject(vm)
        .onAppear {
            vm.kickOffGets()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
