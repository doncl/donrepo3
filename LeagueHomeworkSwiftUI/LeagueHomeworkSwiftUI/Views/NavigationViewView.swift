//
//  NavigationViewView.swift
//  LeagueHomeworkSwiftUI
//
//  Created by Don Clore on 10/4/22.
//

import SwiftUI

struct NavigationViewView: View {
    @EnvironmentObject var vm: ViewModel

    @State var selection: PostWithUser?

    var body: some View {
        if vm.postsWithUsers.isEmpty {
            ProgressView("Loading...")
        } else {
            NavigationView {
                List(vm.postsWithUsers, selection: $selection) { postWithUser in
                    NavigationLink(postWithUser.user.name) {
                        UserView(user: postWithUser.user)
                    }
                }
            }
        }
    }
}

struct NavigationViewView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationViewView()
    }
}
