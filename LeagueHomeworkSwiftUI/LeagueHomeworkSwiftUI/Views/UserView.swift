//
//  UserView.swift
//  LeagueHomeworkSwiftUI
//
//  Created by Don Clore on 10/2/22.
//

import Kingfisher
import MapKit
import SwiftUI

struct UserView: View {
    enum Constants {
        static let avatarDim: CGFloat = 64
        static let gutters: CGFloat = 24
        static let mapAspectRatio: CGFloat = 0.75
        static let formHeight: CGFloat = 550

        // OK guys, I figured the latlong values are in the middle of the Pacific Ocean, haha.
        // OK, so randomly swap between League's office locations.
        static let torontoGeo: SafeUser.Geo = .init(latitude: 43.6532, longitude: -79.3832)
        static let chicagogeo: SafeUser.Geo = .init(latitude: 41.7871, longitude: -87.6298)
    }

    var user: SafeUser

    @State private var region =
        MKCoordinateRegion(
            center: CLLocationCoordinate2D(
                latitude: 46.6062,
                longitude: 122.3321
            ), // Seattle
            span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5)
        )

    var body: some View {
        GeometryReader { proxy in
            ScrollView {
                KFImage(user.avatar)
                    .resizable()
                    .frame(width: Constants.avatarDim, height: Constants.avatarDim)
                    .cornerRadius(Constants.avatarDim / 2, antialiased: true)

                Text(user.name)
                    .font(.title)

                Form {
                    Section(header: Text("Primary Properties")) {
                        FormRow(labelText: "ID:", valueText: String(user.id))
                        FormRow(labelText: "User Name:", valueText: user.userName)
                        FormRow(labelText: "Email:", valueText: user.email)
                        if let phone = user.phone {
                            FormRow(labelText: "Phone:", valueText: phone)
                        }
                        if let company = user.company {
                            FormRow(labelText: "Company:", valueText: company.name)
                        }
                    }

                    if let address = user.address {
                        Section(header: Text("Address:")) {
                            FormRow(labelText: "Street:", valueText: address.street)
                            if let suite = address.suite {
                                FormRow(labelText: "Suite:", valueText: suite)
                            }
                            FormRow(labelText: "City:", valueText: address.city)
                            FormRow(labelText: "Zip Code:", valueText: address.zipCode)
                        }
                    }
                }
                .frame(width: proxy.size.width, height: Constants.formHeight)

                MapView(geo: Bool.random() ? Constants.torontoGeo : Constants.chicagogeo)
                    .frame(width: proxy.size.width, height: proxy.size.width * Constants.mapAspectRatio)
            }
        }
    }
}

struct MapView: View {
    var geo: SafeUser.Geo

    var body: some View {
        Map(coordinateRegion:
            .constant(
                MKCoordinateRegion(
                    center: CLLocationCoordinate2D(
                        latitude: geo.latitude,
                        longitude: geo.longitude
                    ),
                    span: MKCoordinateSpan(
                        latitudeDelta: 1,
                        longitudeDelta: 1
                    )
                )))
    }
}

struct FormRow: View {
    var labelText: String
    var valueText: String

    var body: some View {
        HStack {
            Text(labelText)
            Spacer()
            Text(valueText)
                .truncationMode(.middle)
        }
    }
}

struct UserView_Previews: PreviewProvider {
    static let user: SafeUser =
        .init(
            id: 1,
            avatar: URL(string: "https://i.pravatar.cc/150?u=1")!,
            name: "Leanne Graham",
            userName: "Bret",
            email: "Sincere@april.biz",
            address: SafeUser.Address(
                street: "Kulas Light",
                suite: "Apt. 556",
                city: "Gwenborough",
                zipCode: "92998-3874",
                geo: SafeUser.Geo(
                    latitude: -37.3159,
                    longitude: 81.1496
                )
            ),
            phone: "1-770-736-8031 x56442",
            website: URL(string: "hidegard.org")!,
            company: SafeUser.Company(
                name: "Romaguera-Crona",
                catchPhrase: "Multi-layered...",
                bs: "harness real-time e-markets"
            )
        )

    static var previews: some View {
        UserView(user: user)
    }
}
