//
//  ThreeColumnView.swift
//  LeagueHomeworkSwiftUI
//
//  Created by Don Clore on 10/2/22.
//

import Combine
import Kingfisher
import SwiftUI

struct ThreeColumnView: View {
    enum Constants {
        static let imageDim: CGFloat = 64
    }

    @EnvironmentObject var vm: ViewModel
    @State private var columnVisibility = NavigationSplitViewVisibility.all

    @State var selection: PostWithUser? {
        didSet {
            if let selection = selection {
                selectionPath = [selection]
            }
        }
    }

    @State var selectionPath: [PostWithUser] = []

    var body: some View {
        if vm.postsWithUsers.isEmpty {
            ProgressView("Loading....")
        } else {
            NavigationSplitView(columnVisibility: $columnVisibility) {
                List(vm.postsWithUsers, selection: $selection) { postWithUser in
                    NavigationLink(postWithUser.user.name, value: postWithUser)
                }
                .navigationTitle("User Posts")
            } content: {
                if let selection = selection {
                    VStack {
                        NavigationLinkView(postWithUser: selection)
                        Spacer()
                    }
                }
            } detail: {
                if let selection = selection {
                    UserView(user: selection.user)
                }
            }
        }
    }
}

struct ThreeColumnView_Previews: PreviewProvider {
    static var previews: some View {
        ThreeColumnView()
    }
}
