//
//  NonCustomAlignmentGuide.swift
//  CustomAlignmentGuides
//
//  Created by Don Clore on 4/17/23.
//

import SwiftUI

struct NonCustomAlignmentGuide: View {
  var body: some View {
    VStack(alignment: .leading) {
      ForEach(0 ..< 10) { position in
        Text("Number \(position)")
          .alignmentGuide(.leading) { _ in
            CGFloat(position) * -10
          }
      }
    }
    .background(.red)
    .frame(width: 400, height: 400)
    .background(.blue)
  }
}

struct NonCustomAlignmentGuide_Previews: PreviewProvider {
  static var previews: some View {
    NonCustomAlignmentGuide()
  }
}
