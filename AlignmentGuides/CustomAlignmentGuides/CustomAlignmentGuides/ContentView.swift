//
//  ContentView.swift
//  CustomAlignmentGuides
//
//  Created by Don Clore on 4/17/23.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    HStack {
      VStack {
        Text("@pfunk")
          .alignmentGuide(.midAccountAndName) { context in
            context[VerticalAlignment.center]
          }

        Image("don-clore")
          .resizable()
          .frame(width: 64, height: 64)
      }

      VStack {
        Text("Full name:")
        Text("DON CLORE")
          .alignmentGuide(.midAccountAndName) { context in
            context[VerticalAlignment.center]
          }
          .font(.largeTitle)
      }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}

extension VerticalAlignment {
  enum MidAccountAndName: AlignmentID {
    static func defaultValue(in context: ViewDimensions) -> CGFloat {
      context[.top]
    }
  }

  static let midAccountAndName = VerticalAlignment(MidAccountAndName.self)
}
