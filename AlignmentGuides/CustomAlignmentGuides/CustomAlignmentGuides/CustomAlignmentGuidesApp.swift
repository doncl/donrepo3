//
//  CustomAlignmentGuidesApp.swift
//  CustomAlignmentGuides
//
//  Created by Don Clore on 4/17/23.
//

import SwiftUI

@main
struct CustomAlignmentGuidesApp: App {
  var body: some Scene {
    WindowGroup {
      ContentView()
    }
  }
}
