//
//  CSwiftTests.swift
//  CSwiftTests
//
//  Created by Don Clore on 12/25/22.
//

@testable import CSwift
import XCTest

final class CSwiftTests: XCTestCase {
    let lib = CPPLib()
    let wrapper = CPPWrapper()

    func testCalulationFromObjcpluplusClassInWrapper() {
        let value = wrapper.calculateValue()
        XCTAssertEqual(value, 42)
    }

    func testCalculationFromCpluplusClassPropertyInObjcPluplusClass() {
        let value = wrapper.calculateValueFromHelper()
        XCTAssertEqual(value, 56)
    }

    func testCalculationFromLib() throws {
        guard let value = lib?.calculateValue() else {
            XCTFail("lib is nil")
            return
        }
        XCTAssertEqual(value, 72)
    }
}
