//
//  CPPClassLib.hpp
//  CPPLib
//
//  Created by Don Clore on 12/25/22.
//

#ifndef CPPClassLib_hpp
#define CPPClassLib_hpp

#include <stdio.h>

class CPPClassLib {
public:
    int calculateValue();
};


#endif /* CPPClassLib_hpp */
