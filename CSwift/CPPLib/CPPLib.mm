//
//  CPPLib.m
//  CPPLib
//
//  Created by Don Clore on 12/25/22.
//

#import "CPPLib.h"
#include "CPPClassLib.hpp"

@implementation CPPLib
{
    CPPClassLib *helper;
}
-(instancetype)init {
    self = [super init];
    if (self) {
        self->helper = new CPPClassLib();
    }
    
    return self;
}

-(int)calculateValue {
    return self->helper->calculateValue();
}

@end
