//
//  CPPLib.h
//  CPPLib
//
//  Created by Don Clore on 12/25/22.
//

#import <Foundation/Foundation.h>

@interface CPPLib : NSObject
-(instancetype)init;
-(int)calculateValue;
@end
