//
//  RootVC.swift
//  CSwift
//
//  Created by Don Clore on 12/25/22.
//

import UIKit

class RootVC: UIViewController {
    let wrapper = CPPWrapper()
    let lib = CPPLib()

    let objcLabel = UILabel()
    let cPPLabel = UILabel()
    let cPPLibLabel = UILabel()

    lazy var stack: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [objcLabel, cPPLabel, cPPLibLabel])
        stack.axis = .vertical
        stack.spacing = 24
        return stack
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white

        [objcLabel, cPPLabel, cPPLibLabel].forEach {
            $0.font = UIFont.boldSystemFont(ofSize: 28)
            $0.numberOfLines = 0
            $0.textAlignment = .center
        }

        objcLabel.textColor = .purple
        cPPLabel.textColor = .red
        cPPLibLabel.textColor = .blue

        stack.translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(stack)

        NSLayoutConstraint.activate([
            stack.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
            stack.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor),
            stack.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 24),
            stack.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: -24),
        ])

        let objcValue = wrapper.calculateValue()
        objcLabel.text = "Value from Objc Method = \(objcValue)"

        let cppValue = wrapper.calculateValueFromHelper()
        cPPLabel.text = "Value from C++ Method = \(cppValue)"

        let cppLibValue = lib?.calculateValue()
        cPPLibLabel.text = "Value from C++ static lib = \(cppLibValue!)"
    }
}
