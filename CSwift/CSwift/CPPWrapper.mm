//
//  CPPWrapper.m
//  CSwift
//
//  Created by Don Clore on 12/25/22.
//

#import "CPPWrapper.h"
#include "CPPClass.hpp"


@implementation CPPWrapper
{
    CPPClass *helper;
};

-(instancetype) init {
    self = [super init];
    if (self) {
        self->helper = new CPPClass();
    }
    return self;
}

-(int)calculateValue {
    return 42;
}

-(int)calculateValueFromHelper {
    return self->helper->calculateValue();
}
@end
