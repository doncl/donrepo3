//
//  CPPClass.hpp
//  CSwift
//
//  Created by Don Clore on 12/25/22.
//

#ifndef CPPClass_hpp
#define CPPClass_hpp

#include <stdio.h>

class CPPClass {
public:
    int calculateValue();
};

#endif /* CPPClass_hpp */
