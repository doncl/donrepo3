//
//  CPPWrapper.h
//  CSwift
//
//  Created by Don Clore on 12/25/22.
//

#import <Foundation/Foundation.h>
#include "CPPLib.h"

NS_ASSUME_NONNULL_BEGIN

@interface CPPWrapper : NSObject
-(instancetype)init;
-(int)calculateValue;
-(int)calculateValueFromHelper;
@end

NS_ASSUME_NONNULL_END
