//
//  ContentView.swift
//  KeychainSharingApp1
//
//  Created by Don Clore on 5/25/23.
//

import SwiftUI

struct ContentView: View {
    @State private var addKeychainString = ""
    @State private var showAlert = false
    @State private var alertMessage = ""
    let mySecret1Key = "MySecret1"

    var body: some View {
        Form() {
            Section {
                TextField("Add Keychain item \"\(mySecret1Key)\"", text: $addKeychainString)
                    .onSubmit {
                        processAddResult()
                    }
            } header: {
                Text("Add Stuff")
            }
            
            Section {
                Button {
                    processGetSecretResult()
                } label: {
                    Text("Show value entered for \"MySecret1\"")
                }
                
                Button {
                    processDeleteSecretResult()
                } label: {
                    Text("Delete existing value for \"MySecret1\"")
                }
            } header: {
                Text("Show, and delete stuff")
            }
        }
        .padding()
        .alert(isPresented: $showAlert) {
            Alert(
                title: Text("Result:"),
                message: Text(alertMessage),
                dismissButton: .default(Text("OK")) {
                    self.alertMessage = ""
                    self.showAlert = false
                })
        }
        .background(Color.yellow)
    }
    
    
    private func processDeleteSecretResult() {
        let result = KeychainService.shared.deleteSharedKeyChainItem(key: mySecret1Key)
        switch result {
            
        case .success:
            alertMessage = "Successfully deleted value for key: \"\(mySecret1Key)\""
            showAlert = true
            
        case .failure(let error):
            let keyChainError = error as! KeychainServiceError
            switch keyChainError {
            case .cantDeleteIttem(let osError):
                alertMessage = "Couldn't delete value for key: \"\(mySecret1Key)\", error = \(osError)"
                showAlert = true
                
            default:
                break
            }
        }
    }
    
    private func processGetSecretResult() {
        let result = KeychainService.shared.findSharedKeychainItem(key: mySecret1Key)
        switch result {
        case .success(let string):
            alertMessage = "Result from searching for \"\(mySecret1Key)\" is \"\(string)\""
            showAlert = true
            
        case .failure(let error):
            let keyChainError = error as! KeychainServiceError
            switch keyChainError {
            case .cantDecodeValue:
                alertMessage = "Failed to decode value that came from keychain."
                showAlert = true
            case .cantFindItem(let osErr):
                alertMessage = "Couldn't find item for \"\(mySecret1Key)\", error = \(osErr)"
                showAlert = true
                
            default:
                break
            }
        }
 
    }
    
    private func processAddResult() {
        guard !addKeychainString.isEmpty else {
            return
        }
        let result = KeychainService.shared.addSharedKeychainItem(key: mySecret1Key, itemValue: addKeychainString)
        switch result {
        case .success:
            alertMessage = "Successfully stored value for \"\(mySecret1Key)\" to keychain"
            showAlert = true
            
        case .failure(let error):
            let keyChainError = error as! KeychainServiceError
            switch keyChainError {
                
            case .cantEncodeValue:
                alertMessage = "Failed to encode value for \"\(addKeychainString)\""
                showAlert = true
                
            case .cantSaveItem(let osError):
                alertMessage = "Failed to save for \"\(mySecret1Key)\" to Keychain, error = \(osError)"
                showAlert = true
            default:
                break
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

