//
//  KeychainSharingApp1App.swift
//  KeychainSharingApp1
//
//  Created by Don Clore on 5/25/23.
//

import SwiftUI

@main
struct KeychainSharingApp1App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
