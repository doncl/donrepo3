//
//  KeychainService.swift
//  KeychainSharingApp1
//
//  Created by Don Clore on 5/25/23.
//

import Foundation
import os

enum KeychainServiceError: Error {
    case cantEncodeValue
    case cantDecodeValue
    case cantDeleteIttem(OSStatus)
    case cantSaveItem(OSStatus)
    case cantFindItem(OSStatus)
}

struct KeychainService {
    let groupName = "63HZLY9JEQ.com.beerbarrelpokerstudios.KeychainSharing"
    
    static var shared = KeychainService()
    
    
    private init() {}
    
    func deleteSharedKeyChainItem(key: String) -> Result<Bool, Error> {
        let queryDelete: [String: AnyObject] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: key as AnyObject,
            kSecAttrSynchronizable as String: kCFBooleanTrue,
            kSecAttrAccessGroup as String: groupName as AnyObject,
        ]
        
        let resultCodeDelete = SecItemDelete(queryDelete as CFDictionary)
        
        if resultCodeDelete != noErr {
            Self.logger.error("Error deleting key \(key) from Keychain: \(resultCodeDelete)")
            return .failure(KeychainServiceError.cantDeleteIttem(resultCodeDelete))
        }
        return .success(true)
    }
    
    func addSharedKeychainItem(key: String, itemValue: String) -> Result<Bool, Error> {
        guard let valueData = itemValue.data(using: .utf8) else {
            Self.logger.error("Error saving text to Keychain")
            return .failure(KeychainServiceError.cantEncodeValue)
        }
        
        let queryAdd: [String: AnyObject] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: key as AnyObject,
            kSecValueData as String: valueData as AnyObject,
            kSecAttrAccessible as String: kSecAttrAccessibleWhenUnlocked,
            kSecAttrSynchronizable as String: kCFBooleanTrue,
            kSecAttrAccessGroup as String: groupName as AnyObject,
        ]
        
        let resultCode = SecItemAdd(queryAdd as CFDictionary, nil)
        
        if resultCode != noErr {
            Self.logger.error("Error saving to Keychain: \(resultCode)")
            return .failure(KeychainServiceError.cantSaveItem(resultCode))
        }
        return .success(true)
    }
    
    func findSharedKeychainItem(key: String) -> Result<String, Error> {
        let queryLoad: [String: AnyObject] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: key as AnyObject,
            kSecReturnData as String: kCFBooleanTrue,
            kSecMatchLimit as String: kSecMatchLimitOne,
            kSecAttrSynchronizable as String: kCFBooleanTrue,
            kSecAttrAccessGroup as String: groupName as AnyObject,
        ]
        
        var result: AnyObject?
        
        let resultCodeLoad = withUnsafeMutablePointer(to: &result) {
            SecItemCopyMatching(queryLoad as CFDictionary, UnsafeMutablePointer($0))
        }
        
        if resultCodeLoad == noErr {
            if let result = result as? Data, let value = NSString(data: result, encoding: String.Encoding.utf8.rawValue) as? String {
                return .success(value)
            } else {
                Self.logger.error("Failed to decode found item for key \(key)")
                return .failure(KeychainServiceError.cantDecodeValue)
            }
        } else {
            Self.logger.error("Failed to find item for key \(key), error = \(resultCodeLoad)")
            return .failure(KeychainServiceError.cantFindItem(resultCodeLoad))
        }
    }
}

extension KeychainService {
    static let logger = Logger(
        subsystem: "com.beerbarrelpokerstudios.KeychainSharing.keychainservice",
        category: String(describing: KeychainService.self)
    )
}


