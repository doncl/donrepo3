//
//  KeychainSharingApp2App.swift
//  KeychainSharingApp2
//
//  Created by Don Clore on 5/25/23.
//

import SwiftUI

@main
struct KeychainSharingApp2App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
