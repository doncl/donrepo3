//
//  KeyChainNotSharingAppApp.swift
//  KeyChainNotSharingApp
//
//  Created by Don Clore on 5/26/23.
//

import SwiftUI

@main
struct KeyChainNotSharingAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
