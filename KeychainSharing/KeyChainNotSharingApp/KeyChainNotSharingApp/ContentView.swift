//
//  ContentView.swift
//  KeyChainNotSharingApp
//
//  Created by Don Clore on 5/26/23.
//

import SwiftUI

struct ContentView: View {
    @State private var showAlert = false
    @State private var alertMessage = ""
    let mySecret1Key = "MySecret1"
    
    var body: some View {
        
        VStack {
            Form() {
                Section {
                    Button {
                        processGetSecretResult()
                    } label: {
                        Text("Show value entered for \"MySecret1\"")
                    }
                    
                } header: {
                    Text("Show stuff")
                }
            }
            .padding()
            .alert(isPresented: $showAlert) {
                Alert(
                    title: Text("Result:"),
                    message: Text(alertMessage),
                    dismissButton: .default(Text("OK")) {
                        self.alertMessage = ""
                        self.showAlert = false
                    })
            }
            .background(Color.pink)
        }
        .padding()
    }
    
    private func processGetSecretResult() {
        let result = KeychainService.shared.findSharedKeychainItem(key: mySecret1Key)
        switch result {
        case .success(let string):
            alertMessage = "Result from searching for \"\(mySecret1Key)\" is \"\(string)\""
            showAlert = true
            
        case .failure(let error):
            let keyChainError = error as! KeychainServiceError
            switch keyChainError {
            case .cantDecodeValue:
                alertMessage = "Failed to decode value that came from keychain."
                showAlert = true
            case .cantFindItem(let osErr):
                alertMessage = "Couldn't find item for \"\(mySecret1Key)\", error = \(osErr)"
                showAlert = true
                
            default:
                break
            }
        }
 
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
