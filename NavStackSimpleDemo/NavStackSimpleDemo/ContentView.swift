//
//  ContentView.swift
//  NavStackSimpleDemo
//
//  Created by Don Clore on 5/22/23.
//

import SwiftUI

struct ContentView: View {
    @State private var presentedNumbers = [1, 4, 8]

      var body: some View {
          NavigationStack(path: $presentedNumbers) {
              List(1..<50) { i in
//                  NavigationLink(value: i) {
//                      Label("Row \(i)", systemImage: "\(i).circle")
//                  }
              }
              .navigationDestination(for: Int.self) { i in
                  Text("Detail \(i)")
              }
              .navigationTitle("Navigation")
          }
          .too
      }
}

//struct ContentView: View {
//    @State private var navPath = NavigationPath()
//
//    var body: some View {
//        NavigationStack(path: $navPath) {
//            Button("Jump to random") {
//                navPath.append(Int.random(in: 1..<50))
//            }
//
//            List(1..<50) { i in
//                NavigationLink(value: "Row \(i)") {
//                    Label("Row \(i)", systemImage: "\(i).circle")
//                }
//            }
//            .navigationDestination(for: Int.self) { i in
//                Text("Int Detail \(i)")
//            }
//            .navigationDestination(for: String.self) { i in
//                Text("String Detail \(i)")
//            }
//            .navigationTitle("Navigation")
//        }
//    }
//}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
