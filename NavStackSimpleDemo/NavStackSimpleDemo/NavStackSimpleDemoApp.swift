//
//  NavStackSimpleDemoApp.swift
//  NavStackSimpleDemo
//
//  Created by Don Clore on 5/22/23.
//

import SwiftUI

@main
struct NavStackSimpleDemoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
