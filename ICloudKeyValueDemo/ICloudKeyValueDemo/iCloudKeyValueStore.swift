//
//  iCloudKeyValueStore.swift
//  ICloudKeyValueDemo
//
//  Created by Don Clore on 5/25/23.
//

import Foundation

class iCloudKeyValueStore {
    
    static let shared = iCloudKeyValueStore()
    private let keyValueStore = NSUbiquitousKeyValueStore.default

    // Add observers for changes
    private init() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(iCloudKeysChanged),
            name: NSUbiquitousKeyValueStore.didChangeExternallyNotification,
            object: keyValueStore)
        
        keyValueStore.synchronize()
    }

    // Set data
    func set(key: String, value: Any) {
        keyValueStore.set(value, forKey: key)
        keyValueStore.synchronize()
    }

    // Get data
    func get(key: String) -> Any? {
        return keyValueStore.object(forKey: key)
    }

    // Remove data
    func remove(key: String) {
        keyValueStore.removeObject(forKey: key)
        keyValueStore.synchronize()
    }

    // Observe changes
    @objc private func iCloudKeysChanged(notification: Notification) {
        // handle changes in iCloud key-value store
    }
    
    
}
