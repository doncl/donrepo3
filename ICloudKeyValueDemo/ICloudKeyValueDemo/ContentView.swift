//
//  ContentView.swift
//  ICloudKeyValueDemo
//
//  Created by Don Clore on 5/25/23.
//

import SwiftUI

struct ContentView: View {
    @State private var iCloudKeyValueValue = ""
    @State private var showAlert = false
    @State private var alertMessage = ""
    let mySecret1Key = "MySecret1"

    var body: some View {
        Form() {
            Section {
                TextField("Add iCloud KV item \"\(mySecret1Key)\"", text: $iCloudKeyValueValue)
                    .onSubmit {
                        processAddResult()
                    }
            } header: {
                Text("Add Stuff")
            }
            
            Section {
                Button {
                    processGetSecretResult()
                } label: {
                    Text("Show value entered for \"MySecret1\"")
                }
                
//                Button {
//                    processDeleteSecretResult()
//                } label: {
//                    Text("Delete existing value for \"MySecret1\"")
//                }
            } header: {
                Text("Show stuff")
            }
        }
        .padding()
        .alert(isPresented: $showAlert) {
            Alert(
                title: Text("Result:"),
                message: Text(alertMessage),
                dismissButton: .default(Text("OK")) {
                    self.alertMessage = ""
                    self.showAlert = false
                })
        }
        .background(Color.accentColor)
    }
    
    
    
    private func processGetSecretResult() {
        guard let value = iCloudKeyValueStore.shared.get(key: mySecret1Key) else {
            alertMessage = "Failed to get value for \"\(mySecret1Key)\""
            showAlert = true
            return
        }
        alertMessage = "retrieved value \"\(value)\" for key \"\(mySecret1Key)\""
        showAlert = true
    }
    
    private func processAddResult() {
        guard !iCloudKeyValueValue.isEmpty else {
            alertMessage = "Nothing to store - put something in the textfield"
            showAlert = true
            return
        }
        
        iCloudKeyValueStore.shared.set(key: mySecret1Key, value: iCloudKeyValueValue)
        
        alertMessage = "Stored value for \"\(mySecret1Key)\""
        showAlert = true
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

