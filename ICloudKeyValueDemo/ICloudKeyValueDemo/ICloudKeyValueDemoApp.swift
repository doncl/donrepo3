//
//  ICloudKeyValueDemoApp.swift
//  ICloudKeyValueDemo
//
//  Created by Don Clore on 5/25/23.
//

import SwiftUI

@main
struct ICloudKeyValueDemoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
