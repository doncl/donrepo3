//
//  AView.swift
//  NavExample
//
//  Created by Don Clore on 9/8/22.
//

import SwiftUI

struct AView: View {
    var color: Color

    var body: some View {
        color
    }
}

struct AView_Previews: PreviewProvider {
    static var previews: some View {
        AView(color: .blue)
    }
}
