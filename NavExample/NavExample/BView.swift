//
//  BView.swift
//  NavExample
//
//  Created by Don Clore on 9/8/22.
//

import SwiftUI

struct BView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct BView_Previews: PreviewProvider {
    static var previews: some View {
        BView()
    }
}
