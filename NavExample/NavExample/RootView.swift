//
//  ContentView.swift
//  NavExample
//
//  Created by Don Clore on 9/8/22.
//

import SwiftUI

struct RootView: View {
    let colors: [Color] = [.blue, .green, .brown, .pink]
    var body: some View {
        NavigationStack {
            List(colors, id: \.self) { color in
                NavigationLink(String(describing: color), value: color)
            }
            .navigationDestination(for: Color.self) { color in
                AView(color: color)
            }
            .navigationTitle("Colors")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        RootView()
    }
}
