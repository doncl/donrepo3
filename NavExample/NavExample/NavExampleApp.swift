//
//  NavExampleApp.swift
//  NavExample
//
//  Created by Don Clore on 9/8/22.
//

import SwiftUI

@main
struct NavExampleApp: App {
    var body: some Scene {
        WindowGroup {
            RootView()
        }
    }
}
